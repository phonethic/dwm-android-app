package com.phonethics.fragments;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyResponse;
import com.android.volley.VolleyResponse.ErrorListener;
import com.android.volley.VolleyResponse.Listener;
import com.android.volley.toolbox.JsonObjectRequest;

import com.android.volley.toolbox.Volley;
import com.flurry.android.FlurryAgent;
import com.localytics.android.LocalyticsSession;
import com.phonethics.adapters.SubmissionsAdapter;
import com.phonethics.customclass.SongInfo;

import com.phonethics.dancewithmadhuri.R;
import com.phonethics.dancewithmadhuri.SubmittedVideos;
import com.phonethics.network.NetworkCheck;
import com.phonethics.sharepref.SharePref;

public class Submissions extends Fragment implements OnClickListener,OnItemClickListener {

	TextView			text_browse_song,bttn_text_select_song,text_submit_video,bttn_text_upload_video;
	ListView			list_select_song,list_upload_video;
	String				URL_DWM,URL_VIDEO_SUBMISSIONS;
	String				UPLOAD_VIDEO_URL="";
	ArrayList<SongInfo> songInfoArr;
	ImageView	pBar;
	Animation	rotate;
	String				VIDEO_PATH,USER_ID;
	String				SONG_ID;
	SharePref			sharePref;
	String				message;
	String				cantUploadVideo="";
	AlertDialog 		alert=null;
	boolean				onStartCall = true;
	NetworkCheck		netCheck;

	private static boolean useLocalytics 		= false;
	private static boolean useFlurry 		= false;
	private LocalyticsSession localyticsSession;
	private static  String LOCALYTICS_APP_KEY	=""; 


	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.submissions, null);
		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/KabelMediumBT.ttf");
		Button bttn = (Button) view.findViewById(R.id.bttn_menu);
		TextView text_tittle = (TextView) view.findViewById(R.id.text_tittle);
		text_tittle.setText("Submission");
		text_tittle.setTypeface(tf);
		bttn.setTypeface(tf);

		sharePref				= new SharePref(getActivity());
		netCheck				= new NetworkCheck(getActivity());
		USER_ID					= sharePref.getUserId();
		URL_DWM					= getResources().getString(R.string.url_md_live_server);
		URL_VIDEO_SUBMISSIONS	= URL_DWM + getResources().getString(R.string.url_vide_submissions);
		UPLOAD_VIDEO_URL		= URL_DWM + getResources().getString(R.string.url_submitt_video);
		cantUploadVideo			= getResources().getString(R.string.cant_upload_video);
		//UPLOAD_VIDEO_URL		= "http://dancewithmadhuri.com/submit-android.php";


		text_browse_song 		= (TextView) view.findViewById(R.id.text_browse_song);
		bttn_text_select_song 	= (TextView) view.findViewById(R.id.bttn_text_select_song);
		text_submit_video 		= (TextView) view.findViewById(R.id.text_submit_video);
		bttn_text_upload_video 	= (TextView) view.findViewById(R.id.bttn_text_upload_video);
		list_select_song		= (ListView) view.findViewById(R.id.list_select_song);
		list_upload_video		= (ListView) view.findViewById(R.id.list_upload_video);
		rotate 					= AnimationUtils.loadAnimation(getActivity(), R.anim.rotate);
		pBar					= (ImageView) view.findViewById(R.id.progressBar2);
		songInfoArr				= new ArrayList<SongInfo>();

		text_browse_song.setTypeface(tf);
		bttn_text_select_song.setTypeface(tf);
		text_submit_video.setTypeface(tf);
		bttn_text_upload_video.setTypeface(tf);

		text_browse_song.setTextSize(17);
		bttn_text_select_song.setTextSize(17);
		text_submit_video.setTextSize(17);
		bttn_text_upload_video.setTextSize(17);

		list_select_song.setVisibility(View.VISIBLE);
		list_upload_video.setVisibility(View.VISIBLE);
		list_select_song.setCacheColorHint(0);


		useFlurry			= getResources().getBoolean(R.bool.useFlurry);
		useLocalytics			= getResources().getBoolean(R.bool.useLocalytics);

		if(useLocalytics){
			LOCALYTICS_APP_KEY 		= getResources().getString(R.string.LOCALYTICS_APP_KEY);
			this.localyticsSession 	= new LocalyticsSession(getActivity(),LOCALYTICS_APP_KEY);
			this.localyticsSession.open();
			//this.localyticsSession.upload();
			localyticsSession.tagScreen(getResources().getString(R.string.localEvent_tab_submission));
			this.localyticsSession.upload();
		}

		bttn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				RootFragment rf = (RootFragment) getActivity();
				rf.showSlidingMenu();
			}
		});




		/*bttn_text_upload_video.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				list_upload_video.performItemClick(v, 0, 0);
			}
		});*/


		/*list_select_song.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getActivity(), SubmittedVideos.class);
				intent.putExtra("songId", songInfoArr.get(pos).getSong_id());
				intent.putExtra("songName", songInfoArr.get(pos).getTitle());
				startActivity(intent);
				getActivity().overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);



			}
		});*/

		list_select_song.setCacheColorHint(0);
		list_select_song.setOnItemClickListener(this);
		list_upload_video.setOnItemClickListener(this);
		bttn_text_upload_video.setOnClickListener(this);


		/*list_upload_video.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub

				SONG_ID = songInfoArr.get(pos).getSong_id();
				Log.d("Id", "MdSongId =="+ "SongId -- "+SONG_ID+" UserId -- "+USER_ID);

				AlertDialog.Builder  alertDialog = new AlertDialog.Builder(getActivity());
				//alertDialog.setIcon(R.drawable.ic_launcher);
				alertDialog.setTitle("Dance With Madhuri");
				alertDialog.setMessage(cantUploadVideo);
				alertDialog.setCancelable(true);
				alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub

						alert.dismiss();
					}
				});
				alert = alertDialog.create();
				alert.show();
			}
		});*/

		/*getSongList(URL_VIDEO_SUBMISSIONS);*/


		return view;
	}



	public void getSongList(String url){
		pBar.setVisibility(View.VISIBLE);
		pBar.startAnimation(rotate);
		Log.d("Md", "MdUrl >> " + url);

		RequestQueue 				queue;
		queue					= Volley.newRequestQueue(getActivity());

		VolleyResponse.Listener<JSONObject> listener = new VolleyResponse.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub
				Log.d("Responce", "MdSongsResponce success "+ response.toString());
				try{
					if(response.getString("success").equalsIgnoreCase("true")){
						ParseJsonData(response.getJSONArray("data"));		
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}

			}
		};

		VolleyResponse.ErrorListener errorListener = new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				Log.d("Error", "MdSongsResponce error "+ error.toString());
				pBar.clearAnimation();
				pBar.setVisibility(View.GONE);
				showToast("Network Error!");
			}
		};

		JsonObjectRequest req = new JsonObjectRequest(Method.GET, url, null, listener, errorListener);
		queue.add(req);
	}


	public void ParseJsonData(JSONArray jsonArr){
		pBar.setVisibility(View.VISIBLE);
		pBar.startAnimation(rotate);
		try{
			for(int i=0;i<jsonArr.length();i++){
				SongInfo songInfo = new SongInfo();

				JSONObject tempObj 	= jsonArr.getJSONObject(i);
				String id 			=	tempObj.getString("Song_ID");
				String title		=	tempObj.getString("Title");
				String movie		=	tempObj.getString("Movie");
				String choreographer =	tempObj.getString("Choreographer");
				String thumb		=	tempObj.getString("Thumbnail");
				String locktype		=	tempObj.getString("Lock_Type");
				String category		=	tempObj.getString("Category");
				String category_name=	tempObj.getString("Category_Name");
				String is_video 	=	tempObj.getString("Is_Video");
				String video_link 	=	tempObj.getString("Video_Link");

				songInfo.setSong_id(id);
				songInfo.setTitle(title);
				songInfo.setMovie(movie);
				songInfo.setChoreographer(choreographer);
				songInfo.setThumbnail(thumb);
				songInfo.setLock_type(locktype);
				songInfo.setCategory(category);
				songInfo.setCategory_name(category_name);
				songInfo.setIs_video(is_video);
				songInfo.setVideo_link(video_link);

				if(category.equalsIgnoreCase("5")){

				}else{
					songInfoArr.add(songInfo);	
				}


			}

			list_select_song.setAdapter(new SubmissionsAdapter(getActivity(), songInfoArr));
			list_upload_video.setAdapter(new SubmissionsAdapter(getActivity(), songInfoArr));
			pBar.setVisibility(View.INVISIBLE);
			pBar.clearAnimation();

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}



	// And to convert the image URI to the direct file system path of the image file
	public String getRealPathFromURI(Uri contentUri) {
		// can post image
		String [] proj={MediaStore.Video.Media.DATA};
		Cursor cursor = getActivity().getContentResolver().query(contentUri,
				proj, // Which columns to return
				null, // WHERE clause; which rows to return (all rows)
				null, // WHERE clause selection arguments (none)
				null); // Order-by clause (ascending by name)
		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}


	public void showToast(String content){
		try{
			Toast.makeText(getActivity(), content, 0).show();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if(data == null || resultCode == 0){
			VIDEO_PATH="";
		}else if(requestCode == 2){

			/*layout.setBackgroundResource(R.drawable.published_page);*/

			final Uri contentUri = data.getData();
			VIDEO_PATH = getPath(contentUri);
			//showImage(path);


		}

		else if(requestCode == 10){
			Uri selectedImage = data.getData();
			try {

				VIDEO_PATH = getRealPathFromURI(selectedImage);
				//showImage(path);


			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}



		}

		Log.d("Video Path", "MdVideoPath==" + VIDEO_PATH);
		new UploadVideo().execute("");
		//uploadVideo(UPLOAD_VIDEO_URL);

		/*try {
			byte[] byteArr = readBytes(VIDEO_PATH);
			Log.d("Video Path", "MdVideoSize==" + byteArr.length);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/


		super.onActivityResult(requestCode, resultCode, data);
	}

	private String getPath(Uri selectedImage) {
		// TODO Auto-generated method stub

		String[] projection = { MediaStore.Video.Media.DATA };
		Cursor cursor = getActivity().getContentResolver().query(selectedImage, projection, null, null, null);
		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);

	}


	public byte[] readBytes(String videoPath) throws IOException {
		ByteArrayOutputStream byteBuffer = null;
		try{
			Uri uri 				= Uri.parse(videoPath);	
			//InputStream inputStream = getActivity().getContentResolver().openInputStream(uri);
			InputStream inputStream = new FileInputStream(new File(videoPath));


			// this dynamically extends to take the bytes you read
			byteBuffer 	= new ByteArrayOutputStream();

			// this is storage overwritten on each iteration with bytes
			int bufferSize = 2*1024*1024;
			byte[] buffer = new byte[bufferSize];

			// we need to know how may bytes were read to write them to the byteBuffer
			int len = 0;
			while ((len = inputStream.read(buffer)) != -1) {
				byteBuffer.write(buffer, 0, len);
			}
			inputStream.close();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		// and then we can return your byte array.
		return byteBuffer.toByteArray();
	}


	public class UploadVideo extends AsyncTask<String, String, String>{



		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pBar.setVisibility(View.VISIBLE);
			pBar.startAnimation(rotate);
			Toast.makeText(getActivity(), "Please Wait", Toast.LENGTH_LONG).show();
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub

			/*	 HttpClient 		httpclient 		= new DefaultHttpClient();
			 HttpPost 			httppost 		= new HttpPost(YOUR_URL);
			 FileBody filebodyVideo 			= new FileBody(new File(videoPath));*/

			String upLoadServerUri = UPLOAD_VIDEO_URL;
			String fileName 		= "video";
			String lineEnd 			= "\r\n";
			String twoHyphens 		= "--";
			String boundary			= "---------------------------127412016815782193391561906476";

			HttpURLConnection 		conn 			= null;
			DataOutputStream 		dos 			= null;
			DataInputStream 		inStream 		= null;




			int bytesRead, bytesAvailable, bufferSize;
			byte[] buffer;
			int maxBufferSize = 2* 1024 * 1024;
			String responseFromServer = "";

			File sourceFile = new File(VIDEO_PATH);
			FileInputStream fileInputStream = null;
			try {
				fileInputStream = new FileInputStream(sourceFile);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (!sourceFile.isFile()) {
				Log.e("Huzza", "MdSource File Does not exist");

			}else{
				Log.e("Huzza", "MdSource File exist "+sourceFile.length());

			}

			try { // open a URL connection to the Servlet
				//Log.d("", "MdVideosize bytes "+readBytes(VIDEO_PATH).length);
				Log.d("", "MdVideosize file "+sourceFile.length());

				URL url = new URL(upLoadServerUri);
				conn = (HttpURLConnection) url.openConnection(); // Open a HTTP  connection to  the URL
				conn.setDoInput(true); // Allow Inputs
				conn.setDoOutput(true); // Allow Outputs
				conn.setUseCaches(false); // Don't use a Cached Copy

				conn.setRequestMethod("POST");
				conn.setRequestProperty("Connection", "Keep-Alive");
				conn.setRequestProperty("ENCTYPE", "multipart/form-data");
				conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
				//conn.setChunkedStreamingMode(1024);
				conn.setFixedLengthStreamingMode((int) sourceFile.length());
				conn.setRequestProperty("uploaded_file", fileName);

				dos = new DataOutputStream(conn.getOutputStream());



				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"song_id\"" + lineEnd+lineEnd+SONG_ID);
				///dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""+ fileName+".mp4" + "\"" + lineEnd);
				//dos.writeBytes(lineEnd);
				dos.writeBytes(lineEnd + twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"upload_file\"; filename=\""+fileName+".mp4");
				dos.writeBytes(lineEnd+"Content-Type: video/mp4"+lineEnd+lineEnd);

				//bytesAvailable = fileInputStream.available(); // create a buffer of  maximum size

				bytesAvailable = fileInputStream.available();
				//while(bytesAvailable>-1){
				Log.i("Huzza", "MdInitial .available : " + bytesAvailable);

				bufferSize = Math.min(bytesAvailable, maxBufferSize);

				buffer = new byte[bufferSize];

				int bufferLength = 0;
				// read file and write it into form...
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				while (bytesRead > 0) {
					dos.write(buffer, 0, bufferSize);
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);
				}
				//		bytesAvailable = fileInputStream.read();
				//}


				/*buffer = new byte[8192];
				bytesRead = 0;
				while ((bytesRead = fileInputStream.read(buffer)) >0		) {
				    dos.write(buffer, 0, bytesRead);
				}*/

				// send multipart form data necesssary after file data...
				//dos.writeBytes(lineEnd);
				dos.writeBytes(lineEnd+twoHyphens + boundary +  lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"code\"\r\n\r\n\r\n");
				dos.writeBytes(twoHyphens+boundary+lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"user_id\""+lineEnd+lineEnd+USER_ID);
				dos.writeBytes(lineEnd+twoHyphens+boundary+twoHyphens+lineEnd);

				//dos.writeBytes(lineEnd+"Content-Type: video/mp4"+lineEnd+lineEnd);

				// Responses from the server (code and message)
				int serverResponseCode 			= conn.getResponseCode();
				String serverResponseMessage 	= conn.getResponseMessage();

				Log.i("Upload file to server", "MdHTTP Response is : " + serverResponseMessage + ": " + serverResponseCode);
				// close streams
				Log.i("Upload file to server", "Md== "+fileName + " File is written");
				/*fileInputStream.close();
				dos.flush();
				dos.close();*/



				try {
					BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
					String line;
					while ((line = rd.readLine()) != null) {
						Log.i("Huzza", "MdRES Message: " + line);
						message = line;
					}
					rd.close();
				} catch (IOException ioex) {
					Log.e("Huzza", "error: " + ioex.getMessage(), ioex);
				}

				fileInputStream.close();
				dos.flush();
				dos.close();
			}catch(Exception ex){
				ex.printStackTrace();
			}

			return null;
		}


		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pBar.setVisibility(View.GONE);
			pBar.clearAnimation();
			Toast.makeText(getActivity(), ""+message, Toast.LENGTH_LONG).show();
		}


	}

	public class UploadVideoFile extends AsyncTask<String, String, String>{




		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pBar.setVisibility(View.VISIBLE);
			pBar.startAnimation(rotate);
			Toast.makeText(getActivity(), "Please Wait", Toast.LENGTH_LONG).show();
		}

		@Override
		protected String doInBackground(String... params) {
			try{
				File f = new File(VIDEO_PATH);

			}catch(Exception ex){
				ex.printStackTrace();
			}
			return SONG_ID;

		}


		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pBar.setVisibility(View.GONE);
			pBar.clearAnimation();
			Toast.makeText(getActivity(), ""+message, Toast.LENGTH_LONG).show();
		}



	}





	public void uploadVideo(String url){
		pBar.setVisibility(View.VISIBLE);
		RequestQueue queue;
		queue							= Volley.newRequestQueue(getActivity());
		Listener<String> listener = new Listener<String>() {

			@Override
			public void onResponse(String response) {
				// TODO Auto-generated method stub
				Log.d("", "MdVolleyMultipart respose== "+response.toString());
				pBar.setVisibility(View.INVISIBLE);
			}
		};

		ErrorListener error = new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				Log.d("", "MdVolleyError error== "+error.toString());
				pBar.setVisibility(View.INVISIBLE);

			}
		};




		/*MultiPartRequest<String> request1 = new MultiPartRequest<String>(Method.POST,url,listener,error) {

			@Override
			protected VolleyResponse<String> parseNetworkResponse(
					NetworkResponse response) {
				// TODO Auto-generated method stub
				return null;
			}
		};

		request1.addFile("video.mp4", VIDEO_PATH);
		request1.addMultipartParam("boundry", "multipart/form-data; boundary=", "---------------------------127412016815782193391561906476");*/

		//MultipartEntity entity = new MultipartEntity("---------------------------127412016815782193391561906476");

		/*MultiPartRequest<String> request = new MultiPartRequest<String>(Method.POST,url,listener,error) {

			@Override
			protected VolleyResponse<String> parseNetworkResponse(
					NetworkResponse response) {
				// TODO Auto-generated method stub
				Log.d("", "MdNetworkResponse respose== "+response.toString());
				return null;
			}
		};*/

		Map<String, String> headersMap = new HashMap<String, String>();
		Map<String, String> params	 = new HashMap<String, String>();
		headersMap.put("ENCTYPE", "multipart/form-data");
		headersMap.put("uploaded_file", "video.mp4");

		//request.addFile("video.mp4", VIDEO_PATH);

		//request.addMultipartParam("songid", "song_id", SONG_ID);
		//request.addMultipartParam("filename", "upload_file", "video.mp4");
		//request.addMultipartParam("userid", "user_id", USER_ID);
		params.put("song_id", SONG_ID);
		params.put("upload_file", "video.mp4");
		params.put("user_id", USER_ID);
		//request.setHeaders(headersMap);

		//queue.add(request);
	}



	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if(useFlurry){
			FlurryAgent.onStartSession(getActivity(), getResources().getString(R.string.flurry_id));
			FlurryAgent.logEvent(getResources().getString(R.string.fEvent_tab_submission));
		}

		if(onStartCall){
			onStartCall = false;
			if(netCheck.isNetworkAvailable()){
				getSongList(URL_VIDEO_SUBMISSIONS);
			}else{
				pBar.setVisibility(View.GONE);
				pBar.clearAnimation();
				showToast("No Internet Connection");
			}
		}

	}


	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if(useFlurry){
			FlurryAgent.onEndSession(getActivity());
		}


	}


	@Override
	public void onItemClick(AdapterView<?> parent, View view, int pos, long arg3) {
		// TODO Auto-generated method stub
		switch (parent.getId()) {
		case R.id.list_select_song:
			Intent intent = new Intent(getActivity(), SubmittedVideos.class);
			intent.putExtra("songId", songInfoArr.get(pos).getSong_id());
			intent.putExtra("songName", songInfoArr.get(pos).getCategory_name()+" - "+songInfoArr.get(pos).getTitle());
			startActivity(intent);
			getActivity().overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			break;
		case R.id.list_upload_video:
			showAlert();
			break;
		default:
			break;
		}
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.bttn_text_upload_video:
			showAlert();
			//showAlertForPickVideo(0);
			break;

		default:
			break;
		}
	}


	public void showAlertForPickVideo(int pos){
		SONG_ID = songInfoArr.get(pos).getSong_id();
		Log.d("Id", "MdSongId =="+ "SongId -- "+SONG_ID+" UserId -- "+USER_ID);

		AlertDialog.Builder  alertDialog = new AlertDialog.Builder(getActivity());
		//alertDialog.setIcon(R.drawable.ic_launcher);
		alertDialog.setTitle("Dance With Madhuri");
		alertDialog.setMessage("Choose Video.");
		alertDialog.setCancelable(true);
		alertDialog.setPositiveButton("From Gallery", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setType("video/*");
				intent.setAction(Intent.ACTION_GET_CONTENT);

				startActivityForResult(intent, 10);

				getActivity().overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				alert.dismiss();
			}
		});
		alertDialog.setNegativeButton("Reacord Video", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				Intent takePictureIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

				startActivityForResult(takePictureIntent, 2);	
				getActivity().overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});
		alert = alertDialog.create();
		alert.show();
	}
	public void showAlert(){
		//SONG_ID = songInfoArr.get(pos).getSong_id();
		Log.d("Id", "MdSongId =="+ "SongId -- "+SONG_ID+" UserId -- "+USER_ID);

		AlertDialog.Builder  alertDialog = new AlertDialog.Builder(getActivity());
		//alertDialog.setIcon(R.drawable.ic_launcher);
		alertDialog.setTitle("Dance With Madhuri");
		alertDialog.setMessage(cantUploadVideo);
		alertDialog.setCancelable(true);
		alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub

				alert.dismiss();
			}
		});
		alert = alertDialog.create();
		alert.show();
	}




	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(useLocalytics){
			this.localyticsSession.open();
		}
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub

		super.onPause();
		if(useLocalytics){
			this.localyticsSession.close();
			this.localyticsSession.upload();
		}
	}

}


