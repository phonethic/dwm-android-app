package com.phonethics.fragments;

import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyResponse;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.LoggingBehavior;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.Settings;
import com.phonethics.dancewithmadhuri.LoginScreen;
import com.phonethics.dancewithmadhuri.R;
import com.phonethics.sharepref.SharePref;

public class Logout extends Fragment {

	TextView	text_wana_logOut;
	Button		bttn_logout,bttn_cancel;
	private Session.StatusCallback statusCallback = new SessionStatusCallback();
	SharePref	sharePref;
	boolean		from_fb=false;
	String		url_logout = "";
	ImageView	pBar;
	Animation	rotate;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.logout, null);
		Settings.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);

		Session session = Session.getActiveSession();

		if (session == null) {
			if (savedInstanceState != null) {
				session = Session.restoreSession(getActivity(), null, statusCallback, savedInstanceState);
			}
			if (session == null) {
				session = new Session(getActivity());
			}
			Session.setActiveSession(session);
			if (session.getState().equals(SessionState.CREATED_TOKEN_LOADED)) {
				session.openForRead(new Session.OpenRequest(this).setCallback(statusCallback));
			}
		}
		sharePref = new SharePref(getActivity());
		
		from_fb	  = sharePref.isLoginFromFb();
		
		url_logout	= getResources().getString(R.string.url_md_live_server)+getResources().getString(R.string.url_user_logout);
		
		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/KabelMediumBT.ttf");
		Button bttn = (Button) view.findViewById(R.id.bttn_menu);
		bttn_logout = (Button) view.findViewById(R.id.bttn_logout);
		bttn_cancel = (Button) view.findViewById(R.id.bttn_cancel);
		
		rotate = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate);
		pBar		= (ImageView) view.findViewById(R.id.progressBar2);
		pBar.setVisibility(View.INVISIBLE);
		text_wana_logOut = (TextView) view.findViewById(R.id.text_wantToLogout);
		TextView text_tittle = (TextView) view.findViewById(R.id.text_tittle);
		text_tittle.setTypeface(tf);
		bttn.setTypeface(tf);
		bttn_logout.setTypeface(tf);
		bttn_cancel.setTypeface(tf);
		text_wana_logOut.setTypeface(tf);
		bttn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				RootFragment rf = (RootFragment) getActivity();
				rf.showSlidingMenu();
			}
		});
		bttn_logout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*	Session session = Session.getActiveSession();
				if (!session.isClosed()) {
					session.closeAndClearTokenInformation();
				}*/

				//sharePref.setUserLogin(false);
				//sharePref.setUserName("");
				if(from_fb){
					try{
						Session.openActiveSession(getActivity(), false, new Session.StatusCallback() {


							// callback when session changes state
							@Override
							public void call(Session session, SessionState state, Exception exception) {

								if(session.isOpened()){	
									session.closeAndClearTokenInformation();
									//exception.printStackTrace();
									//Intent intent = new Intent(getActivity(), LoginScreen.class);
									//sharePref.setUserLogin(false);
									//startActivity(intent);
									//getActivity().finish();
								}
								Log.i("Clearing", "Token");
							}
						});
					}catch(Exception ex){
						Log.i("Log out error", "Log out error");
						ex.printStackTrace();
					}
				}else{
					logoutRequest();
				}
			}


		});



		return view;
	}


	@Override
	public void onStart() {
		super.onStart();
		if(statusCallback!=null){
			Session.getActiveSession().addCallback(statusCallback);
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		Session.getActiveSession().removeCallback(statusCallback);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Session.getActiveSession().onActivityResult(getActivity(), requestCode, resultCode, data);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		Session session = Session.getActiveSession();
		Session.saveSession(session, outState);
	}

	private class SessionStatusCallback implements Session.StatusCallback {
		@Override
		public void call(Session session, SessionState state, Exception exception) {
			//updateView();
			loginBack();
		}
	}


	public void logoutRequest(){
		String 	email	= sharePref.getUserEmail();
		String	token	= sharePref.getUserToken();
		JSONObject jObj = new JSONObject();
		try{
			
			jObj.put("uname", email);
			jObj.put("token", token);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		
		RequestQueue queue = Volley.newRequestQueue(getActivity());
		JsonObjectRequest req = new JsonObjectRequest(Method.POST, url_logout, jObj, new VolleyResponse.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub
				pBar.setVisibility(View.INVISIBLE);
				
				Log.d("Responce", "MDLogout responce "+response.toString());
				
				try{
					String success = response.getString("success");
					if(success.equalsIgnoreCase("true")){
						Toast.makeText(getActivity(), "You have successfully logged out.", 0).show();
						
					}else if(response.getString("message").equalsIgnoreCase("Already logout")){
						Toast.makeText(getActivity(), "You are already logged out.", 0).show();

					}else{
						Toast.makeText(getActivity(), response.getString("message"), 0).show();
					}	
					loginBack();
				}catch(Exception ex){
					
				}
			}
		}, new VolleyResponse.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				pBar.setVisibility(View.GONE);
				Log.d("Error", "MDLogout error "+error.toString());
			}
		});
		pBar.setVisibility(View.VISIBLE);
		queue.add(req);
	}
	
	
	public void loginBack(){
		sharePref.setLoginFromFb(false);
		sharePref.setUserLogin(false);
		sharePref.setUserName("");
		
		Intent intent = new Intent(getActivity(), LoginScreen.class);
		startActivity(intent);
		getActivity().finish();
	}
}
