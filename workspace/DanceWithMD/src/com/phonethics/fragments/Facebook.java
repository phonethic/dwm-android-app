package com.phonethics.fragments;

import com.flurry.android.FlurryAgent;
import com.localytics.android.LocalyticsSession;

import com.phonethics.dancewithmadhuri.R;

import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;



public class Facebook extends Fragment {


	WebView web;
	String url_facebook;
	ImageView img,bckimg,share_btn,bck,forth,refresh;
	ImageView	pBar;
	Animation	rotate;

	private static boolean useLocalytics 		= false;
	private static boolean useFlurry 		= false;
	private LocalyticsSession localyticsSession;
	private static  String LOCALYTICS_APP_KEY	=""; 

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.facebook, null);
		rotate = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate);
		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/KabelMediumBT.ttf");
		Button bttn = (Button) view.findViewById(R.id.bttn_menu);
		web			= (WebView) view.findViewById(R.id.web_view_disclaimer);
		bck 		= (ImageView) view.findViewById(R.id.webback);
		forth 		= (ImageView) view.findViewById(R.id.forth);
		refresh 	= (ImageView) view.findViewById(R.id.refresh);
		pBar		= (ImageView) view.findViewById(R.id.progressBar2);

		useFlurry				= getResources().getBoolean(R.bool.useFlurry);
		useLocalytics			= getResources().getBoolean(R.bool.useLocalytics);
		if(useLocalytics){
			LOCALYTICS_APP_KEY 		= getResources().getString(R.string.LOCALYTICS_APP_KEY);
			this.localyticsSession 	= new LocalyticsSession(getActivity(),LOCALYTICS_APP_KEY);
			this.localyticsSession.open();
			//this.localyticsSession.upload();
			localyticsSession.tagScreen(getResources().getString(R.string.localEvent_tab_facebook));
			this.localyticsSession.upload();
		}


		TextView text_tittle = (TextView) view.findViewById(R.id.text_tittle);
		text_tittle.setTypeface(tf);
		text_tittle.setText("Faceboook");
		bttn.setTypeface(tf);

		url_facebook = getResources().getString(R.string.url_facebook_dwm);
		bttn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				RootFragment rf = (RootFragment) getActivity();
				rf.showSlidingMenu();
			}
		});

		//web.loadUrl(url_facebook);
		web.getSettings().setJavaScriptEnabled(true);
		web.getSettings().setPluginState(PluginState.ON);
		web.getSettings().setUserAgentString("Mozilla/5.0 (Linux; U; Android 2.0; en-us; Droid Build/ESD20) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17");
		//web.getSettings().setBuiltInZoomControls(true);
		web.setWebViewClient(new MyWebViewClient());
		web.loadUrl(url_facebook);



		//web.loadUrl(url_facebook);

		//web.setWebViewClient(new MyWebViewClient());
		return view;
	}


	class MyWebViewClient extends WebViewClient {
		@Override
		// show the web page in webview but not in web browser
		public boolean shouldOverrideUrlLoading(WebView view, String url) {

			try
			{
				/*view.getSettings().setJavaScriptEnabled(true);
				view.getSettings().setPluginsEnabled(true);
				view.getSettings().setPluginState(PluginState.ON);
				view.getSettings().setAllowFileAccess(true);
				view.getSettings().setBuiltInZoomControls(true);
				view.getSettings().setBlockNetworkLoads(false);*/


				view.loadUrl(url);


				bck.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(web.canGoBack())
						{
							web.goBack();
						}
					}
				});

				forth.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(web.canGoForward())
						{
							web.goForward();
						}


					}
				});


				refresh.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						web.reload();

					}
				});


			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}

			return true;
		}



		@Override
		public void onPageFinished(WebView view, String url) {
			// TODO Auto-generated method stub
			super.onPageFinished(view, url);
			pBar.clearAnimation();
			pBar.setVisibility(View.GONE);
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			// TODO Auto-generated method stub
			super.onPageStarted(view, url, favicon);
			pBar.startAnimation(rotate);
			pBar.setVisibility(View.VISIBLE);

		}

		@Override
		public void onLoadResource(WebView view, String url) {
			// TODO Auto-generated method stub
			super.onLoadResource(view, url);

		}


	}



	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if(useFlurry){
			FlurryAgent.onStartSession(getActivity(), getResources().getString(R.string.flurry_id));
			FlurryAgent.logEvent(getResources().getString(R.string.fEvent_tab_facebook));
		}

	}


	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if(useFlurry){
			FlurryAgent.onEndSession(getActivity());
		}


	}



	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(useLocalytics){
			this.localyticsSession.open();
			localyticsSession.tagScreen(getResources().getString(R.string.localEvent_tab_facebook));
		}
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub

		super.onPause();
		if(useLocalytics){
			this.localyticsSession.close();
			this.localyticsSession.upload();
		}
	}





}
