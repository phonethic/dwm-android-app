package com.phonethics.fragments;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.localytics.android.LocalyticsSession;
import com.phonethics.adapters.TweetsAdapter;

import com.phonethics.dancewithmadhuri.R;
import com.phonethics.network.NetworkCheck;

public class Twitter extends Fragment implements OnClickListener {

	String 		HostUrl						=	"";
	String		TweetsUrl					=	"https://api.twitter.com/1.1/search/tweets.json?count=10&q=%23dancewithmadhuri&result_type=mixed";
	//String		consumerKey					=	"xvz1evFS4wEEPTGEFPHBog";
	String		consumerKey					=	"63xdK9qQ3nORq0WOqiXw";
	//String		consumerSecret				=	"L8qq9PZyRg6ieKGEKhZolGC0vJWLw8iEJ88DRdyOg";
	String		consumerSecret				=	"rrIgw30gSajYsPEJJ6Gi6dS9uS8RoS9vzePZlPJq6Qo";
	String		consumerKeyRfcEncoded 		=	consumerKey;
	String		consumerSecretRfcEncoded 	= 	consumerSecret;
	String		bearerToken					=	"",access_token="";
	String		tweetsFile					=	"/sdcard/dwm/tweets.txt";

	public static final int 		progress_bar_type = 0; 
	private ProgressDialog 			pDialog;

	Button		button;
	Context		context;
	ListView	list;

	NetworkCheck				netCheck;
	ArrayList<String> 			tweets_Arr,img_url_Arr,screen_name_arr;
	TweetsAdapter				tweet_adapter;
	Activity					actCtx;

	ImageView	pBar;
	Animation	rotate;
	boolean						callOnStart = true;
	Button						bttn_tweet;
	private static boolean useLocalytics 		= false;
	private static boolean useFlurry 			= false;
	private LocalyticsSession localyticsSession;
	private static  String LOCALYTICS_APP_KEY	=""; 



	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view 			 = inflater.inflate(R.layout.twitter, null);
		Typeface tf 		 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/KabelMediumBT.ttf");
		Button bttn 		 = (Button) view.findViewById(R.id.bttn_menu);
		TextView text_tittle = (TextView) view.findViewById(R.id.text_tittle);
		text_tittle.setText("#DanceWithMadhuri");
		list		 = (ListView) view.findViewById(R.id.list_view);
		netCheck			= new NetworkCheck(getActivity());
		pDialog 			= new ProgressDialog(getActivity());

		rotate = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate);
		pBar		= (ImageView) view.findViewById(R.id.progressBar2);
		pBar.setVisibility(View.VISIBLE);
		pBar.startAnimation(rotate);
		HostUrl = getResources().getString(R.string.url_twitter_host);
		text_tittle.setTypeface(tf);
		bttn.setTypeface(tf);
		tweets_Arr = new ArrayList<String>();
		img_url_Arr = new ArrayList<String>();
		screen_name_arr = new ArrayList<String>();
		bttn_tweet = (Button) view.findViewById(R.id.bttn_send_tweet);
		bttn_tweet.setOnClickListener(this);

		useFlurry			= getResources().getBoolean(R.bool.useFlurry);
		useLocalytics			= getResources().getBoolean(R.bool.useLocalytics);

		if(useLocalytics){
			LOCALYTICS_APP_KEY 		= getResources().getString(R.string.LOCALYTICS_APP_KEY);
			this.localyticsSession 	= new LocalyticsSession(getActivity(),LOCALYTICS_APP_KEY);
			this.localyticsSession.open();
			//this.localyticsSession.upload();
			localyticsSession.tagScreen(getResources().getString(R.string.localEvent_tab_tweets));
			this.localyticsSession.upload();
		}
		
		/*File file = new File(tweetsFile);
		if(file.exists()){
			parseJsonData();
			//bearerToken		= "NjN4ZEs5cVEzbk9ScTBXT3FpWHc6cnJJZ3czMGdTYWpZc1BFSko2R2k2ZFM5dVM4Um9TOXZ6ZVBabFBKcTZRbw==";
			//new AuthenticateApp().execute("");

		}else if(netCheck.isNetworkAvailable()){
			bearerToken		= "NjN4ZEs5cVEzbk9ScTBXT3FpWHc6cnJJZ3czMGdTYWpZc1BFSko2R2k2ZFM5dVM4Um9TOXZ6ZVBabFBKcTZRbw==";
			new AuthenticateApp().execute("");
		}else{
			Toast.makeText(getActivity(), "No Internet Connection", 0).show();
		}*/
		
		bttn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				RootFragment rf = (RootFragment) getActivity();
				rf.showSlidingMenu();
			}
		});

		/*	Handler handler = new Handler();
		Runnable runnable = new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				startOpreation();
			}
		};
		handler.postDelayed(runnable, 200); */


		return view;
	}

	public void startOpreation(){
		File file = new File(tweetsFile);
		if(file.exists()){
			//parseJsonData();
			bearerToken		= "NjN4ZEs5cVEzbk9ScTBXT3FpWHc6cnJJZ3czMGdTYWpZc1BFSko2R2k2ZFM5dVM4Um9TOXZ6ZVBabFBKcTZRbw==";
			new AuthenticateApp().execute("");

		}else if(netCheck.isNetworkAvailable()){
			bearerToken		= "NjN4ZEs5cVEzbk9ScTBXT3FpWHc6cnJJZ3czMGdTYWpZc1BFSko2R2k2ZFM5dVM4Um9TOXZ6ZVBabFBKcTZRbw==";
			new AuthenticateApp().execute("");
		}else{
			Toast.makeText(getActivity(), "No Internet Connection", 0).show();
		}
	}
	public void parseJsonData(){
		Handler handler;
		Runnable runnable;
		tweets_Arr.clear();
		img_url_Arr.clear();
		screen_name_arr.clear();
		File file = new File(tweetsFile);
		if(file.exists()){
			handler = new Handler();
			runnable = new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub


					try{
						FileInputStream filename = new FileInputStream(tweetsFile);
						FileChannel fc = filename.getChannel();

						MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
						String jString = Charset.defaultCharset().decode(bb).toString();
						JSONObject json = new JSONObject(jString);
						JSONArray jArr = json.getJSONArray("statuses");
						for(int i=0;i<jArr.length();i++){
							JSONObject tempJObj = jArr.getJSONObject(i);
							String tweet = tempJObj.getString("text");
							tweets_Arr.add(tweet);

							JSONObject tempUserObj = tempJObj.getJSONObject("user");
							String img_url = tempUserObj.getString("profile_image_url");
							img_url_Arr.add(img_url);

							String name = tempUserObj.getString("screen_name");
							screen_name_arr.add(name);

						}
						filename.close();
						tweet_adapter = new TweetsAdapter(getActivity(), tweets_Arr,img_url_Arr,screen_name_arr);
						list.setAdapter(tweet_adapter);
						//list.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, tweets_Arr));
					}catch(Exception ex){
						ex.printStackTrace();
					}

				}
			};
			handler.postDelayed(runnable, 500);

		}
	}


	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case progress_bar_type:
			pDialog = new ProgressDialog(getActivity());
			pDialog.setIndeterminate(false);
			pDialog.setMessage("Please wait");
			pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			pDialog.setCancelable(true);
			pDialog.show();
			return pDialog;
		default:
			return null;
		}
	}


	public class AuthenticateApp extends AsyncTask<String, String, String>{

		Calendar cal = Calendar.getInstance();
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub

			super.onPreExecute();
			//pDialog.show();
			pBar.setVisibility(View.VISIBLE);
			pBar.startAnimation(rotate);
			Log.d("","MdAsynch -- Pre -- Time -->>"+cal.getTimeInMillis());
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			try{
				HttpClient	httpClient	=	new DefaultHttpClient();
				HttpPost	httpPost	=	new HttpPost(HostUrl);

				httpPost.setHeader("Authorization", "Basic "+bearerToken);
				//httpPost.setHeader("Content-Length", "29");
				httpPost.setHeader("Content-Type","application/x-www-form-urlencoded;charset=UTF-8");

				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
				nameValuePairs.add(new BasicNameValuePair("grant_type", "client_credentials"));
				//nameValuePairs.add(new BasicNameValuePair("Content-Length", "29"));
				//AAAAAAAAAAAAAAAAAAAAACnXSAAAAAAADqKcLqqsLtQ%2Fjgw2nbOONUwkeQQ%3DafhBX4nYaV4K6c3Z1ul8eUQlBS3L3qvRRtJUAP4

				UrlEncodedFormEntity urlen	=	new UrlEncodedFormEntity(nameValuePairs,HTTP.UTF_8);
				httpPost.setEntity(urlen);

				HttpResponse response 	= httpClient.execute(httpPost);
				//String status = response.getEntity().getContent().toString();
				//Log.d("Status", "Status " + status);


				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				StringBuffer stringBuffer = new StringBuffer("");
				String line = "";
				String LineSeparator = System.getProperty("line.separator");
				while ((line = bufferedReader.readLine()) != null) {
					stringBuffer.append(line + LineSeparator); 

				}
				bufferedReader.close();
				//Log.i("Response : ", " Response "+stringBuffer.toString());


				JSONObject jobj = new JSONObject(stringBuffer.toString());
				access_token 	= jobj.getString("access_token");

				Log.i("==", "Response "+access_token);


			}catch(Exception ex){
				ex.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.d("","MdAsynch -- Post -- Time -->>"+cal.getTimeInMillis());
			GetTweets getTweets = new GetTweets();
			getTweets.execute("");
			//removeDialog(progress_bar_type);
		}

	}


	public class GetTweets extends AsyncTask<String, String, String>{

		int count = 0;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}


		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub

			try{
				HttpClient	httpClient	=	new DefaultHttpClient();
				HttpGet		httpGet		=	new HttpGet(TweetsUrl);
				httpGet.setHeader("Authorization", "Bearer "+access_token);

				HttpResponse response 	= httpClient.execute(httpGet);
				String status 			= response.getStatusLine().toString();
				Log.d("Status", "Status " + status);


				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				StringBuffer stringBuffer = new StringBuffer("");
				String line = "";
				String LineSeparator = System.getProperty("line.separator");
				while ((line = bufferedReader.readLine()) != null) {
					stringBuffer.append(line + LineSeparator); 

				}
				bufferedReader.close();
				Log.i("Response Tweets: ", "Response Tweets"+stringBuffer.toString());


				InputStream input = new ByteArrayInputStream(stringBuffer.toString().getBytes());


				// Output stream to write file

				try{
					File myDirectory = new File(Environment.getExternalStorageDirectory(), "dwm");
					if(!myDirectory.exists()) {                                 
						myDirectory.mkdirs();
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}

				FileOutputStream output = new FileOutputStream(tweetsFile);
				byte data[] = new byte[1024];
				long total = 0;
				while (   ((count = input.read(data)) != -1)  ) {
					total += count;
					output.write(data, 0, count);
				}
				output.flush();
				output.close();
				input.close();

			}catch(Exception ex){
				ex.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			//pDialog.dismiss();
			pBar.setVisibility(View.GONE);

			pBar.clearAnimation();
			parseJsonData();
		}


	}


	public String convetToBase64(String str){

		String bsStr = "";
		try {
			byte[] byteArray = str.getBytes("UTF-8");
			bsStr = Base64.encodeToString(byteArray, Base64.DEFAULT);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Log.d("bsstr", "bsstr "+bsStr);
		return bsStr;
	}





	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if(useFlurry){
			FlurryAgent.onStartSession(getActivity(), getResources().getString(R.string.flurry_id));
			FlurryAgent.logEvent(getResources().getString(R.string.fEvent_tab_tweets));
		}
		if(callOnStart){
			callOnStart = false;
			Handler handler = new Handler();
			Runnable runnable = new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					startOpreation();
				}
			};
			handler.postDelayed(runnable, 200); 
		}
	}




	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if(useFlurry){
			FlurryAgent.onEndSession(getActivity());
		}
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==(R.id.bttn_send_tweet)){
			boolean installed  =   appInstalledOrNot("com.twitter.android");
			if(installed)
			{
				//System.out.println("App already installed om your phone");
				//Toast.makeText(context, "Installed", 0).show();

				try{


					//FlurryAgent.logEvent("NSFL_Tweet_New");
					Intent  intent = new Intent(Intent.ACTION_SEND);
					intent.putExtra(Intent.EXTRA_TEXT, "#DanceWithMadhuri");
					intent.setType("text/plain");
					final PackageManager pm = getActivity().getPackageManager();
					final List activityList = pm.queryIntentActivities(intent, 0);
					int len =  activityList.size();
					for (int i = 0; i < len; i++) {
						final ResolveInfo app = (ResolveInfo) activityList.get(i);
						if ("com.twitter.android.PostActivity".equals(app.activityInfo.name)) {
							final ActivityInfo activity=app.activityInfo;
							final ComponentName name=new ComponentName(activity.applicationInfo.packageName, activity.name);
							intent=new Intent(Intent.ACTION_SEND);
							intent.addCategory(Intent.CATEGORY_LAUNCHER);
							intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
							intent.setComponent(name);
							intent.putExtra(Intent.EXTRA_TEXT, "#DanceWithMadhuri ");
							getActivity().startActivity(intent);
							break;
						}
					}
				}
				catch(final ActivityNotFoundException e) {
					Log.i("NotInstalled", "no twitter native",e );
				}

			}
			else
			{
				//System.out.println("App is not installed om your phone");
				//Toast.makeText(context, "App is not installed", 0).show();

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
				alertDialogBuilder.setTitle("Twitter App");
				alertDialogBuilder
				.setMessage("Twitter Application is not installed. Would you like to install it?")
				.setCancelable(false)
				.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {

						//Dealer_location.this.finish();
						/*Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.twitter.android"));
						startActivity(intent);*/

						try {
							startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.twitter.android")));
						} catch (android.content.ActivityNotFoundException anfe) {
							startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.twitter.android")));
						}
					}
				})
				.setNegativeButton("No",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

				AlertDialog alertDialog = alertDialogBuilder.create();

				alertDialog.show();

			}


		}
	}
	
	
	
	private boolean appInstalledOrNot(String uri)
	{
		PackageManager pmngr = getActivity().getPackageManager();
		boolean app_installed = false;
		try
		{
			pmngr.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
			app_installed = true;
		}
		catch (PackageManager.NameNotFoundException e)
		{
			app_installed = false;
		}
		return app_installed ;
	}


}
