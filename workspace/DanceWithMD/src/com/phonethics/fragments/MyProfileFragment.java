package com.phonethics.fragments;

import java.io.File;
import java.util.ArrayList;
import java.util.Formatter.BigDecimalLayoutForm;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyResponse;
import com.android.volley.toolbox.HurlStack;

import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.flurry.android.FlurryAgent;
import com.localytics.android.LocalyticsSession;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.phonethics.adapters.BadgesAdapter;
import com.phonethics.circularimageview.CircularImageView;

import com.phonethics.customclass.UserDetails;
import com.phonethics.dancewithmadhuri.LeaderBoard;
import com.phonethics.dancewithmadhuri.R;

import com.phonethics.network.NetworkCheck;
import com.phonethics.sharepref.SharePref;

public class MyProfileFragment extends Fragment implements OnClickListener  {



	Button		bttn_leadBoard,bttn_downloadVideos;
	TextView	text_rank,text_userRank,
	text_points,text_pointsUser,
	text_bages,text_badgeUser,
	text_specialBadges,text_specialBadgesUser,
	text_dancers,text_dancersUser,text_userName,text_stats,text_summary;
	UserDetails userDetails;
	String		emailId;
	static String		url_dwm = "";
	static String		url_rank;
	static String		url_myBadge;
	static String		url_totalDancers;
	SharePref	share;
	RequestQueue queue;


	ImageView	pBar;
	Animation	rotate;
	static NetworkCheck netCheck;


	String		urank="-";
	String		uname="-";
	String		upoints="-";
	String		ubadges="-";
	String		uspecial_badges="-";
	String		udancers="-";
	String		utotal_badges="-";
	String		utotal_special_badges="-";

	ArrayList<String> badgesImage_arr,badgeName_arr,badgeCount_arr;
	ListView		list_badges;
	LinearLayout	linear_stats,linear_list;
	ImageView		imgTrans1,imgTrans2,imgTrans3,imgTrans4,imgTrans5,imgTrans6;
	ImageView img_profile;
	ImageLoader		imageLoader;
	View 			view;
	Float			transValue = 2f;
	static int				alphaValue = 120;
	boolean					callOnRestart = true;
	boolean					setImage = false;

	LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT,0.7f);
	LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT,1f);


	DisplayImageOptions options;
	ImageLoaderConfiguration config;

	private static boolean useLocalytics 		= false;
	private static boolean useFlurry 			= false;
	private LocalyticsSession localyticsSession;
	private static  String LOCALYTICS_APP_KEY	="";



	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		callOnRestart = true;
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		view = inflater.inflate(R.layout.my_profile, null);
		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/KabelMediumBT.ttf");
		share							= new SharePref(getActivity());
		if(share.isLoginFromFb()){
			emailId					= share.getUserId();
		}else{
			emailId					= share.getUserEmail();
		}

		useFlurry				= getResources().getBoolean(R.bool.useFlurry);
		useLocalytics			= getResources().getBoolean(R.bool.useLocalytics);

		if(useLocalytics){
			LOCALYTICS_APP_KEY 		= getResources().getString(R.string.LOCALYTICS_APP_KEY);
			this.localyticsSession 	= new LocalyticsSession(getActivity(),LOCALYTICS_APP_KEY);
			this.localyticsSession.open();
			//this.localyticsSession.upload();
			localyticsSession.tagScreen(getResources().getString(R.string.localEvent_tab_my_profile));
			this.localyticsSession.upload();
		}

		Button bttn 					= (Button) view.findViewById(R.id.bttn_menu);
		TextView text_tittle 			= (TextView) view.findViewById(R.id.text_tittle);

		text_rank						= (TextView) view.findViewById(R.id.text_rank);
		text_userRank					= (TextView) view.findViewById(R.id.text_rank_users);

		text_points						= (TextView) view.findViewById(R.id.text_points);
		text_pointsUser					= (TextView) view.findViewById(R.id.text_points_users);

		text_bages						= (TextView) view.findViewById(R.id.text_badges);
		text_badgeUser					= (TextView) view.findViewById(R.id.text_badges_users);

		text_specialBadges				= (TextView) view.findViewById(R.id.text_special_badges);
		text_specialBadgesUser			= (TextView) view.findViewById(R.id.text_special_badges_users);

		text_dancers					= (TextView) view.findViewById(R.id.text_dancers);
		text_dancersUser				= (TextView) view.findViewById(R.id.text_dancers_users);

		text_stats						= (TextView) view.findViewById(R.id.text_stats);
		text_summary					= (TextView) view.findViewById(R.id.text_summary);

		text_userName					= (TextView) view.findViewById(R.id.text_user_name);
		bttn_leadBoard					= (Button) view.findViewById(R.id.bttn_leader_board);
		bttn_downloadVideos				= (Button) view.findViewById(R.id.bttn_download);
		list_badges						= (ListView) view.findViewById(R.id.list_badge);
		list_badges.setCacheColorHint(0);

		img_profile						= (ImageView) view.findViewById(R.id.img_profile);

		//imgTrans1						= (ImageView) view.findViewById(R.id.img_trans_stat_and_batch);
		/*imgTrans2						= (ImageView) view.findViewById(R.id.img_trans_rank);
		imgTrans3						= (ImageView) view.findViewById(R.id.img_trans_points);
		imgTrans4						= (ImageView) view.findViewById(R.id.img_trans_badges);
		imgTrans5						= (ImageView) view.findViewById(R.id.img_tran_specialBadge);
		imgTrans6						= (ImageView) view.findViewById(R.id.img_tran_dancers);*/

		//imageLoader						= new ImageLoader(getActivity());
		imageLoader = ImageLoader.getInstance();
		config= new ImageLoaderConfiguration.Builder(getActivity())

		.denyCacheImageMultipleSizesInMemory()
		.threadPoolSize(4)

		.discCache(new UnlimitedDiscCache(new File(android.os.Environment.getExternalStorageDirectory(),"dwm")))
		.build();
		imageLoader.init(config);
		options = new DisplayImageOptions.Builder()
		.cacheOnDisc(true)

		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		.displayer(new RoundedBitmapDisplayer(10))
		.showStubImage(R.drawable.unknown)
		.build();

		imageLoader.init(config);


		//imageLoader.setStubId(R.drawable.unknown);
		//imageLoader.setRoundedCorner(true, 10);

		//img_profile.setBackgroundResource(R.drawable.unknown);

		linear_stats					= (LinearLayout) view.findViewById(R.id.linear_stats);
		linear_list						= (LinearLayout) view.findViewById(R.id.liner_list);
		linear_list.setVisibility(View.GONE);
		linear_stats.setVisibility(View.VISIBLE);

		rotate 							= AnimationUtils.loadAnimation(getActivity(), R.anim.rotate);
		pBar							= (ImageView) view.findViewById(R.id.progressBar2);
		pBar.startAnimation(rotate);
		pBar.setVisibility(View.VISIBLE);

		netCheck						= new NetworkCheck(getActivity());

		userDetails						= new UserDetails();

		badgesImage_arr					= new ArrayList<String>();
		badgeName_arr					= new ArrayList<String>();
		badgeCount_arr 					= new ArrayList<String>();
		text_tittle.setText("My Profile");
		text_tittle.setTypeface(tf);
		bttn.setTypeface(tf);

		text_rank.setTypeface(tf);
		text_userRank.setTypeface(tf);
		text_points.setTypeface(tf);
		text_pointsUser.setTypeface(tf);
		text_bages.setTypeface(tf);
		text_badgeUser.setTypeface(tf);
		text_specialBadges.setTypeface(tf);
		text_specialBadgesUser.setTypeface(tf);
		text_dancers.setTypeface(tf);
		text_dancersUser.setTypeface(tf);
		bttn_leadBoard.setTypeface(tf);
		bttn_downloadVideos.setTypeface(tf);
		text_stats.setTypeface(tf);
		text_summary.setTypeface(tf);
		text_userName.setTypeface(tf);

		text_rank.setText("Rank");
		text_points.setText("Points");
		text_bages.setText("Badges");
		text_specialBadges.setText("Special Badges");
		text_dancers.setText("Dancers");

		text_userName.setText("Welcome \n"+share.getUserName());



		url_dwm							= getResources().getString(R.string.url_md_live_server);
		url_rank						= url_dwm + getResources().getString(R.string.url_my_points_rank);
		url_myBadge						= url_dwm + getResources().getString(R.string.url_my_badges);
		url_totalDancers				= url_dwm + getResources().getString(R.string.url_total_dancers);
		queue							= Volley.newRequestQueue(getActivity());


		bttn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				RootFragment rf = (RootFragment) getActivity();
				rf.showSlidingMenu();
			}
		});


		text_stats.setOnClickListener(this);
		text_summary.setOnClickListener(this);
		bttn_leadBoard.setOnClickListener(this);

		/*text_stats.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				linear_stats.setVisibility(View.VISIBLE);
				linear_list.setVisibility(View.GONE);
				text_stats.setBackgroundResource(R.drawable.button_purple);
				//text_summary.setBackgroundResource(getResources().getColor(Color.TRANSPARENT));
				text_summary.setBackgroundColor(Color.TRANSPARENT);
				text_stats.setLayoutParams(params1);
				text_summary.setLayoutParams(params2);

				text_stats.setTextColor(Color.WHITE);
				text_summary.setTextColor(Color.parseColor("#9e9e9e"));

			}
		});
		text_summary.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linear_stats.setVisibility(View.GONE);
				linear_list.setVisibility(View.VISIBLE);
				text_summary.setBackgroundResource(R.drawable.button_purple);
				//text_stats.setBackgroundResource(getResources().getColor(Color.TRANSPARENT));
				text_stats.setBackgroundColor(Color.TRANSPARENT);

				text_stats.setLayoutParams(params2);
				text_summary.setLayoutParams(params1);

				text_summary.setTextColor(Color.WHITE);
				text_stats.setTextColor(Color.parseColor("#9e9e9e"));
			}
		});

		bttn_leadBoard.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getActivity(), LeaderBoard.class);
				intent.putExtra("userName", share.getUserName());
				intent.putExtra("dancers", udancers);
				intent.putExtra("points", upoints);
				intent.putExtra("rank", urank);
				startActivity(intent);
				getActivity().overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});*/

		/*setAlphaForView(imgTrans1, 5f);
		setAlphaForView(imgTrans2, transValue);
		setAlphaForView(imgTrans3, transValue);
		setAlphaForView(imgTrans4, transValue);
		setAlphaForView(imgTrans5,transValue);
		setAlphaForView(imgTrans6,transValue);*/


		//imgTrans1.getBackground().setAlpha(168);
		//imgTrans2.getBackground().setAlpha(alphaValue);
		//imgTrans3.getBackground().setAlpha(alphaValue);
		//imgTrans4.getBackground().setAlpha(alphaValue);
		//imgTrans5.getBackground().setAlpha(alphaValue);
		//imgTrans6.getBackground().setAlpha(alphaValue);
		/*
		Handler handler = new Handler();
		Runnable runnable = new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				start();
			}
		};
		handler.postDelayed(runnable, 800);*/

		return view;
	}






	public void start(){
		if(netCheck.isNetworkAvailable()){
			getMyBadgeRequest(url_myBadge+emailId);
			getRankAndpointsRequest(url_rank+emailId);
			getTotalDancers(url_totalDancers+emailId);
		}else{
			pBar.clearAnimation();
			pBar.setVisibility(View.GONE);
			showToast("No Internet Connection!");
		}
	}


	public void getRankAndpointsRequest(String url){
		Log.d("", "MdRankAndpointsRequest Method "+url);
		///RequestQueue queue							= Volley.newRequestQueue(getActivity());
		JsonObjectRequest request = new JsonObjectRequest(Method.GET, url, null, new VolleyResponse.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub
				try{
					Log.d("", "MdBabgesRank Success "+response.toString());

					boolean success = response.getBoolean("success");
					if(success){
						JSONArray jArr = response.getJSONArray("data");
						for(int i=0; i<jArr.length();i++){
							JSONObject obj = jArr.getJSONObject(jArr.length()-1);
							String name = obj.getString("User_Name");
							String rank = obj.getString("rank");
							String points = obj.getString("points");
							String User_ID = obj.getString("User_ID");

							userDetails.setName(name);
							userDetails.setPoints(points);
							userDetails.setRank(rank);

							uname 	= name;
							upoints = points;
							urank 	= rank;
						}
						//getTotalDancers(url_totalDancers+emailId);
						setTextData();
					}else{
						showToast("Some Network Error!");
					}

				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
		}, new VolleyResponse.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				try{
					Log.d("", "MdBabgesRank error "+error.toString());
					showToast("Network Error!");
					pBar.clearAnimation();
					pBar.setVisibility(View.GONE);

				}catch(Exception ex){

				}
			}
		});
		queue.add(request);
	}



	public void getMyBadgeRequest(String url){
		badgeName_arr.clear();
		badgesImage_arr.clear();
		badgeCount_arr.clear();
		Log.d("", "MdBabgesResponse Method "+url);
		RequestQueue queue							= Volley.newRequestQueue(getActivity());
		JsonObjectRequest request = new JsonObjectRequest(Method.GET, url, null, new VolleyResponse.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub
				try{
					Log.d("", "MdBabgesResponse Success "+response.toString());
					String success = response.getString("success");
					if(success.equalsIgnoreCase("true")){
						JSONArray jArr = response.getJSONArray("data");
						for(int i=0; i<jArr.length();i++){
							JSONObject obj;
							obj 	= jArr.getJSONObject(i);
							if(i==0){

								String badges 	= obj.getString("badge");
								String spbadge 	= obj.getString("spbadge");

								userDetails.setBadges(badges);
								userDetails.setSpecial_badges(spbadge);
								userDetails.setTotal_badges("8");
								userDetails.setTotal_special_badges("4");

								ubadges 				= badges;
								uspecial_badges 		= spbadge;
								utotal_badges			= "8";
								utotal_special_badges 	= "4";
							}else{
								badgeName_arr.add(obj.getString("badge_name"));
								badgesImage_arr.add(obj.getString("image"));
								int count = obj.getInt("count");
								badgeCount_arr.add(""+count);
							}

						}

						//getRankAndpointsRequest(url_rank+emailId);
						setTextData();


					}

				}catch(Exception ex){
					ex.printStackTrace();
				}

			}
		}, new VolleyResponse.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				try{
					Log.d("", "MdBabgesError error "+error.toString());
					showToast("Network Error!");
					pBar.clearAnimation();
					pBar.setVisibility(View.GONE);

				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
		});
		queue.add(request);
	}


	public void getTotalDancers(String url){
		Log.d("", "MdDancersResponse Method "+url);
		RequestQueue queue							= Volley.newRequestQueue(getActivity());
		JsonObjectRequest request = new JsonObjectRequest(Method.GET, url, null, new VolleyResponse.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub
				try{
					Log.d("", "MdDancersResponse Success "+response.toString());
					String success = response.getString("success");
					if(success.equalsIgnoreCase("true")){
						JSONObject obj = response.getJSONObject("data");
						String	counter = obj.getString("count");
						userDetails.setDancers(counter);

						udancers = counter;
						setTextData();
						//showToast("Success");
					}


				}catch(Exception ex){
					ex.printStackTrace();
				}


			}
		}, new VolleyResponse.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				try{
					Log.d("", "MdDancersError error "+error.toString());
					showToast("Network Error!");
					pBar.clearAnimation();
					pBar.setVisibility(View.GONE);

				}catch(Exception ex){

				}
			}
		});
		queue.add(request);
	}


	public void setTextData(){
		Log.d("", "MdTextData Method ");
		pBar.clearAnimation();
		pBar.setVisibility(View.GONE);
		String 		rank 	= userDetails.getRank();
		String		points	= userDetails.getPoints();
		String		badges	= userDetails.getBadges()+"/"+userDetails.getTotal_badges();
		String		specialBadge = userDetails.getSpecial_badges()+"/"+userDetails.getTotal_special_badges();
		String		dancers	= userDetails.getDancers();

		text_userRank					= (TextView) view.findViewById(R.id.text_rank_users);

		text_userRank.setText(urank);
		text_pointsUser.setText(upoints);
		text_badgeUser.setText(ubadges+"/"+utotal_badges);
		text_specialBadgesUser.setText(uspecial_badges+"/"+utotal_special_badges);
		text_dancersUser.setText(udancers);
		if(share.isLoginFromFb()){
			if(!setImage){
				imageLoader.displayImage("http://graph.facebook.com/"+emailId+"/picture?type=large", img_profile,options);
				setImage = true;
			}
		}else{
			if(!setImage){
				imageLoader.displayImage(url_dwm+"images/unknown_person.jpg", img_profile,options);
				setImage = true;
			}

		}
		BadgesAdapter adapter = new BadgesAdapter(getActivity(), badgeName_arr,badgesImage_arr , badgeCount_arr);
		list_badges.setAdapter(adapter);

	}




	public void showToast(String content){
		try{
			Toast.makeText(getActivity(), content, 0).show();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	private void setAlphaForView(View v, float alpha) {
		AlphaAnimation animation = new AlphaAnimation(alpha, alpha);
		animation.setDuration(0); 
		animation.setFillAfter(true); 
		v.startAnimation(animation);
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if(callOnRestart){
			start();
			callOnRestart = false;
		}
		if(useFlurry){
			FlurryAgent.onStartSession(getActivity(), getResources().getString(R.string.flurry_id));
			FlurryAgent.logEvent(getResources().getString(R.string.fEvent_tab_my_profile));
		}

	}


	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if(useFlurry){
			FlurryAgent.onEndSession(getActivity());
		}


	}





	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		callOnRestart = false;
		if(useLocalytics){
			this.localyticsSession.open();
		}
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.text_stats:
			linear_stats.setVisibility(View.VISIBLE);
			linear_list.setVisibility(View.GONE);
			text_stats.setBackgroundResource(R.drawable.button_purple);
			text_summary.setBackgroundResource(R.drawable.button_back);
			//text_summary.setBackgroundColor(Color.TRANSPARENT);
			//text_stats.setLayoutParams(params1);
			//text_summary.setLayoutParams(params2);

			text_stats.setTextColor(Color.WHITE);
			text_summary.setTextColor(Color.parseColor("#9e9e9e"));
			break;


		case R.id.text_summary:
			linear_stats.setVisibility(View.GONE);
			linear_list.setVisibility(View.VISIBLE);
			text_summary.setBackgroundResource(R.drawable.button_purple);
			text_stats.setBackgroundResource(R.drawable.button_back);
			//text_stats.setBackgroundColor(Color.TRANSPARENT);

			//text_stats.setLayoutParams(params2);
			//text_summary.setLayoutParams(params1);

			text_summary.setTextColor(Color.WHITE);
			text_stats.setTextColor(Color.parseColor("#9e9e9e"));
			break;


		case R.id.bttn_leader_board:
			if(netCheck.isNetworkAvailable()){
				Intent intent = new Intent(getActivity(), LeaderBoard.class);
				intent.putExtra("userName", share.getUserName());
				intent.putExtra("dancers", udancers);
				intent.putExtra("points", upoints);
				intent.putExtra("rank", urank);
				startActivity(intent);
				getActivity().overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}else{
				showToast("No Internet Connection");
				pBar.setVisibility(View.GONE);
				pBar.clearAnimation();
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub

		super.onPause();
		if(useLocalytics){
			this.localyticsSession.close();
			this.localyticsSession.upload();
		}
	}



}

