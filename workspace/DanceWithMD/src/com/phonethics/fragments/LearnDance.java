package com.phonethics.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyResponse;
import com.android.volley.VolleyResponse.ErrorListener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.flurry.android.FlurryAgent;
import com.localytics.android.LocalyticsSession;
import com.phonethics.adapters.CategoriesAdapter;
import com.phonethics.customclass.CategoryDetails;

import com.phonethics.dancewithmadhuri.DanceCateogary;
import com.phonethics.dancewithmadhuri.R;
import com.phonethics.network.NetworkCheck;
import com.phonethics.sharepref.SharePref;

public class LearnDance extends Fragment implements OnItemClickListener{



	String							url_dwm;
	String							url_songs;
	//ArrayList<SongInfo> 		songInfoArr;
	ArrayList<CategoryDetails> 		categoryInfoArr;
	CategoryDetails					categoryInfo;
	RequestQueue 					queue;


	ImageView	pBar;
	Animation	rotate;
	ListView						listView;
	NetworkCheck					netCheck;
	Map<String, String> 			flurryEeventParams;
	boolean							onlyOnStart = true;
	SharePref						pref;
	


private static boolean useLocalytics 		= false;
private static boolean useFlurry 		= false;
private LocalyticsSession localyticsSession;
private static  String LOCALYTICS_APP_KEY	="";


	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		View view = inflater.inflate(R.layout.learn_dance, null);
		rotate = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate);
		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/KabelMediumBT.ttf");
		Button bttn = (Button) view.findViewById(R.id.bttn_menu);
		TextView text_tittle = (TextView) view.findViewById(R.id.text_tittle);
		text_tittle.setText("Learn Dance");
		text_tittle.setTypeface(tf);
		bttn.setTypeface(tf);

		pref					= new SharePref(getActivity());
		
		
		useFlurry				= getResources().getBoolean(R.bool.useFlurry);
		useLocalytics			= getResources().getBoolean(R.bool.useLocalytics);
		
		if(useLocalytics){
			LOCALYTICS_APP_KEY 		= getResources().getString(R.string.LOCALYTICS_APP_KEY);
			this.localyticsSession 	= new LocalyticsSession(getActivity(),LOCALYTICS_APP_KEY);
			this.localyticsSession.open();
			//this.localyticsSession.upload();
			localyticsSession.tagScreen(getResources().getString(R.string.localEvent_tab_learn_dance));
			localyticsSession.setCustomerData("Name", pref.getUserName());
			localyticsSession.setCustomerData("Email", pref.getUserEmail());
			localyticsSession.setCustomerName(pref.getUserName());
			localyticsSession.setCustomerEmail(pref.getUserEmail());
			this.localyticsSession.upload();
		}

		netCheck			= new NetworkCheck(getActivity());
		flurryEeventParams	= new HashMap<String, String>();
		url_dwm 		= getResources().getString(R.string.url_md_live_server);
		url_songs		= url_dwm + getResources().getString(R.string.url_songs_cateogories);
		categoryInfoArr	= new ArrayList<CategoryDetails>();
		queue			= Volley.newRequestQueue(getActivity());
		pBar		= (ImageView) view.findViewById(R.id.progressBar2);
		listView		= (ListView) view.findViewById(R.id.list_learn_dance);
		listView.setCacheColorHint(0);

		bttn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				RootFragment rf = (RootFragment) getActivity();
				rf.showSlidingMenu();
			}
		});



		/*listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				if( (netCheck.isNetworkAvailable()) && (categoryInfoArr!=null)	){
					String category_id = categoryInfoArr.get(pos).getStyle_id();
					String style_name = categoryInfoArr.get(pos).getStyle_name();
					Intent intent = new Intent(getActivity(), DanceCateogary.class);
					intent.putExtra("category_id", category_id);
					intent.putExtra("style_name", style_name);
					startActivity(intent);
					getActivity().overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}else{
					showToast("No Internet Connetion");
				}
			}
		});*/

		listView.setOnItemClickListener(this);

		return view;
	}


	public void getSongs(String url){
		pBar.startAnimation(rotate);
		pBar.setVisibility(View.VISIBLE);
		Log.d("", "MdCategoryUrl "+url);


		VolleyResponse.Listener<JSONObject> listener = new VolleyResponse.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub
				Log.d("Responce", "MdSongsResponce success "+ response.toString());
				try{
					String success = response.getString("success");
					if(success.equalsIgnoreCase("true")){
						ParseJsonData(response);
					}

				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
		};

		VolleyResponse.ErrorListener errorListener = new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				Log.d("Error", "MdSongsResponce error "+ error.toString());
				pBar.clearAnimation();
				pBar.setVisibility(View.INVISIBLE);
				showToast("Network Error!");

			}
		};

		JsonObjectRequest req = new JsonObjectRequest(Method.GET, url, null, listener, errorListener);
		queue.add(req);

	}






	public void ParseJsonData(JSONObject jsonObj){
		try{
			JSONArray jArr = jsonObj.getJSONArray("data");
			for(int i =0;i<jArr.length();i++){
				categoryInfo = new CategoryDetails();
				JSONObject temObj = jArr.getJSONObject(i);
				categoryInfo.setStyle_id(temObj.getString("Style_ID"));
				categoryInfo.setStyle_name(temObj.getString("Style_Name"));
				categoryInfo.setThumbnail(temObj.getString("Thumbnail"));
				categoryInfo.setVideo_link(temObj.getString("Video_Link"));
				categoryInfo.setImage_link(temObj.getString("ImageLink"));
				categoryInfo.setCategory(temObj.getString("Category"));

				if(categoryInfo.getStyle_name().equalsIgnoreCase("Jhalak")){

				}else{
					categoryInfoArr.add(categoryInfo);	
				}

			}

			listView.setAdapter(new CategoriesAdapter(getActivity(), categoryInfoArr));
			pBar.clearAnimation();
			pBar.setVisibility(View.INVISIBLE);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}



	public void showToast(String content){
		try{
			Toast.makeText(getActivity(), content, 0).show();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if(useFlurry){
			FlurryAgent.onStartSession(getActivity(), getResources().getString(R.string.flurry_id));
			FlurryAgent.logEvent(getResources().getString(R.string.fEvent_tab_learn_dance));
		}
		if(onlyOnStart){
			onlyOnStart = false;
			if(netCheck.isNetworkAvailable()){
				getSongs(url_songs);
			}else{
				pBar.setVisibility(View.GONE);
				pBar.clearAnimation();
				showToast("No Internet Connection");
			}
		}

	}


	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if(useFlurry){
			FlurryAgent.onEndSession(getActivity());
		}


	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int pos, long arg3) {
		// TODO Auto-generated method stub
		if( (netCheck.isNetworkAvailable()) && (categoryInfoArr!=null)	){
			
			String category_id = categoryInfoArr.get(pos).getStyle_id();
			String style_name = categoryInfoArr.get(pos).getStyle_name();
			Intent intent = new Intent(getActivity(), DanceCateogary.class);
			intent.putExtra("category_id", category_id);
			intent.putExtra("style_name", style_name);
			startActivity(intent);
			getActivity().overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}else{
			showToast("No Internet Connetion");
		}
	}
	
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(useLocalytics){
			this.localyticsSession.open();
			localyticsSession.tagScreen(getResources().getString(R.string.localEvent_tab_learn_dance));
		}
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub

		super.onPause();
		if(useLocalytics){
			this.localyticsSession.close();
			this.localyticsSession.upload();
		}
	}

}
