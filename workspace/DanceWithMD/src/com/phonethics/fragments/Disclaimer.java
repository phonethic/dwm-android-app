package com.phonethics.fragments;

import com.flurry.android.FlurryAgent;
import com.localytics.android.LocalyticsSession;

import com.phonethics.dancewithmadhuri.R;

import android.content.ContextWrapper;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;



public class Disclaimer extends Fragment {


	WebView web;
	String	url_disclaimer;
	String	url_dwm;


	private static boolean useLocalytics 		= false;
	private static boolean useFlurry 		= false;
	private LocalyticsSession localyticsSession;
	private static  String LOCALYTICS_APP_KEY	=""; 

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.disclaimer, null);
		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/KabelMediumBT.ttf");
		Button bttn = (Button) view.findViewById(R.id.bttn_menu);
		web			= (WebView) view.findViewById(R.id.web_view_disclaimer);
		web.getSettings();
		
		//web.setBackgroundColor(Color.argb(1, 255, 255, 255));
		//web.setBackgroundColor(0x00000000);
		float screenDensity = getResources().getDisplayMetrics().density;
		//Toast.makeText(getActivity(), ""+screenDensity,0).show();
		if(screenDensity<2.0){
			web.setBackgroundColor(Color.BLACK);
			Log.d("", "Md>>Screen Density >> less than 2.0 >> "+screenDensity);
		}else{
			web.setBackgroundColor(Color.TRANSPARENT);
			Log.d("", "Md>>Screen Density >> more than 2.0 >> "+screenDensity);
		}

		useFlurry				= getResources().getBoolean(R.bool.useFlurry);
		useLocalytics			= getResources().getBoolean(R.bool.useLocalytics);

		if(useLocalytics){
			LOCALYTICS_APP_KEY 		= getResources().getString(R.string.LOCALYTICS_APP_KEY);
			this.localyticsSession 	= new LocalyticsSession(getActivity(),LOCALYTICS_APP_KEY);
			this.localyticsSession.open();
			//this.localyticsSession.upload();
			localyticsSession.tagScreen(getResources().getString(R.string.localEvent_tab_disclaimer));
			this.localyticsSession.upload();
		}
		if(useFlurry){
			FlurryAgent.logEvent(getResources().getString(R.string.fEvent_tab_disclaimer));
		}
		

		//web.getBackground().setAlpha(100);

		TextView text_tittle = (TextView) view.findViewById(R.id.text_tittle);
		text_tittle.setTypeface(tf);
		text_tittle.setText("Disclaimer");
		bttn.setTypeface(tf);

		url_dwm = getResources().getString(R.string.url_md_live_server);
		url_disclaimer =url_dwm+ getResources().getString(R.string.url_disclaimer);
		bttn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				RootFragment rf = (RootFragment) getActivity();
				rf.showSlidingMenu();
			}
		});

		web.loadUrl(url_disclaimer);
		return view;
	}


	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if(useFlurry	){
			FlurryAgent.onStartSession(getActivity(), getResources().getString(R.string.flurry_id));
			
		}

	}


	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if(useFlurry){
			FlurryAgent.onEndSession(getActivity());
		}


	}


	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(useLocalytics){
			this.localyticsSession.open();
			localyticsSession.tagScreen(getResources().getString(R.string.localEvent_tab_disclaimer));
			
		}
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub

		super.onPause();
		if(useLocalytics){
			this.localyticsSession.close();
			this.localyticsSession.upload();
		}
	}


}
