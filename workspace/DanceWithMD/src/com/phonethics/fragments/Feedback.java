package com.phonethics.fragments;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyResponse;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.flurry.android.FlurryAgent;
import com.localytics.android.LocalyticsSession;
import com.phonethics.adapters.FeedbackAdapter;
import com.phonethics.adapters.TweetsAdapter;
import com.phonethics.customclass.FeedbackInfo;

import com.phonethics.dancewithmadhuri.R;
import com.phonethics.imageloader.ImageLoader;
import com.phonethics.network.NetworkCheck;

public class Feedback extends Fragment implements OnClickListener,OnItemClickListener {

	ListView 		list_feedback;
	String 			feedback_url		= "";
	String			feedbackFile		=	"/sdcard/dwm/feedback.txt";
	NetworkCheck				netCheck;
	ArrayList<String> 			tweets_Arr,img_url_Arr,screen_name_arr;
	FeedbackAdapter				tweet_adapter;
	Button						bttn_feedback;

	public static final int 		progress_bar_type = 0; 
	private ProgressDialog 			pDialog;

	ImageView	pBar;
	Animation	rotate;
	static String							FEEDBACK_EMAIL_ID;

	/** Create Request Queue Object Volley */
	RequestQueue queue;
	AlertDialog alert=null;
	Typeface tf;
	boolean		callOnStrat = true;
	ArrayList<FeedbackInfo>		feedbackInfoArr;


	private static boolean useLocalytics 		= false;
	private static boolean useFlurry 		= false;
	private LocalyticsSession localyticsSession;
	private static  String LOCALYTICS_APP_KEY	=""; 


	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		/** Initialize Request Queue Object Volley */
		queue=Volley.newRequestQueue(getActivity());
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.feedback, null);
		tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/KabelMediumBT.ttf");
		rotate = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate);
		Button bttn = (Button) view.findViewById(R.id.bttn_menu);
		list_feedback = (ListView) view.findViewById(R.id.list_view);
		list_feedback.setCacheColorHint(0);
		feedback_url = getResources().getString(R.string.url_md_live_server) + getResources().getString(R.string.url_feedback);
		FEEDBACK_EMAIL_ID	=	getResources().getString(R.string.feed_back_email_id);
		netCheck			= new NetworkCheck(getActivity());
		pDialog 			= new ProgressDialog(getActivity());
		tweets_Arr 			= new ArrayList<String>();
		img_url_Arr 		= new ArrayList<String>();
		screen_name_arr 	= new ArrayList<String>();
		feedbackInfoArr		= new ArrayList<FeedbackInfo>();
		bttn_feedback		= (Button) view.findViewById(R.id.bttn_feedback);
		bttn_feedback.setTypeface(tf);
		pBar		= (ImageView) view.findViewById(R.id.progressBar2);
		pBar.startAnimation(rotate);
		pBar.setVisibility(View.VISIBLE);

		TextView text_tittle = (TextView) view.findViewById(R.id.text_tittle);
		text_tittle.setText("Feedback");
		text_tittle.setTypeface(tf);
		bttn.setTypeface(tf);
		bttn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				RootFragment rf = (RootFragment) getActivity();
				rf.showSlidingMenu();
			}
		});


		useFlurry			= getResources().getBoolean(R.bool.useFlurry);
		useLocalytics			= getResources().getBoolean(R.bool.useLocalytics);

		if(useLocalytics){
			LOCALYTICS_APP_KEY 		= getResources().getString(R.string.LOCALYTICS_APP_KEY);
			this.localyticsSession 	= new LocalyticsSession(getActivity(),LOCALYTICS_APP_KEY);
			this.localyticsSession.open();
			//this.localyticsSession.upload();
			localyticsSession.tagScreen(getResources().getString(R.string.localEvent_tab_feedback));
			this.localyticsSession.upload();
		}

		/*bttn_feedback.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Intent.ACTION_SEND);
				intent.setType("message/rfc822");
				intent.putExtra(Intent.EXTRA_EMAIL, new String[]{FEEDBACK_EMAIL_ID});
				startActivity(intent);

			}
		});

		list_feedback.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				showAlert(arg2);
			}
		});*/
		list_feedback.setOnItemClickListener(this);
		bttn_feedback.setOnClickListener(this);



		/*Handler handler = new Handler();
		Runnable runnable = new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				startOpreation();
			}
		};
		handler.postDelayed(runnable, 200);*/
		return view;
	}

	public void startOpreation(){
		File file = new File(feedbackFile);
		if(file.exists()){
			//new DownloadFile().execute("");  //----ASYNC EXECUTE-----
			//bearerToken		= "NjN4ZEs5cVEzbk9ScTBXT3FpWHc6cnJJZ3czMGdTYWpZc1BFSko2R2k2ZFM5dVM4Um9TOXZ6ZVBabFBKcTZRbw==";
			//new AuthenticateApp().execute("");
			//new DownloadFile().execute("");
			showVolleyArray();
		}else if(netCheck.isNetworkAvailable()){
			//bearerToken		= "NjN4ZEs5cVEzbk9ScTBXT3FpWHc6cnJJZ3czMGdTYWpZc1BFSko2R2k2ZFM5dVM4Um9TOXZ6ZVBabFBKcTZRbw==";
			//new DownloadFile().execute(""); //----ASYNC EXECUTE-----
			showVolleyArray();
		}else{
			pBar.setVisibility(View.GONE);
			pBar.clearAnimation();
			Toast.makeText(getActivity(), "No Internet Connection", 0).show();
		}
	}

	/** Modified method for Volley
	 * 	It includes a JSONArray parameter
	 *  */
	public void parseJsonData(final JSONArray response){

		Log.i("Volley print","volley print in parseJsondata()");

		Handler handler;
		Runnable runnable;
		tweets_Arr.clear();
		img_url_Arr.clear();
		screen_name_arr.clear();
		//File file = new File(feedbackFile);
		//if(file.exists()){
		handler = new Handler();
		runnable = new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try{
					//FileInputStream filename = new FileInputStream(feedbackFile);
					//FileChannel fc = filename.getChannel();

					//MappedByteBuffer bb=fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
					//String jString = Charset.defaultCharset().decode(bb).toString();
					//JSONObject json = new JSONObject(jString);
					//JSONArray jArr = new JSONArray(jString);
					//JSONArray jArr=jString.getJSONArray(jString); //jstring
					for(int i=0;i<response.length();i++){ //jarr.length()

						FeedbackInfo		feedBackInfo = new FeedbackInfo();
						JSONObject tempJObj = response.getJSONObject(i); //jarr.getjsonobject
						String text = tempJObj.getString("Feedback_Txt");
						tweets_Arr.add(text);

						String img_url = tempJObj.getString("Picture");
						img_url_Arr.add(img_url);

						String name = tempJObj.getString("User_Name");
						screen_name_arr.add(name);


						feedBackInfo.setFeedbackText(tempJObj.getString("Feedback_Txt"));
						feedBackInfo.setUserId(tempJObj.getString("User_ID"));
						feedBackInfo.setPicture(tempJObj.getString("Picture"));
						feedBackInfo.setUserName(tempJObj.getString("User_Name"));

						feedbackInfoArr.add(feedBackInfo);
					}
					pBar.setVisibility(View.GONE);
					pBar.clearAnimation();
					//filename.close();
					tweet_adapter = new FeedbackAdapter(getActivity(), feedbackInfoArr);
					list_feedback.setAdapter(tweet_adapter);
					//list.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, tweets_Arr));
				}catch(Exception ex){
					ex.printStackTrace();
				}

			}
		};
		handler.postDelayed(runnable, 300);

		//}
	}


	public void showAlert(int pos){
		AlertDialog.Builder  alertDialog = new AlertDialog.Builder(getActivity());
		//alertDialog.setIcon(R.drawable.ic_launcher);
		View view =  getActivity().getLayoutInflater().inflate(R.layout.feedback_dialog, null);
		ImageView imgview = (ImageView) view.findViewById(R.id.img_user_feedback);
		ImageLoader	loader = new ImageLoader(getActivity());
		TextView  text			= (TextView) view.findViewById(R.id.text_user_name_feedback);
		TextView  text_feedback	= (TextView) view.findViewById(R.id.text_feedback_content);
		text.setText(screen_name_arr.get(pos));
		text_feedback.setText(tweets_Arr.get(pos));
		text.setTypeface(tf);
		text_feedback.setTypeface(tf);
		loader.DisplayImage(img_url_Arr.get(pos), imgview);
		alertDialog.setView(view);
		alertDialog.setTitle("Dance With Madhuri");
		//alertDialog.setMessage(cantUploadVideo);
		alertDialog.setCancelable(true);
		alertDialog.setPositiveButton("Close", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				alert.dismiss();
			}
		});

		alert = alertDialog.create();
		alert.show();
	}

	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case progress_bar_type:
			pDialog = new ProgressDialog(getActivity());
			pDialog.setIndeterminate(false);
			pDialog.setMessage("Please wait");
			pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			pDialog.setCancelable(true);
			pDialog.show();
			return pDialog;
		default:
			return null;
		}
	}

	public class DownloadFile extends AsyncTask<String, String, String>{

		int count = 0;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pBar.startAnimation(rotate);
			pBar.setVisibility(View.VISIBLE);
		}


		@Override
		protected String doInBackground(String... params) {
			try{
				HttpClient	httpClient	=	new DefaultHttpClient();
				HttpGet		httpGet		=	new HttpGet(feedback_url);
				//httpGet.setHeader("Authorization", "Bearer "+access_token);

				HttpResponse response 	= httpClient.execute(httpGet);
				String status 			= response.getStatusLine().toString();
				Log.d("Status", "Status " + status);

				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				StringBuffer stringBuffer = new StringBuffer("");
				String line = "";
				String LineSeparator = System.getProperty("line.separator");
				while ((line = bufferedReader.readLine()) != null) {
					stringBuffer.append(line + LineSeparator); 
				}
				bufferedReader.close();
				Log.i("Response Tweets: ", "Response Tweets"+stringBuffer.toString());

				InputStream input = new ByteArrayInputStream(stringBuffer.toString().getBytes());


				// Output stream to write file

				try{
					File myDirectory = new File(Environment.getExternalStorageDirectory(), "dwm");
					if(!myDirectory.exists()) {                                 
						myDirectory.mkdirs();
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}

				FileOutputStream output = new FileOutputStream(feedbackFile);
				byte data[] = new byte[1024];
				long total = 0;
				while (   ((count = input.read(data)) != -1)  ) {
					total += count;
					output.write(data, 0, count);
				}
				output.flush();
				output.close();
				input.close();

			}catch(Exception ex){
				ex.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			//pDialog.dismiss();
			pBar.setVisibility(View.GONE);
			pBar.clearAnimation();

			//parseJsonData();
		}
	}

	/** The method that deals with JsonArrayRequest using Volley */
	void showVolleyArray(){


		JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(Request.Method.GET, feedback_url, null, 
				new VolleyResponse.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				Calendar cal=Calendar.getInstance();
				Log.i("Volley timer", "MdAfter request >> "+cal.getTimeInMillis());
				try{
					if(response.getString("success").equalsIgnoreCase("true") || response.getBoolean("success")){
						parseJsonData(response.getJSONArray("data"));
						Log.d("", "MdFeedback >> " +response.toString());
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}

			}
		}, 
		new VolleyResponse.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				showToast("Network Error!");
				pBar.setVisibility(View.GONE);
				pBar.clearAnimation();
			}
		});


		Calendar cal=Calendar.getInstance();
		Log.i("Volley timer", "MdBefore request >> "+cal.getTimeInMillis());

		queue.add(jsonArrayRequest);
	}

	public void showToast(String content){
		try{
			Toast.makeText(getActivity(), content, 0).show();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if(useFlurry){
			FlurryAgent.onStartSession(getActivity(), getResources().getString(R.string.flurry_id));
			FlurryAgent.logEvent(getResources().getString(R.string.fEvent_tab_feedback));
		}
		if(callOnStrat){
			Handler handler = new Handler();
			Runnable runnable = new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					startOpreation();
				}
			};
			handler.postDelayed(runnable, 200);
		}

	}


	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if(useFlurry){
			FlurryAgent.onEndSession(getActivity());
		}


	}

	@Override
	public void onItemClick(AdapterView<?> view, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		switch (view.getId()) {
		case R.id.list_view:
			showAlert(arg2);
			break;

		default:
			break;
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.bttn_feedback:
			Intent intent = new Intent(Intent.ACTION_SEND);
			intent.setType("message/rfc822");
			intent.putExtra(Intent.EXTRA_EMAIL, new String[]{FEEDBACK_EMAIL_ID});
			startActivity(intent);

			break;

		default:
			break;
		}
	}


	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(useLocalytics){
			this.localyticsSession.open();
			localyticsSession.tagScreen(getResources().getString(R.string.localEvent_tab_feedback));
		}
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub

		super.onPause();
		if(useLocalytics){
			this.localyticsSession.close();
			this.localyticsSession.upload();
		}
	}

}
