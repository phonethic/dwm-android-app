package com.phonethics.fragments;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyResponse;
import com.android.volley.VolleyResponse.ErrorListener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.flurry.android.FlurryAgent;
import com.localytics.android.LocalyticsSession;
import com.phonethics.adapters.StarSpeakListViewAdapter;

import com.phonethics.dancewithmadhuri.R;
import com.phonethics.dancewithmadhuri.YoutubeVideoPlay;

public class StarSpeak extends Fragment {

	String SPEAK_URL=null;
	String starSpeakFile="/sdcard/dwm/feedback.txt";
	ListView listviewStarSpeak;

	StarSpeakListViewAdapter adapter;

	//type embedd=1; type host=0
	ArrayList<String> titleArrayList,thumbnailArrayList,descriptionArrayList,videoLinkArrayList;
	ArrayList<Integer> videoTypeArrayList;
	ImageView	pBar;
	Animation	rotate;
	boolean		callOnstart =  true;

	private static boolean useLocalytics 		= false;
	private static boolean useFlurry 		= false;
	private LocalyticsSession localyticsSession;
	private static  String LOCALYTICS_APP_KEY	=""; 

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.starpeak, null);
		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/KabelMediumBT.ttf");
		Button bttn = (Button) view.findViewById(R.id.bttn_menu);
		TextView text_tittle = (TextView) view.findViewById(R.id.text_tittle);
		text_tittle.setText("Star Speak");
		text_tittle.setTypeface(tf);
		bttn.setTypeface(tf);
		rotate = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate);
		pBar		= (ImageView) view.findViewById(R.id.progressBar2);
		pBar.startAnimation(rotate);
		pBar.setVisibility(View.VISIBLE);
		SPEAK_URL=getResources().getString(R.string.starSpeakURL);

		listviewStarSpeak=(ListView) view.findViewById(R.id.listviewStarSpeak);
		titleArrayList=new ArrayList<String>();
		thumbnailArrayList=new ArrayList<String>();
		descriptionArrayList=new ArrayList<String>();
		videoLinkArrayList=new ArrayList<String>();
		videoTypeArrayList=new ArrayList<Integer>();

		listviewStarSpeak=(ListView) view.findViewById(R.id.listviewStarSpeak);
		listviewStarSpeak.setCacheColorHint(0);

		useFlurry			= getResources().getBoolean(R.bool.useFlurry);
		useLocalytics			= getResources().getBoolean(R.bool.useLocalytics);

		if(useLocalytics){
			LOCALYTICS_APP_KEY 		= getResources().getString(R.string.LOCALYTICS_APP_KEY);
			this.localyticsSession 	= new LocalyticsSession(getActivity(),LOCALYTICS_APP_KEY);
			this.localyticsSession.open();
			//this.localyticsSession.upload();
			localyticsSession.tagScreen(getResources().getString(R.string.localEvent_tab_star_speak));
			this.localyticsSession.upload();
		}
		listviewStarSpeak.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				String url=videoLinkArrayList.get(pos);
				int type=videoTypeArrayList.get(pos);
				if(type==1){ //embedd
					Intent intent=new Intent(getActivity(),YoutubeVideoPlay.class);
					intent.putExtra("youtubeUrl", url);
					intent.putExtra("starName", titleArrayList.get(pos));
					intent.putExtra("FROM_JHALAK", false);

					startActivity(intent);
					getActivity().overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
			}
		});

		adapter=new StarSpeakListViewAdapter(getActivity(), 0, titleArrayList, thumbnailArrayList, descriptionArrayList, videoLinkArrayList, videoTypeArrayList);


		//new DownloadFile().execute("");



		bttn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				RootFragment rf = (RootFragment) getActivity();
				rf.showSlidingMenu();
			}
		});
		return view;
	}


	public void ParseJsonData(final JSONArray jArr){
		Handler handler;
		Runnable runnable;
		titleArrayList.clear();
		thumbnailArrayList.clear();
		descriptionArrayList.clear();
		//File file = new File(starSpeakFile);
		/*if(file.exists()){*/
		handler = new Handler();
		runnable = new Runnable() {

			@Override
			public void run() {

				try{
					/*FileInputStream filename = new FileInputStream(starSpeakFile);
						FileChannel fc = filename.getChannel();

						MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
						String jString = Charset.defaultCharset().decode(bb).toString();
						JSONArray jArr = new JSONArray(jString);*/
					for(int i=0;i<jArr.length();i++){
						JSONObject tempJObj = jArr.getJSONObject(i);
						String title = tempJObj.getString("Title");
						titleArrayList.add(title);

						String thumbnail = tempJObj.getString("Thumbnail");
						thumbnailArrayList.add(thumbnail);

						String description = tempJObj.getString("Description");
						descriptionArrayList.add(description);

						String videoLink = tempJObj.getString("Video_Link");
						videoLinkArrayList.add(videoLink);

						String videoType = tempJObj.getString("Type");
						Log.i("type",""+videoType);
						if(videoType.equals("embedd")){
							videoTypeArrayList.add(1);
						}
						else{
							videoTypeArrayList.add(0);
						}

					}
					//filename.close();
					adapter=new StarSpeakListViewAdapter(getActivity(), 0, titleArrayList, thumbnailArrayList, descriptionArrayList, videoLinkArrayList, videoTypeArrayList);
					listviewStarSpeak.setAdapter(adapter);

				}catch(Exception ex){
					ex.printStackTrace();
				}

			}
		};
		handler.postDelayed(runnable, 300);

		//}
	}
	//------------PARSING JSOON DATA-----------------




	//------------ASYNC CLASS DOWNLOAD FILE-----------------
	public class DownloadFile extends AsyncTask<String, String, String>{

		int count = 0;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}


		@Override
		protected String doInBackground(String... params) {

			try{
				HttpClient	httpClient	=	new DefaultHttpClient();
				HttpGet		httpGet		=	new HttpGet(SPEAK_URL);
				//httpGet.setHeader("Authorization", "Bearer "+access_token);

				HttpResponse response 	= httpClient.execute(httpGet);
				String status 			= response.getStatusLine().toString();
				Log.d("Status", "Status " + status);


				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				StringBuffer stringBuffer = new StringBuffer("");
				String line = "";
				String LineSeparator = System.getProperty("line.separator");
				while ((line = bufferedReader.readLine()) != null) {
					stringBuffer.append(line + LineSeparator); 

				}
				bufferedReader.close();
				Log.i("Response Tweets: ", "Response Tweets"+stringBuffer.toString());


				InputStream input = new ByteArrayInputStream(stringBuffer.toString().getBytes());


				// Output stream to write file

				try{
					File myDirectory = new File(Environment.getExternalStorageDirectory(), "dwm");
					if(!myDirectory.exists()) {                                 
						myDirectory.mkdirs();
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}

				FileOutputStream output = new FileOutputStream(starSpeakFile);
				byte data[] = new byte[1024];
				long total = 0;
				while (   ((count = input.read(data)) != -1)  ) {
					total += count;
					output.write(data, 0, count);
				}
				output.flush();
				output.close();
				input.close();

			}catch(Exception ex){
				ex.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			//pDialog.dismiss();
			//parseJsonData();
		}
		//------------ASYNC CLASS DOWNLOAD FILE-----------------


	}



	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if(callOnstart){
			getStartSpeakResponce(SPEAK_URL);
		}
		if(useFlurry){
			FlurryAgent.onStartSession(getActivity(), getResources().getString(R.string.flurry_id));
			FlurryAgent.logEvent(getResources().getString(R.string.fEvent_tab_submission));
		}

	}


	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		callOnstart = false;
		if(useFlurry){
			FlurryAgent.onEndSession(getActivity());
		}


	}



	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(useLocalytics){
			this.localyticsSession.open();
		}
	}

	





	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		callOnstart = false;
		if(useLocalytics){
			this.localyticsSession.close();
			this.localyticsSession.upload();
		}
	}


	public void getStartSpeakResponce(String url){
		RequestQueue 				queue;
		queue			= Volley.newRequestQueue(getActivity());
		try{
			VolleyResponse.Listener<JSONObject> listener = new VolleyResponse.Listener<JSONObject>() {

				@Override
				public void onResponse(JSONObject response) {
					// TODO Auto-generated method stub
					Log.d("Responce", "MdGurusResponce success "+ response.toString());
					pBar.clearAnimation();
					pBar.setVisibility(View.GONE);
					try{
						String success = response.getString("success");
						if(success.equalsIgnoreCase("true")){
							JSONArray jArr = response.getJSONArray("data");
							ParseJsonData(jArr);
						}

					}catch(Exception ex){

					}
				}
			};

			VolleyResponse.ErrorListener errorListener = new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {
					// TODO Auto-generated method stub
					Log.d("Error", "MdSongsResponce error "+ error.toString());
					pBar.clearAnimation();
					pBar.setVisibility(View.GONE);
					showToast("Network Error!");
				}
			};

			JsonObjectRequest req = new JsonObjectRequest(Method.GET, url, null, listener, errorListener);
			queue.add(req);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	public void showToast(String content){
		try{
			Toast.makeText(getActivity(), content, 0).show();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}




}
