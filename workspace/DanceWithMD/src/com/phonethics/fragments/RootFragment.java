package com.phonethics.fragments;

import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.phonethics.dancewithmadhuri.BaseActivity;
import com.phonethics.dancewithmadhuri.LoginScreen;
import com.phonethics.dancewithmadhuri.SampleListFragment;
import com.phonethics.dancewithmadhuri.R;

import com.phonethics.sharepref.SharePref;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyResponse;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.LoggingBehavior;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.Settings;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

public class RootFragment extends BaseActivity {


	private Fragment contFragment;
	Context	context;
	boolean menuVisble = false;
	HomeFragment homefrgament;
	private Fragment contFragment_home;
	AlertDialog 	alert=null;
	boolean		from_fb=false;
	SharePref	sharePref;
	String		url_logout = "";
	private Session.StatusCallback statusCallback = null;
	boolean backToLogin  = false;
	boolean	removeCallBack = false;
	boolean loggedOut = false;



	public RootFragment(int titleRes) {
		super(titleRes);
		// TODO Auto-generated constructor stub
	}
	public RootFragment() {
		super(R.string.app_name);
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		context = this;
		sharePref = new SharePref(context);

		Settings.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);

		Session session = Session.getActiveSession();
		if (session == null) {
			if (savedInstanceState != null) {
				session = Session.restoreSession(this, null, statusCallback, savedInstanceState);
			}
			if (session == null) {
				session = new Session(this);
			}
			Session.setActiveSession(session);
			if (session.getState().equals(SessionState.CREATED_TOKEN_LOADED)) {
				session.openForRead(new Session.OpenRequest(this).setCallback(statusCallback));
			}
		}

		url_logout	= getResources().getString(R.string.url_md_live_server)+getResources().getString(R.string.url_user_logout);
		if(savedInstanceState != null){
			contFragment = getSupportFragmentManager().getFragment(savedInstanceState, "contFragment");
		}
		if(savedInstanceState == null){
			if(sharePref.getNewUserLogin()){
				contFragment_home = new HomeFragment();
				contFragment 	  = new HomeFragment();
			}else{
				contFragment_home = new LearnDance();
				contFragment 	  = new LearnDance();
			}

		}
		from_fb	  = sharePref.isLoginFromFb();
		setContentView(R.layout.content_frame);
		getSupportFragmentManager()
		.beginTransaction()
		.replace(R.id.content_frame, contFragment)
		.commit();

		// set the Behind View
		setBehindContentView(R.layout.menu_frame);
		getSupportFragmentManager()
		.beginTransaction()
		.replace(R.id.menu_frame, new SampleListFragment())
		.commit();

		// customize the SlidingMenu
		if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
			getSlidingMenu().setBehindOffsetRes(R.dimen.slidingmenu_offset);
			//Toast.makeText(getApplicationContext(), "Portrait", 0).show();
		}else{
			//Toast.makeText(getApplicationContext(), "LANDSCAPE", 0).show();
			getSlidingMenu().setBehindOffsetRes(R.dimen.slidingmenu_offset_land);
		}

		if(contFragment instanceof HomeFragment){
			//Toast.makeText(getApplicationContext(), "Touch None", 0).show();
			getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
		}else{
			//Toast.makeText(getApplicationContext(), "Touch Full", 0).show();
			getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
		}


	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		getSupportFragmentManager().putFragment(outState, "contFragment", contFragment);
		Session session = Session.getActiveSession();
		Session.saveSession(session, outState);
	}

	public void switchContent(Fragment fragment) {

		contFragment = fragment;
		getSupportFragmentManager()
		.beginTransaction()
		.replace(R.id.content_frame, fragment)
		.commit();

		setBehindContentView(R.layout.menu_frame);
		getSupportFragmentManager()
		.beginTransaction()
		.replace(R.id.menu_frame, new SampleListFragment())
		.commit();

		// customize the SlidingMenu

		if(contFragment instanceof HomeFragment){
			//Toast.makeText(getApplicationContext(), "S-Touch None", 0).show();
			getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);

		}else{
			//Toast.makeText(getApplicationContext(), "S-Touch full", 0).show();
			getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
		}
		getSlidingMenu().showContent(true);



	}

	public void refreshSlidingMenu(){
		getSlidingMenu().refreshDrawableState();
	}

	public void showSlidingMenu(){
		getSlidingMenu().showMenu(true);

		menuVisble = true;
		/*Intent intent = new Intent();
		intent.setAction("com.phonethics.CUSTOM_INTENT");
		intent.putExtra("menuVisble", menuVisble);
		sendBroadcast(intent);*/


	}
	@Override
	public void showMenu() {
		// TODO Auto-generated method stub
		/*menuVisble = true;
		Intent intent = new Intent();
		intent.setAction("com.phonethics.CUSTOM_INTENT");
		intent.putExtra("menuVisble", menuVisble);
		sendBroadcast(intent);*/
		super.showMenu();
		Toast.makeText(getApplicationContext(), "Menu Show ", 0).show();
	}

	@Override
	public void showContent() {
		// TODO Auto-generated method stub
		menuVisble = false;
		super.showContent();


	}



	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);

	}


	public void logoutAlert(){
		getSlidingMenu().showContent(true);
		AlertDialog.Builder  alertDialog = new AlertDialog.Builder(context);
		//alertDialog.setIcon(R.drawable.ic_launcher);
		alertDialog.setTitle("Dance With Madhuri");
		alertDialog.setMessage("Are you sure, You want to Log out ?");
		alertDialog.setCancelable(true);
		alertDialog.setPositiveButton("Log out", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				alert.dismiss();
				if(from_fb){
					backToLogin = true;
					logoutFromFb();
					//loginBack();
				}else{
					logoutRequest();
				}
				//onBackPressed();
			}
		});
		alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				alert.dismiss();
			}

		});

		alert = alertDialog.create();
		alert.show();
	}



	public void logoutRequest(){
		String 	email	= sharePref.getUserEmail();
		String	token	= sharePref.getUserToken();
		JSONObject jObj = new JSONObject();
		try{

			jObj.put("uname", email);
			jObj.put("token", token);
		}catch(Exception ex){
			ex.printStackTrace();
		}


		RequestQueue queue = Volley.newRequestQueue(context);
		JsonObjectRequest req = new JsonObjectRequest(Method.POST, url_logout, jObj, new VolleyResponse.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub
				//pBar.setVisibility(View.INVISIBLE);

				Log.d("Responce", "MDLogout responce "+response.toString());

				try{
					String success = response.getString("success");

					if(success.equalsIgnoreCase("true")){
						Toast.makeText(context, "You have successfully logged out.", 0).show();
						backToLogin  = true;
					}else if(response.getString("message").equalsIgnoreCase("Already logout")){
						Toast.makeText(context, "You are already logged out.", 0).show();
						backToLogin  = true;
					}else{
						Toast.makeText(context, response.getString("message"), 0).show();
					}	
					loginBack();
				}catch(Exception ex){

				}
			}
		}, new VolleyResponse.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				//pBar.setVisibility(View.INVISIBLE);
				Log.d("Error", "MDLogout error "+error.toString());
			}
		});
		//pBar.setVisibility(View.VISIBLE);
		queue.add(req);
	}


	public void logoutFromFb(){
		try{
			Session.openActiveSession(this, false, new Session.StatusCallback() {


				// callback when session changes state
				@Override
				public void call(Session session, SessionState state, Exception exception) {

					if(session.isOpened()){	
						session.closeAndClearTokenInformation();
						//exception.printStackTrace();
						//Intent intent = new Intent(getActivity(), LoginScreen.class);
						//sharePref.setUserLogin(false);
						//startActivity(intent);
						//getActivity().finish();
						//backToLogin  = true;

					}
					//statusCallback = new SessionStatusCallback();
					loginBack();
					Log.i("Clearing", "Token");

				}
			});
		}catch(Exception ex){
			Log.i("Log out error", "Log out error");
			ex.printStackTrace();
		}
	}


	public void loginBack(){
		//Toast.makeText(context, "Login Back", 0).show();
		if(backToLogin && !loggedOut){
			loggedOut = true;
			sharePref.setLoginFromFb(false);
			sharePref.setUserLogin(false);
			sharePref.setUserName("");

			Intent intent = new Intent(context, LoginScreen.class);
			
			startActivity(intent);
			finish();
		}
	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
	}


	private class SessionStatusCallback implements Session.StatusCallback {
		@Override
		public void call(Session session, SessionState state, Exception exception) {
			//updateView();
			loginBack();
		}
	}


	@Override
	public void onStop() {
		super.onStop();
		if(statusCallback!=null){
			Session.getActiveSession().removeCallback(statusCallback);
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		if(statusCallback!=null){
			Session.getActiveSession().addCallback(statusCallback);
		}
	}


}
