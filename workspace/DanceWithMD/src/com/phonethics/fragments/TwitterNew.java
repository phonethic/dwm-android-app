package com.phonethics.fragments;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.MaskFilter;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.AutoText;
import android.text.TextUtils;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Scroller;
import android.widget.TextView;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyResponse;
import com.android.volley.VolleyResponse.ErrorListener;
import com.android.volley.VolleyResponse.Listener;
import com.android.volley.toolbox.Volley;
import com.flurry.android.FlurryAgent;

import com.localytics.android.LocalyticsSession;
import com.phonethics.circularimageview.CircularImageView;
import com.phonethics.customclass.ListTwits;
import com.phonethics.customclass.Twit;

import com.phonethics.dancewithmadhuri.R;
import com.phonethics.exten.GsonRequest;
import com.phonethics.exten.ObtainTokenRequest;
import com.phonethics.exten.Urls;
import com.phonethics.imageloader.ImageLoader;
import com.phonethics.network.NetworkCheck;

public class TwitterNew extends Fragment implements ObtainTokenRequest.OnTokenObtainListener, ErrorListener, View.OnClickListener {

	private static final String TOKEN = "token";
	private static final String TWITS = "twits";

	private TextView mLoadingErrorText;
	static TwitsAdapter mTwitsAdapter;
	private ArrayList<Twit> mTwits;

	public ImageView pBar;

	public static ArrayList<String> tweets_Arr;
	public static ArrayList<String> img_url_Arr;
	public static ArrayList<String> screen_name_arr;
	public static ArrayList<String> tweet_id;

	static int PULL_DOWN = 1;

	public boolean progressVisible = true;

	String tweetsFile =	"/sdcard/dwm/tweets.txt";

	private RequestQueue mRequestQueue;
	//private ImageLoader mImageLoader;
	private String mToken;
	ListView twitsList ;

	boolean callOnStart = true;
	Animation	rotate;
	Typeface tf;
	Button bttn_tweet;



	private static boolean useLocalytics 		= false;
	private static boolean useFlurry 			= false;
	private LocalyticsSession localyticsSession;
	private static  String LOCALYTICS_APP_KEY	=""; 



	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view 			 = inflater.inflate(R.layout.twitter, null);
		tf 		 			= Typeface.createFromAsset(getActivity().getAssets(), "fonts/KabelMediumBT.ttf");
		Button bttn 		 = (Button) view.findViewById(R.id.bttn_menu);
		bttn.setTypeface(tf);
		bttn_tweet = (Button) view.findViewById(R.id.bttn_send_tweet);
		TextView text_tittle = (TextView) view.findViewById(R.id.text_tittle);
		rotate = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate);
		text_tittle.setText("#DanceWithMadhuri");
		text_tittle.setTypeface(tf);

		useFlurry				= getResources().getBoolean(R.bool.useFlurry);
		useLocalytics			= getResources().getBoolean(R.bool.useLocalytics);

		if(useLocalytics){
			LOCALYTICS_APP_KEY 		= getResources().getString(R.string.LOCALYTICS_APP_KEY);
			this.localyticsSession 	= new LocalyticsSession(getActivity(),LOCALYTICS_APP_KEY);
			this.localyticsSession.open();
			//this.localyticsSession.upload();
			localyticsSession.tagScreen(getResources().getString(R.string.localEvent_tab_tweets));
			this.localyticsSession.upload();
		}

		bttn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				RootFragment rf = (RootFragment) getActivity();
				rf.showSlidingMenu();
			}
		});

		bttn_tweet.setText("Tweet   #DanceWithMadhuri");

		bttn_tweet.setTypeface(tf);
		bttn_tweet.setOnClickListener(this);


		tweets_Arr = new ArrayList<String>();
		img_url_Arr = new ArrayList<String>();
		screen_name_arr = new ArrayList<String>();
		tweet_id = new ArrayList<String>();

		pBar = (ImageView) view.findViewById(R.id.progressBar2);
		pBar.startAnimation(rotate);

		/** Initialize volley */
		mRequestQueue = Volley.newRequestQueue(getActivity());

		/** Restore saved instance */
		if (savedInstanceState != null) {
			mToken = savedInstanceState.getString(TOKEN);
			mTwits = (ArrayList<Twit>) savedInstanceState.getSerializable(TWITS);
		}
		if (mTwits == null) mTwits = new ArrayList<Twit>();



		/** Initialize list view */
		mTwitsAdapter = new TwitsAdapter();
		twitsList = (ListView) view.findViewById(R.id.list_view);
		twitsList.setCacheColorHint(0);
		twitsList.setAdapter(mTwitsAdapter);
		twitsList.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub
				if(scrollState == SCROLL_STATE_IDLE){

					int lastPos = twitsList.getLastVisiblePosition();
					Log.d("", "MdLast >> "+lastPos);
					if(lastPos==tweets_Arr.size()-1){
						Log.d("", "MdLast If >> "+lastPos);
						mTwitsAdapter.loadMoreData();
					}
				}
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub


			}
		});

		if (TextUtils.isEmpty(mToken)) {
			mRequestQueue.add(new ObtainTokenRequest(this, this));
		} else {
			onTokenObtain(mToken);
		}

		/*	Handler handler = new Handler();
		Runnable runnable = new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				startOpreation();
			}
		};
		handler.postDelayed(runnable, 200); */


		return view;
	}









	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if(useFlurry){
			FlurryAgent.onStartSession(getActivity(), getResources().getString(R.string.flurry_id));
			FlurryAgent.logEvent(getResources().getString(R.string.fEvent_tab_tweets));
		}

	}




	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if(useFlurry){
			FlurryAgent.onEndSession(getActivity());
		}
		tweets_Arr.clear(); img_url_Arr.clear(); screen_name_arr.clear();
		tweet_id.clear();

		PULL_DOWN = 1;
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==(R.id.bttn_send_tweet)){
			boolean installed  =   appInstalledOrNot("com.twitter.android");
			if(installed)
			{
				//System.out.println("App already installed om your phone");
				//Toast.makeText(context, "Installed", 0).show();

				try{


					//FlurryAgent.logEvent("NSFL_Tweet_New");
					Intent  intent = new Intent(Intent.ACTION_SEND);
					intent.putExtra(Intent.EXTRA_TEXT, "#DanceWithMadhuri");
					intent.setType("text/plain");
					final PackageManager pm = getActivity().getPackageManager();
					final List activityList = pm.queryIntentActivities(intent, 0);
					int len =  activityList.size();
					for (int i = 0; i < len; i++) {
						final ResolveInfo app = (ResolveInfo) activityList.get(i);
						if ("com.twitter.android.PostActivity".equals(app.activityInfo.name)) {
							final ActivityInfo activity=app.activityInfo;
							final ComponentName name=new ComponentName(activity.applicationInfo.packageName, activity.name);
							intent=new Intent(Intent.ACTION_SEND);
							intent.addCategory(Intent.CATEGORY_LAUNCHER);
							intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
							intent.setComponent(name);
							intent.putExtra(Intent.EXTRA_TEXT, "#DanceWithMadhuri ");
							getActivity().startActivity(intent);
							break;
						}
					}
				}
				catch(final ActivityNotFoundException e) {
					Log.i("NotInstalled", "no twitter native",e );
				}

			}
			else
			{
				//System.out.println("App is not installed om your phone");
				//Toast.makeText(context, "App is not installed", 0).show();

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
				alertDialogBuilder.setTitle("Twitter App");
				alertDialogBuilder
				.setMessage("Twitter Application is not installed. Would you like to install it?")
				.setCancelable(false)
				.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {

						//Dealer_location.this.finish();
						/*Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.twitter.android"));
						startActivity(intent);*/

						try {
							startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.twitter.android")));
						} catch (android.content.ActivityNotFoundException anfe) {
							startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.twitter.android")));
						}
					}
				})
				.setNegativeButton("No",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

				AlertDialog alertDialog = alertDialogBuilder.create();

				alertDialog.show();

			}


		}
	}



	private boolean appInstalledOrNot(String uri)
	{
		PackageManager pmngr = getActivity().getPackageManager();
		boolean app_installed = false;
		try
		{
			pmngr.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
			app_installed = true;
		}
		catch (PackageManager.NameNotFoundException e)
		{
			app_installed = false;
		}
		return app_installed ;
	}


	@Override
	public void onTokenObtain(String token) {
		mToken = token;
		mTwitsAdapter.loadMoreData();
	}

	@Override
	public void onErrorResponse(VolleyError error) {
		try{
			String message = error.getCause() != null ? error.getCause().getMessage() : getString(R.string.some_error);
			mLoadingErrorText.setText(message);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	private class TwitsAdapter extends BaseAdapter implements Listener<ListTwits> {
		//DisplayImageOptions options;
		//ImageLoaderConfiguration config=null;
		ImageLoader loader=new ImageLoader(getActivity());
		File cacheDir;
		Context context=getActivity();

		TwitHolder holder;

		public TwitsAdapter(){
			//------------Set configuration for Image loader-------------------
			NetworkCheck network = new NetworkCheck(context);

			//			if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
			//			{
			//				cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"/.madhuritweets");
			//			}
			//			else
			//			{
			//				cacheDir=context.getCacheDir();
			//			}
			//
			//			if(!cacheDir.exists())
			//			{
			//				cacheDir.mkdirs();
			//			}else if(network.isNetworkAvailable())
			//			{
			//
			//			}
			//
			//			config= new ImageLoaderConfiguration.Builder(context)
			//
			//			.denyCacheImageMultipleSizesInMemory()
			//			.threadPoolSize(20)
			//
			//			.discCache(new UnlimitedDiscCache(cacheDir))
			//			.build();
			//			loader.init(config);
			//
			//			options = new DisplayImageOptions.Builder()
			//			.cacheOnDisc(true)
			//			.bitmapConfig(Bitmap.Config.RGB_565)
			//			.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
			//			.build();
			//
			//			loader.init(config);
			//------------Set configuration for Image loader-------------------
		}

		@Override
		public int getCount() {
			Log.i("size","size "+tweets_Arr.size());
			return tweets_Arr.size();
		}

		@Override
		public Twit getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			if (convertView == null) {
				LayoutInflater inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.tweet_list_layout, parent, false);

				holder = new TwitHolder();
				holder.avatarImage = (CircularImageView) convertView.findViewById(R.id.img_profile);
				holder.avatarImage.setScaleType(ScaleType.CENTER_CROP);
				holder.nameText = (TextView) convertView.findViewById(R.id.text_name);
				holder.twitText = (TextView) convertView.findViewById(R.id.text_tweet);
				holder.imgBack			= (ImageView) convertView.findViewById(R.id.img_back_trans);
				holder.nameText.setTypeface(tf);
				holder.twitText.setTypeface(tf);
				holder.twitText.setAutoLinkMask(Linkify.ALL);
				holder.twitText.setLinkTextColor(Color.parseColor("#B3A0DE"));
				holder.imgBack.getBackground().setAlpha(60);
				convertView.setTag(holder);
			} else {
				holder = (TwitHolder) convertView.getTag();
			}


			try{
				holder.nameText.setText(screen_name_arr.get(position));
				holder.twitText.setText(tweets_Arr.get(position));
				loader.DisplayImage(img_url_Arr.get(position), holder.avatarImage);
			}catch(Exception ex){
				ex.printStackTrace();
			}


			return convertView;
		}

		public void loadMoreData() {
			pBar.setVisibility(View.VISIBLE);
			pBar.startAnimation(rotate);
			if(tweets_Arr.size()<=0){
				String url = Urls.USER_TIMELINE_URL;
				/*PULL_DOWN	=	PULL_DOWN+1;
        		String url=Urls.SEARCH_MD_TWEETS+"&count="+(PULL_DOWN*10);*/
				Log.i("url","Mdurl >> "+url);
				mRequestQueue.add(new GsonRequest<ListTwits>(mToken, url, ListTwits.class,new VolleyResponse.Listener<ListTwits>() {

					@Override
					public void onResponse(ListTwits response) {
						// TODO Auto-generated method stub

						try{
							Log.d("", "MDTweetResponse >> "+response.toString());
							pBar.setVisibility(View.GONE);
							pBar.clearAnimation();
							progressVisible=false;
							if(progressVisible==false){
								notifyDataSetChanged();
							}
						}catch(Exception ex){
							ex.printStackTrace();
						}

					}
				} , new VolleyResponse.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						// TODO Auto-generated method stub
						try{
							String message = error.getCause() != null ? error.getCause().getMessage() : getString(R.string.some_error);
							mLoadingErrorText.setText(message);
						}catch(Exception ex){
							ex.printStackTrace();
						}
					}
				} ));
			}
			else{
				//String url=Urls.USER_TIMELINE_URL+"&since_id="+tweet_id.get(0);
				PULL_DOWN	=	PULL_DOWN+1;
				String url = Urls.SEARCH_MD_TWEETS+"&count="+(PULL_DOWN*10);
				Log.i("url","Mdurl >> "+url);
				mRequestQueue.add(new GsonRequest<ListTwits>(mToken, url, ListTwits.class, new VolleyResponse.Listener<ListTwits>() {

					@Override
					public void onResponse(ListTwits response) {
						// TODO Auto-generated method stub
						try{
							Log.d("", "MDTweetResponse >> "+response.toString());
							pBar.setVisibility(View.GONE);
							pBar.clearAnimation();
							progressVisible=false;
							if(progressVisible==false){
								notifyDataSetChanged();
							}
						}catch(Exception ex){
							ex.printStackTrace();
						}
					}
				} , new VolleyResponse.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						// TODO Auto-generated method stub
						try{
							String message = error.getCause() != null ? error.getCause().getMessage() : getString(R.string.some_error);
							mLoadingErrorText.setText(message);
						}catch(Exception ex){
							ex.printStackTrace();
						}
					}
				} ));
			}

		}

		@Override
		public void onResponse(ListTwits twits) {
			pBar.setVisibility(View.GONE);
			pBar.clearAnimation();
			progressVisible=false;
			if(progressVisible==false)
				notifyDataSetChanged();
		}

		private class TwitHolder {
			//ImageView avatarImage;
			CircularImageView avatarImage;
			TextView nameText;
			TextView twitText;
			ImageView imgBack;
		}

	}




	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		PULL_DOWN = 1;
	}


	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(useLocalytics){
			this.localyticsSession.open();
		}
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub

		super.onPause();
		if(useLocalytics){
			this.localyticsSession.close();
			this.localyticsSession.upload();
		}
	}

}
