package com.phonethics.fragments;

import java.util.HashMap;
import java.util.Map;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.opengl.Visibility;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.flurry.android.FlurryAgent;

import com.localytics.android.LocalyticsSession;
import com.phonethics.dancewithmadhuri.R;
import com.phonethics.md.VideoFull;
import com.phonethics.network.NetworkCheck;
import com.phonethics.sharepref.SharePref;
import com.sbstrm.appirater.Appirater;

public class HomeFragment extends Fragment {

	public static VideoView vd;
	//String	url_english		= "http://s3-ap-southeast-1.amazonaws.com/dance-with-md/opening/mp4/360/Site_Intro_English.mp4";
	String	url_english		= "";
	//String	url_hindi		= "http://s3-ap-southeast-1.amazonaws.com/dance-with-md/opening/mp4/360/Site_Intro_Hindi.mp4";
	String	url_hindi		= "";
	String	url_play		= url_english;
	private ProgressDialog pDialog;
	ImageView	pBar;
	Animation	rotate;
	Typeface tf;
	Button bttn_english,bttn_hindi;
	ImageView	img_full_screen,img_help;
	ScrollView scrollView;
	TextView textInfo;
	Animation 						fade_in_anim,fade_out_anim;
	int pos=0;
	Context 		context;
	SharePref		sharePref;
	NetworkCheck	netCheck;
	boolean			menuCliked = false;
	Map<String, String> 	flurryEeventParams;

	private static boolean useLocalytics 		= false;
	private static boolean useFlurry 		= false;
	private LocalyticsSession localyticsSession;
	private static  String LOCALYTICS_APP_KEY	=""; 

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		if(savedInstanceState!=null){
			pos = savedInstanceState.getInt("pos");
			context = getActivity();
		}


	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.home_land, null);
		sharePref = new SharePref(getActivity());
		useFlurry				= getResources().getBoolean(R.bool.useFlurry);
		useLocalytics			= getResources().getBoolean(R.bool.useLocalytics);

		if(useLocalytics){
			LOCALYTICS_APP_KEY 		= getResources().getString(R.string.LOCALYTICS_APP_KEY);
			this.localyticsSession 	= new LocalyticsSession(getActivity(),LOCALYTICS_APP_KEY);
			this.localyticsSession.open();
			//this.localyticsSession.upload();
			localyticsSession.tagScreen("Home");
			
			this.localyticsSession.upload();
		}
		rotate = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate);
		flurryEeventParams	= new HashMap<String, String>();
		tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/KabelMediumBT.ttf");
		url_english = getResources().getString(R.string.url_intro_english);
		url_hindi = getResources().getString(R.string.url_intro_hindi);
		url_play = url_english;
		Button bttn = (Button) view.findViewById(R.id.bttn_menu);
		bttn_english = (Button) view.findViewById(R.id.bttn_english);
		bttn_hindi 	= (Button) view.findViewById(R.id.bttn_hindi);
		pBar		= (ImageView) view.findViewById(R.id.progressBar2);
		img_help	= (ImageView) view.findViewById(R.id.img_help);
		scrollView = (ScrollView) view.findViewById(R.id.scroll_info);
		scrollView.setVisibility(View.INVISIBLE);
		img_full_screen = (ImageView) view.findViewById(R.id.img_full_screen);
		textInfo	= (TextView) view.findViewById(R.id.text_info);
		netCheck	= new NetworkCheck(getActivity());
		fade_in_anim 	= AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);

		TextView text_welcome = (TextView) view.findViewById(R.id.text_welcome);
		text_welcome.setTypeface(tf);
		bttn_english.setTypeface(tf);
		bttn_hindi.setTypeface(tf);
		textInfo.setTypeface(tf);
		sharePref.setPresentTab("Home");

		vd = (VideoView) view.findViewById(R.id.video);
		vd.setVisibility(View.INVISIBLE);
		TextView text_tittle = (TextView) view.findViewById(R.id.text_tittle);
		text_tittle.setTypeface(tf);
		text_tittle.setText("Home");

		bttn.setTypeface(tf);
		if(sharePref.getHelpImageVisiblity()){
			img_help.setVisibility(View.VISIBLE);
		}else{
			img_help.setVisibility(View.GONE);
		}
		
		bttn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				vd.pause();
				vd.setVisibility(View.GONE);
				//vd.setVisibility(View.INVISIBLE);
				RootFragment rf = (RootFragment) getActivity();
				rf.showSlidingMenu();

			}
		});
		bttn_english.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				vd.stopPlayback();
				bttn_english.setBackgroundResource(R.drawable.home_button_back);
				bttn_hindi.setBackgroundResource(R.drawable.button_back);
				url_play		= url_english;
				if(netCheck.isNetworkAvailable()){
					flurryEeventParams.put("Video_Type", "English");
					if(useFlurry){
						
						FlurryAgent.logEvent(getResources().getString(R.string.fEvent_intro_video),flurryEeventParams);
					}
					if(useLocalytics){
						localyticsSession.tagEvent(getResources().getString(R.string.localEvent_intro_video), flurryEeventParams);
					}
					new StreamVideo().execute();
				}else{
					Toast.makeText(getActivity(), "No internet connection", 0).show();
					pBar.clearAnimation();
					pBar.setVisibility(View.INVISIBLE);
					vd.setVisibility(View.GONE);
					scrollView.setVisibility(View.VISIBLE);

				}
			}
		});
		bttn_hindi.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				vd.stopPlayback();
				bttn_hindi.setBackgroundResource(R.drawable.home_button_back);
				bttn_english.setBackgroundResource(R.drawable.button_back);
				url_play		= url_hindi;
				if(netCheck.isNetworkAvailable()){
					if(useFlurry){
						flurryEeventParams.put("Video_Type", "Hindi");
						FlurryAgent.logEvent(getResources().getString(R.string.fEvent_intro_video),flurryEeventParams);
					}
					if(useLocalytics){
						localyticsSession.tagEvent(getResources().getString(R.string.localEvent_intro_video), flurryEeventParams);
					}
					new StreamVideo().execute();
				}else{
					Toast.makeText(getActivity(), "No internet connection", 0).show();
					pBar.clearAnimation();
					pBar.setVisibility(View.INVISIBLE);

					vd.setVisibility(View.GONE);
					scrollView.setVisibility(View.VISIBLE);
				}
			}
		});
		img_full_screen.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				pos = vd.getCurrentPosition();
				if(netCheck.isNetworkAvailable()){
					Intent intent = new Intent(getActivity(), VideoFull.class);
					intent.putExtra("url_play", url_play);
					intent.putExtra("current_pos", vd.getCurrentPosition());
					vd.stopPlayback();
					startActivity(intent);
				}else{
					Toast.makeText(getActivity(), "No internet connection", 0).show();
				}

			}
		});

		
		img_help.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(img_help.isShown()){
					img_help.setVisibility(View.GONE);
					sharePref.setHelpImageVisible(false);
				}
			}
		});
		Appirater.appLaunched(getActivity());

		/*Handler handler = new Handler();
		Runnable runnable = new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				start();
			}
		};
		handler.postDelayed(runnable, 100);*/

		/*if(pos==0){
			new StreamVideo().execute();
		}else{


			MediaController mediacontroller = new MediaController(getActivity());
			mediacontroller.setAnchorView(vd);
			// Get the URL from String VideoURL
			Uri video = Uri.parse(url_play);
			vd.setMediaController(mediacontroller);
			vd.setVideoURI(video);

			vd.requestFocus();
			vd.setOnPreparedListener(new OnPreparedListener() {
				// Close the progress bar and play the video
				public void onPrepared(MediaPlayer mp) {
					//pDialog.dismiss();

					vd.seekTo(pos);
					vd.start();

					scrollView.setVisibility(View.VISIBLE);
					progressBar.setVisibility(View.GONE);


					//scrollView.setVisibility(View.VISIBLE);
				}
			});

		}
		 */

		return view;
	}



	public void start(){

		if(netCheck.isNetworkAvailable()){
			if(pos==0){
				new StreamVideo().execute();
			}else{


				MediaController mediacontroller = new MediaController(getActivity());
				mediacontroller.setAnchorView(vd);
				// Get the URL from String VideoURL
				Uri video = Uri.parse(url_play);
				vd.setMediaController(mediacontroller);
				vd.setVideoURI(video);
				vd.setVisibility(View.VISIBLE);

				//vd.requestFocus();
				vd.setOnPreparedListener(new OnPreparedListener() {
					// Close the progress bar and play the video
					public void onPrepared(MediaPlayer mp) {
						//pDialog.dismiss();

						vd.seekTo(pos);
						vd.start();

						scrollView.setVisibility(View.VISIBLE);
						pBar.clearAnimation();
						pBar.setVisibility(View.GONE);


						//scrollView.setVisibility(View.VISIBLE);
					}
				});

				vd.setOnErrorListener(new OnErrorListener() {

					@Override
					public boolean onError(MediaPlayer mp, int what, int extra) {
						// TODO Auto-generated method stub
						pBar.clearAnimation();
						pBar.setVisibility(View.GONE);

						vd.setVisibility(View.GONE);
						scrollView.setVisibility(View.VISIBLE);
						return true;
					}
				});

			}
		}else{
			pBar.clearAnimation();
			pBar.setVisibility(View.GONE);
			vd.setVisibility(View.GONE);
			scrollView.setVisibility(View.VISIBLE);
			Toast.makeText(getActivity(), "No internet connection", 0).show();
		}

	}

	// StreamVideo AsyncTask
	private class StreamVideo extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressbar
			/* pDialog = new ProgressDialog(getActivity());
            // Set progressbar title
            pDialog.setTitle("Android Video Streaming Tutorial");
            // Set progressbar message
            pDialog.setMessage("Buffering...");
            pDialog.setIndeterminate(false);
            // Show progressbar
            pDialog.show();*/
			pBar.setVisibility(View.VISIBLE);
			pBar.startAnimation(rotate);

		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected void onPostExecute(Void args) {

			try {
				// Start the MediaController

				MediaController mediacontroller = new MediaController(getActivity());
				mediacontroller.setAnchorView(vd);
				// Get the URL from String VideoURL
				Uri video = Uri.parse(url_play);
				vd.setMediaController(mediacontroller);
				vd.setVideoURI(video);
				vd.setVisibility(View.VISIBLE);
				vd.destroyDrawingCache();
				//vd.requestFocus();
				vd.setOnPreparedListener(new OnPreparedListener() {
					// Close the progress bar and play the video
					public void onPrepared(MediaPlayer mp) {
						//pDialog.dismiss();
						vd.start();

						pBar.clearAnimation();
						pBar.setVisibility(View.GONE);

						//scrollView.setVisibility(View.VISIBLE);
					}
				});
				vd.setOnErrorListener(new OnErrorListener() {

					@Override
					public boolean onError(MediaPlayer mp, int what, int extra) {
						// TODO Auto-generated method stub
						vd.setVisibility(View.GONE);
						pBar.clearAnimation();
						pBar.setVisibility(View.GONE);
						scrollView.setVisibility(View.VISIBLE);
						return true;
					}
				});
				scrollView.startAnimation(fade_in_anim);
				fade_in_anim.setFillAfter(true);
				fade_in_anim.setAnimationListener(new AnimationListener() {

					@Override
					public void onAnimationStart(Animation animation) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onAnimationRepeat(Animation animation) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onAnimationEnd(Animation animation) {
						// TODO Auto-generated method stub
						//scrollView.setVisibility(View.VISIBLE);
					}
				});
			} catch (Exception e) {
				//pDialog.dismiss();
				Log.d("Error", ""+e.toString());
				e.printStackTrace();
				pBar.clearAnimation();
				pBar.setVisibility(View.GONE);
				//scrollView.setVisibility(View.VISIBLE);
			}

		}

	}


	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		getActivity().setContentView(R.layout.home_land);
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (vd.isPlaying()){ 
			outState.putInt("pos", vd.getCurrentPosition());

		}
		context = getActivity().getApplicationContext();


	}

	public void setVideoVisibilty(boolean visible){
		if(visible){
			vd.setVisibility(View.VISIBLE);
		}else{
			vd.setVisibility(View.INVISIBLE);
		}

	}


	public static void showVideoView(){
		try{
			vd.resume();
			vd.setVisibility(View.VISIBLE);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	public static void hideVideoView(){
		try{
			vd.pause();
			vd.setVisibility(View.GONE);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}



	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if(useFlurry){
			FlurryAgent.onStartSession(getActivity(), getResources().getString(R.string.flurry_id));
			FlurryAgent.logEvent(getResources().getString(R.string.fEvent_tab_home));
			flurryEeventParams.put("Video_Type", "English");
			FlurryAgent.logEvent(getResources().getString(R.string.fEvent_intro_video));
		}
		start();

	}


	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if(useFlurry){
			FlurryAgent.onEndSession(getActivity());
		}


	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(useLocalytics){
			this.localyticsSession.open();
			
			localyticsSession.tagScreen("Home");
		}
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub

		super.onPause();
		if(useLocalytics){
			this.localyticsSession.close();
			this.localyticsSession.upload();
		}
	}


}
