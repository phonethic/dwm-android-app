package com.phonethics.adapters;

import java.util.ArrayList;

import com.phonethics.adapters.TweetsAdapter.ViewHolder;
import com.phonethics.customclass.SongInfo;
import com.phonethics.dancewithmadhuri.R;
import com.phonethics.imageloader.ImageLoader;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SubmissionsAdapter extends ArrayAdapter<SongInfo> {

	ArrayList<SongInfo> 			songInfoArr;
	private static LayoutInflater 	inflator = null;
	ImageLoader						imageLoader;
	Activity						context;


	public SubmissionsAdapter(Activity context,ArrayList<SongInfo> songInfoArr){
		super(context,R.layout.submission_list_layout, songInfoArr);

		imageLoader	= new ImageLoader(context);
		imageLoader.setStubId(R.drawable.thumbnail);
		this.context =context;  
		this.songInfoArr = songInfoArr;




	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return songInfoArr.size();
	}

	@Override
	public SongInfo getItem(int position) {
		// TODO Auto-generated method stub
		return songInfoArr.get(position);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView==null){
			ViewHolder holder 		= new ViewHolder();
			inflator				= context.getLayoutInflater();
			convertView 			= inflator.inflate(R.layout.submission_list_layout, null);
			holder.text_song		= (TextView)  convertView.findViewById(R.id.text_submission);

			holder.text_song.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/KabelMediumBT.ttf"));



			convertView.setTag(holder);

		}

		ViewHolder hold = (ViewHolder) convertView.getTag();
		try{
			String songName 	= getItem(position).getTitle();
			String categoryName = getItem(position).getCategory_name();
			hold.text_song.setText(categoryName +" - "+ songName);	
			/*if(songName.equalsIgnoreCase("Class 1")){
				hold.text_song.setText(categoryName +" "+ songName);	
			}else{
				hold.text_song.setText(songName);
			}*/
			

		}catch(Exception ex){
			ex.printStackTrace();
		}


		return convertView;
	}





	static class ViewHolder{
		public TextView 	text_song;

	}



}
