package com.phonethics.adapters;

import java.util.ArrayList;


import com.phonethics.customclass.LeaderBoardDetails;
import com.phonethics.dancewithmadhuri.R;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import android.widget.TextView;

public class LeaderBoardAdapter extends ArrayAdapter<LeaderBoardDetails> {

	ArrayList<LeaderBoardDetails> leaderBrdArr;
	Activity			context;
	private static LayoutInflater 		inflator = null;
	Typeface		tf;

	public LeaderBoardAdapter(Activity context,ArrayList<LeaderBoardDetails> leaderBrdArr){
		super(context,R.layout.leaderboard_list_row, leaderBrdArr);

		this.leaderBrdArr = leaderBrdArr;
		this.context = context;
		tf = Typeface.createFromAsset(context.getAssets(), "fonts/KabelMediumBT.ttf");

	}



	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return leaderBrdArr.size();
	}



	@Override
	public LeaderBoardDetails getItem(int position) {
		// TODO Auto-generated method stub
		return leaderBrdArr.get(position);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView==null){
			ViewHolder holder 		= new ViewHolder();
			inflator				= context.getLayoutInflater();
			convertView 			= inflator.inflate(R.layout.leaderboard_list_row, null);

			holder.text_rank		= (TextView) convertView.findViewById(R.id.text_rank_list);
			holder.text_name		= (TextView) convertView.findViewById(R.id.text_name_list);
			holder.text_points		= (TextView) convertView.findViewById(R.id.text_points_list);
			holder.img_back			= (ImageView) convertView.findViewById(R.id.img_back);

			holder.text_rank.setTypeface(tf);
			holder.text_name.setTypeface(tf);
			holder.text_points.setTypeface(tf);
			
			holder.img_back	.getBackground().setAlpha(80);

			convertView.setTag(holder);
		}

		ViewHolder hold = (ViewHolder) convertView.getTag();
		hold.text_name.setText(getItem(position).getUser_name().toString());
		hold.text_rank.setText(getItem(position).getRank().toString());
		hold.text_points.setText(getItem(position).getPoints().toString());

		return convertView;
	}


	static class ViewHolder{
		public TextView 	text_rank,text_name,text_points;
		ImageView	img_back;
	}


	private void setAlphaForView(View v, float alpha) {
		AlphaAnimation animation = new AlphaAnimation(alpha, alpha);
		animation.setDuration(0); 
		animation.setFillAfter(true); 
		v.startAnimation(animation);
	}


}
