package com.phonethics.adapters;

import java.util.ArrayList;


import com.phonethics.circularimageview.CircularImageView;
import com.phonethics.customclass.FeedbackInfo;
import com.phonethics.dancewithmadhuri.R;
import com.phonethics.imageloader.ImageLoader;





import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;



public class FeedbackAdapter extends ArrayAdapter<FeedbackInfo> {


	ArrayList<FeedbackInfo> 				feedBackInfoArr;
	Activity						context;
	private static LayoutInflater 	inflator = null;
	ImageLoader						imageLoader;


	public FeedbackAdapter(Activity context,ArrayList<FeedbackInfo> feedBackInfoArr){
		super(context,R.layout.tweet_list_layout, feedBackInfoArr);
		// TODO Auto-generated constructor stub
		this.context	= context;
		this.feedBackInfoArr = feedBackInfoArr;
		
		imageLoader	= new ImageLoader(context);



	}


	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return feedBackInfoArr.size();
	}

	@Override
	public FeedbackInfo getItem(int position) {
		// TODO Auto-generated method stub
		return feedBackInfoArr.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView==null){

			ViewHolder holder 		= new ViewHolder();
			inflator				= context.getLayoutInflater();

			convertView 			= inflator.inflate(R.layout.tweet_list_layout, null);

			holder.text_tweet		= (TextView) convertView.findViewById(R.id.text_tweet);
			holder.text_name		= (TextView) convertView.findViewById(R.id.text_name);
			holder.imgBack			= (ImageView) convertView.findViewById(R.id.img_back_trans);

			holder.text_tweet.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/KabelMediumBT.ttf"));
			holder.text_name.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/KabelMediumBT.ttf"));
			holder.text_name.setTextColor(Color.WHITE);
			holder.text_tweet.setTextColor(Color.parseColor("#9e9e9e"));
			
			holder.cirImage	= (CircularImageView) convertView.findViewById(R.id.img_profile);
			
			//setAlphaForView(holder.imgBack, 0.5f);
			holder.imgBack.getBackground().setAlpha(70);
			convertView.setTag(holder);

		}

		ViewHolder hold = (ViewHolder) convertView.getTag();


		try{
			
			hold.text_tweet.setText(getItem(position).getFeedbackText());
			hold.text_name.setText(getItem(position).getUserName());
			String 	userId 		= getItem(position).getUserId();
			String	imageUrl 	= "";
			String	picture		= getItem(position).getPicture();
			if(userId.contains("@")){
				imageUrl = context.getResources().getString(R.string.url_md_live_server)+picture;
			}else{
				imageUrl = "http://graph.facebook.com/"+userId+"/picture?type=large";
			}
			



			imageLoader.DisplayImage(imageUrl, hold.cirImage);
		}catch(Exception ex){
			ex.printStackTrace();
		}

		return convertView;
	}

	static class ViewHolder{
		public TextView text_tweet,text_name;
		public ImageView	imageView,imgBack;
		public CircularImageView cirImage;
	}

	private void setAlphaForView(View v, float alpha) {
		AlphaAnimation animation = new AlphaAnimation(alpha, alpha);
		animation.setDuration(0); 
		animation.setFillAfter(true); 
		v.startAnimation(animation);
	}


}
