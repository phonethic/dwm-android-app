package com.phonethics.adapters;

import java.util.ArrayList;

import com.phonethics.adapters.SubmissionsAdapter.ViewHolder;
import com.phonethics.customclass.SongInfo;
import com.phonethics.customclass.UsersVideo;
import com.phonethics.dancewithmadhuri.R;
import com.phonethics.imageloader.ImageLoader;

import android.app.Activity;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ImageView.ScaleType;

public class UsersVideoAdapter extends ArrayAdapter<UsersVideo> {

	ArrayList<UsersVideo> 			userVideoArr;
	private static LayoutInflater 	inflator = null;
	ImageLoader						imageLoader;
	Activity						context;

	int									IMAGE_HIGHT,SCREEN_WIDTH,THUMBNAIL_WIDTH,THUMBNAIL_HEIGHT;
	int									MARGIN_LEFT,MARGIN_TOP,MARGIN_BOTTOM;
	LinearLayout.LayoutParams 			params1;
	RelativeLayout.LayoutParams			params;
	RelativeLayout.LayoutParams			params2;
	boolean								isOldLayout = false;


	public UsersVideoAdapter(Activity context,ArrayList<UsersVideo> userVideoArr){
		super(context,R.layout.users_video_list_layout, userVideoArr);

		imageLoader	= new ImageLoader(context);
		imageLoader.setStubId(R.drawable.thumbnail);
		this.context =context;  
		this.userVideoArr = userVideoArr;


		imageLoader	= new ImageLoader(context);
		imageLoader.setStubId(R.drawable.thumbnail);

		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		SCREEN_WIDTH = metrics.widthPixels;

		if(isOldLayout){
			IMAGE_HIGHT 		= (int) ((SCREEN_WIDTH/4.44));
			MARGIN_LEFT 		= (int) ((SCREEN_WIDTH*6.25)/100);
			//MARGIN_LEFT 		= (int) ((SCREEN_WIDTH*5.625)/100);
			THUMBNAIL_WIDTH 	= (int) ((SCREEN_WIDTH*31.25)/100);
			THUMBNAIL_HEIGHT 	= IMAGE_HIGHT;
			params1 = new LinearLayout.LayoutParams(THUMBNAIL_WIDTH,LayoutParams.MATCH_PARENT);
			params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,IMAGE_HIGHT);
			params2 = new RelativeLayout.LayoutParams(THUMBNAIL_WIDTH,THUMBNAIL_HEIGHT);
			params1.setMargins(MARGIN_LEFT, 0, 0, 0);
			params2.setMargins(MARGIN_LEFT, 0, 0, 0);
		}else{

			//MARGIN_LEFT 		= (int) ((SCREEN_WIDTH*2.68)/100);
			//MARGIN_TOP			= (int) ((IMAGE_HIGHT*2.75)/100);
			//MARGIN_BOTTOM		= (int) ((IMAGE_HIGHT*2.75)/100);
			//MARGIN_LEFT 		= (int) ((SCREEN_WIDTH*5.625)/100);
			//THUMBNAIL_WIDTH 	= (int) ((SCREEN_WIDTH*31.314)/100);
			//THUMBNAIL_HEIGHT 	= (int) ((IMAGE_HIGHT*94.81)/100);
			//THUMBNAIL_HEIGHT 	= IMAGE_HIGHT;
			IMAGE_HIGHT 		= (int) ((SCREEN_WIDTH/3.317));
			THUMBNAIL_WIDTH	 	= (int)((SCREEN_WIDTH*33.125)/100);
			THUMBNAIL_HEIGHT 	= IMAGE_HIGHT;
			MARGIN_TOP			= (int) ((IMAGE_HIGHT*2.75)/100);
			MARGIN_BOTTOM		= (int) ((IMAGE_HIGHT*2.75)/100);

			params1 = new LinearLayout.LayoutParams(THUMBNAIL_WIDTH,LayoutParams.MATCH_PARENT);
			params 	= new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,IMAGE_HIGHT);
			params2 = new RelativeLayout.LayoutParams(THUMBNAIL_WIDTH,THUMBNAIL_HEIGHT);

			//params1.setMargins(MARGIN_LEFT+1, 0, 0, 0);
			params1.setMargins(0, MARGIN_TOP+1, 0, MARGIN_BOTTOM+1);
			//params2.setMargins(MARGIN_LEFT+1, 0, 0, 0);
			params2.setMargins(0, MARGIN_TOP+1, 0, MARGIN_BOTTOM+1);
		}
	}



	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return userVideoArr.size();
	}



	@Override
	public UsersVideo getItem(int position) {
		return userVideoArr.get(position);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView==null){
			ViewHolder holder 			= new ViewHolder();
			inflator					= context.getLayoutInflater();
			convertView 				= inflator.inflate(R.layout.users_video_list_layout, null);
			holder.text_userName		= (TextView)  convertView.findViewById(R.id.text_userName);
			holder.text_userVotes		= (TextView)  convertView.findViewById(R.id.text_userVotes);
			holder.img_back 		  	= (ImageView) convertView.findViewById(R.id.img_back);
			holder.img_thumbnail		= (ImageView) convertView.findViewById(R.id.img_song_thumbnail);
			holder.layoutPlayBttn		= (RelativeLayout) convertView.findViewById(R.id.layout_playBttn);
			holder.layout			 	= (LinearLayout) convertView.findViewById(R.id.linear_img_text);
			//holder.layoutPlayBttn.setLayoutParams(params);
			//holder.layout.setLayoutParams(params);
			//holder.img_thumbnail.setLayoutParams(params1);
			//holder.img_back.setLayoutParams(params);
			//holder.layout.setLayoutParams(params);
			//holder.layoutPlayBttn.setLayoutParams(params2);
			holder.img_back.setLayoutParams(params);
			holder.layout.setLayoutParams(params);
			holder.layoutPlayBttn.setLayoutParams(params2);
			holder.img_thumbnail.setLayoutParams(params1);
			holder.img_thumbnail.setVisibility(View.INVISIBLE);


			holder.text_userName.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/KabelMediumBT.ttf"));
			holder.text_userVotes.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/KabelMediumBT.ttf"));

			//holder.img_thumbnail.setScaleType(ScaleType.FIT_XY);

			convertView.setTag(holder);

		}

		ViewHolder hold = (ViewHolder) convertView.getTag();
		try{
			hold.text_userName.setText(getItem(position).getUserName());
			hold.text_userVotes.setText(getItem(position).getVotes()+" Votes");
			hold.img_thumbnail.setBackgroundResource(R.drawable.playbutton);
			/*imageLoader.DisplayImage(getItem(position).getPicture(), hold.img_thumbnail);*/
		}catch(Exception ex){
			ex.printStackTrace();
		}


		return convertView;
	}


	static class ViewHolder{
		public TextView 	text_userName,text_userVotes;
		RelativeLayout		layoutPlayBttn;
		ImageView			img_thumbnail,img_back;
		LinearLayout		layout;


	}

}
