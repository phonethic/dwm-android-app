package com.phonethics.adapters;

import java.util.ArrayList;

import com.phonethics.adapters.TweetsAdapter.ViewHolder;
import com.phonethics.customclass.CategoryDetails;
import com.phonethics.customclass.SongInfo;
import com.phonethics.dancewithmadhuri.R;
import com.phonethics.imageloader.ImageLoader;
import com.phonethics.md.VideoFull;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class CategoriesAdapter extends ArrayAdapter<CategoryDetails> {

	ArrayList<CategoryDetails> 			songInfoArr;
	private static LayoutInflater 		inflator = null;
	ImageLoader							imageLoader;
	Activity							context;
	int									IMAGE_HIGHT,SCREEN_WIDTH,THUMBNAIL_WIDTH,THUMBNAIL_HEIGHT;
	int									MARGIN_LEFT,MARGIN_TOP,MARGIN_BOTTOM;
	LinearLayout.LayoutParams 			params1;
	RelativeLayout.LayoutParams			params;
	RelativeLayout.LayoutParams			params2;
	boolean								isOldLayout = false;

	public CategoriesAdapter(Activity context,ArrayList<CategoryDetails> songInfoArr){
		super(context,R.layout.learn_dance_list, songInfoArr);

		imageLoader	= new ImageLoader(context);
		imageLoader.setStubId(R.drawable.thumbnail);
		this.context =context;  
		this.songInfoArr = songInfoArr;

		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		SCREEN_WIDTH = metrics.widthPixels;



		if(isOldLayout){
			IMAGE_HIGHT 		= (int) ((SCREEN_WIDTH/4.44));
			MARGIN_LEFT 		= (int) ((SCREEN_WIDTH*6.25)/100);
			//MARGIN_LEFT 		= (int) ((SCREEN_WIDTH*5.625)/100);
			THUMBNAIL_WIDTH 	= (int) ((SCREEN_WIDTH*31.25)/100);
			THUMBNAIL_HEIGHT 	= IMAGE_HIGHT;
			params1 = new LinearLayout.LayoutParams(THUMBNAIL_WIDTH,LayoutParams.MATCH_PARENT);
			params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,IMAGE_HIGHT);
			params2 = new RelativeLayout.LayoutParams(THUMBNAIL_WIDTH,THUMBNAIL_HEIGHT);
			params1.setMargins(MARGIN_LEFT, 0, 0, 0);
			params2.setMargins(MARGIN_LEFT, 0, 0, 0);
		}else{
			
			//MARGIN_LEFT 		= (int) ((SCREEN_WIDTH*2.68)/100);
			//MARGIN_TOP			= (int) ((IMAGE_HIGHT*2.75)/100);
			//MARGIN_BOTTOM		= (int) ((IMAGE_HIGHT*2.75)/100);
			//MARGIN_LEFT 		= (int) ((SCREEN_WIDTH*5.625)/100);
			//THUMBNAIL_WIDTH 	= (int) ((SCREEN_WIDTH*31.314)/100);
			//THUMBNAIL_HEIGHT 	= (int) ((IMAGE_HIGHT*94.81)/100);
			//THUMBNAIL_HEIGHT 	= IMAGE_HIGHT;
			IMAGE_HIGHT 		= (int) ((SCREEN_WIDTH/3.317));
			THUMBNAIL_WIDTH	 = (int)((SCREEN_WIDTH*33.125)/100);
			THUMBNAIL_HEIGHT = IMAGE_HIGHT;
			MARGIN_TOP			= (int) ((IMAGE_HIGHT*2.75)/100);
			MARGIN_BOTTOM		= (int) ((IMAGE_HIGHT*2.75)/100);
			
			params1 = new LinearLayout.LayoutParams(THUMBNAIL_WIDTH,LayoutParams.MATCH_PARENT);
			params 	= new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,IMAGE_HIGHT);
			params2 = new RelativeLayout.LayoutParams(THUMBNAIL_WIDTH,THUMBNAIL_HEIGHT);
			
			//params1.setMargins(MARGIN_LEFT+1, 0, 0, 0);
			params1.setMargins(0, MARGIN_TOP+1, 0, MARGIN_BOTTOM+1);
			//params2.setMargins(MARGIN_LEFT+1, 0, 0, 0);
			params2.setMargins(0, MARGIN_TOP+1, 0, MARGIN_BOTTOM+1);
			
			Log.d("", "Md>>SCREEN_WIDTH -- "+SCREEN_WIDTH);
			Log.d("", "Md>>IMAGE_HIGHT --  "+IMAGE_HIGHT);
			Log.d("", "Md>>THUMBNAIL_WIDTH -- "+THUMBNAIL_WIDTH);
			Log.d("", "Md>>THUMBNAIL_HEIGHT -- "+THUMBNAIL_HEIGHT);
			
		}


	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return songInfoArr.size();
	}

	@Override
	public CategoryDetails getItem(int position) {
		// TODO Auto-generated method stub
		return songInfoArr.get(position);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final int index  = position;
		if(convertView==null){

			ViewHolder holder 		= new ViewHolder();
			inflator				= context.getLayoutInflater();
			convertView 			= inflator.inflate(R.layout.learn_dance_list, null);
			holder.text_cat			= (TextView)  convertView.findViewById(R.id.text_song);
			holder.text_cat_name	= (TextView)  convertView.findViewById(R.id.text_song_name);
			holder.text_cat.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/KabelMediumBT.ttf"));
			holder.text_cat_name.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/KabelMediumBT.ttf"));
			holder.img_song_thumbnail = (ImageView) convertView.findViewById(R.id.img_song_thumbnail);
			holder.img_back 		  = (ImageView) convertView.findViewById(R.id.img_back);
			holder.img_playBttn		  = (ImageView) convertView.findViewById(R.id.img_play_bttn);
			holder.layoutPlayBttn		= (RelativeLayout) convertView.findViewById(R.id.layout_playBttn);
			holder.layout			  = (LinearLayout) convertView.findViewById(R.id.linear_img_text);
			holder.img_back.setLayoutParams(params);
			holder.layout.setLayoutParams(params);
			//holder.img_playBttn.setLayoutParams(params2);
			holder.layoutPlayBttn.setLayoutParams(params2);


			holder.img_song_thumbnail.setLayoutParams(params1);
			/*holder.img_playBttn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					String title = songInfoArr.get(index).getStyle_name();
					String vLink = songInfoArr.get(index).getVideo_link();
					//Toast.makeText(context, title, 0).show();
					if(vLink.equalsIgnoreCase("")){

					}else{
						Log.d("", "Md == "+title+" == "+vLink);
						Intent intent = new Intent(context, VideoFull.class);
						intent.putExtra("url_play", vLink);
						intent.putExtra("current_pos", 0);
						context.startActivity(intent);
						context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}

				}
			});*/
			String vLink = songInfoArr.get(index).getVideo_link();
			if(vLink.equalsIgnoreCase("")){
				holder.img_playBttn.setVisibility(View.GONE);
			}else{
				holder.img_playBttn.setVisibility(View.VISIBLE);
			}

			holder.img_song_thumbnail.setScaleType(ScaleType.CENTER_CROP);

			convertView.setTag(holder);

		}

		ViewHolder hold = (ViewHolder) convertView.getTag();
		try{
			String vLink = songInfoArr.get(index).getVideo_link();
			if(vLink.equalsIgnoreCase("")){
				hold.img_playBttn.setVisibility(View.GONE);
			}else{
				hold.img_playBttn.setVisibility(View.VISIBLE);
			}
			hold.text_cat.setText("Dance Style");
			hold.text_cat.setVisibility(View.GONE);
			hold.text_cat_name.setText(getItem(position).getStyle_name());
			imageLoader.DisplayImage(getItem(position).getThumbnail().replace(" ", "%20"), hold.img_song_thumbnail);
			hold.img_playBttn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					String title = songInfoArr.get(index).getStyle_name();
					String vLink = songInfoArr.get(index).getVideo_link();
					//Toast.makeText(context, title, 0).show();
					if(vLink.equalsIgnoreCase("")){

					}else{
						Log.d("", "Md == "+title+" == "+vLink);
						Intent intent = new Intent(context, VideoFull.class);
						intent.putExtra("url_play", vLink);
						intent.putExtra("current_pos", 0);
						context.startActivity(intent);
						context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}

				}
			});
		}catch(Exception ex){
			ex.printStackTrace();
		}


		return convertView;
	}





	static class ViewHolder{
		public TextView 	text_cat,text_cat_name;
		public ImageView	img_song_thumbnail,img_back,img_playBttn;
		LinearLayout		layout;
		RelativeLayout		layoutPlayBttn;
	}



}
