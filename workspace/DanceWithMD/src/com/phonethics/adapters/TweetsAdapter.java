package com.phonethics.adapters;

import java.util.ArrayList;


import com.phonethics.circularimageview.CircularImageView;
import com.phonethics.dancewithmadhuri.R;
import com.phonethics.imageloader.ImageLoader;





import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;



public class TweetsAdapter extends ArrayAdapter<String> {


	ArrayList<String> 				tweetsArr,img_url,screen_name_arr;
	Activity						context;
	private static LayoutInflater 	inflator = null;
	ImageLoader						imageLoader;


	public TweetsAdapter(Activity context,ArrayList<String> tweets,ArrayList<String> img_url,ArrayList<String> screen_name_arr){
		super(context,R.layout.tweet_list_layout, tweets);
		// TODO Auto-generated constructor stub
		this.context	= context;
		this.tweetsArr = tweets;
		this.img_url= img_url; 
		this.screen_name_arr = screen_name_arr;
		imageLoader	= new ImageLoader(context);



	}


	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return tweetsArr.size();
	}

	@Override
	public String getItem(int position) {
		// TODO Auto-generated method stub
		return tweetsArr.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView==null){

			ViewHolder holder 		= new ViewHolder();
			inflator				= context.getLayoutInflater();

			convertView 			= inflator.inflate(R.layout.tweet_list_layout, null);

			holder.text_tweet		= (TextView) convertView.findViewById(R.id.text_tweet);
			holder.text_name		= (TextView) convertView.findViewById(R.id.text_name);
			holder.imgBack			= (ImageView) convertView.findViewById(R.id.img_back_trans);

			holder.text_tweet.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/KabelMediumBT.ttf"));
			holder.text_name.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/KabelMediumBT.ttf"));
			holder.text_name.setTextColor(Color.WHITE);
			holder.text_tweet.setTextColor(Color.parseColor("#9e9e9e"));
			
			holder.cirImage	= (CircularImageView) convertView.findViewById(R.id.img_profile);
			
			setAlphaForView(holder.imgBack, 0.5f);

			convertView.setTag(holder);

		}

		ViewHolder hold = (ViewHolder) convertView.getTag();


		try{
			hold.text_tweet.setText(getItem(position));
			hold.text_name.setText(screen_name_arr.get(position));

			



			imageLoader.DisplayImage(img_url.get(position), hold.cirImage);
		}catch(Exception ex){
			ex.printStackTrace();
		}

		return convertView;
	}

	static class ViewHolder{
		public TextView text_tweet,text_name;
		public ImageView	imageView,imgBack;
		public CircularImageView cirImage;
	}

	private void setAlphaForView(View v, float alpha) {
		AlphaAnimation animation = new AlphaAnimation(alpha, alpha);
		animation.setDuration(0); 
		animation.setFillAfter(true); 
		v.startAnimation(animation);
	}


}
