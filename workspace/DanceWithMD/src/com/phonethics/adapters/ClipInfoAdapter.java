package com.phonethics.adapters;

import java.util.ArrayList;

import com.phonethics.adapters.TweetsAdapter.ViewHolder;
import com.phonethics.customclass.CategoryDetails;
import com.phonethics.customclass.ClipInfo;
import com.phonethics.customclass.SongInfo;
import com.phonethics.dancewithmadhuri.R;
import com.phonethics.imageloader.ImageLoader;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ClipInfoAdapter extends ArrayAdapter<ClipInfo> {

	ArrayList<ClipInfo> 			clipInfoArr;
	private static LayoutInflater 	inflator = null;
	ImageLoader						imageLoader;
	Activity						context;
	String 							seenVideos;
	String							lock_type;
	int count;
	int									IMAGE_HIGHT,SCREEN_WIDTH,THUMBNAIL_WIDTH,THUMBNAIL_HEIGHT;
	int									MARGIN_LEFT,MARGIN_TOP,MARGIN_BOTTOM;
	LinearLayout.LayoutParams 			params1;
	RelativeLayout.LayoutParams			params;
	RelativeLayout.LayoutParams			params2;
	boolean								isOldLayout = false;

	public ClipInfoAdapter(Activity context,ArrayList<ClipInfo> clipInfoArr,String seenVideos,String lock_type){
		super(context,R.layout.learn_dance_list, clipInfoArr);

		imageLoader	= new ImageLoader(context);
		imageLoader.setStubId(R.drawable.thumbnail);
		this.context =context;  
		this.clipInfoArr = clipInfoArr;
		this.seenVideos = seenVideos;
		count = Integer.parseInt(seenVideos);
		this.lock_type = lock_type;


		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		SCREEN_WIDTH = metrics.widthPixels;
		if(isOldLayout){
			IMAGE_HIGHT 		= (int) ((SCREEN_WIDTH/4.44));
			MARGIN_LEFT 		= (int) ((SCREEN_WIDTH*6.25)/100);
			//MARGIN_LEFT 		= (int) ((SCREEN_WIDTH*5.625)/100);
			THUMBNAIL_WIDTH 	= (int) ((SCREEN_WIDTH*31.25)/100);
			THUMBNAIL_HEIGHT 	= IMAGE_HIGHT;
			params1 = new LinearLayout.LayoutParams(THUMBNAIL_WIDTH,LayoutParams.MATCH_PARENT);
			params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,IMAGE_HIGHT);
			params2 = new RelativeLayout.LayoutParams(THUMBNAIL_WIDTH,THUMBNAIL_HEIGHT);
			params1.setMargins(MARGIN_LEFT, 0, 0, 0);
			params2.setMargins(MARGIN_LEFT, 0, 0, 0);

		}else{
			//MARGIN_LEFT 		= (int) ((SCREEN_WIDTH*2.68)/100);
			//MARGIN_TOP			= (int) ((IMAGE_HIGHT*2.75)/100);
			//MARGIN_BOTTOM		= (int) ((IMAGE_HIGHT*2.75)/100);
			//MARGIN_LEFT 		= (int) ((SCREEN_WIDTH*5.625)/100);
			//THUMBNAIL_WIDTH 	= (int) ((SCREEN_WIDTH*31.314)/100);
			//THUMBNAIL_HEIGHT 	= (int) ((IMAGE_HIGHT*94.81)/100);
			//THUMBNAIL_HEIGHT 	= IMAGE_HIGHT;
			IMAGE_HIGHT 		= (int) ((SCREEN_WIDTH/3.317));
			THUMBNAIL_WIDTH	 = (int)((SCREEN_WIDTH*33.125)/100);
			THUMBNAIL_HEIGHT = IMAGE_HIGHT;
			MARGIN_TOP			= (int) ((IMAGE_HIGHT*2.75)/100);
			MARGIN_BOTTOM		= (int) ((IMAGE_HIGHT*2.75)/100);
			
			params1 = new LinearLayout.LayoutParams(THUMBNAIL_WIDTH,LayoutParams.MATCH_PARENT);
			params 	= new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,IMAGE_HIGHT);
			params2 = new RelativeLayout.LayoutParams(THUMBNAIL_WIDTH,THUMBNAIL_HEIGHT);
			
			//params1.setMargins(MARGIN_LEFT+1, 0, 0, 0);
			params1.setMargins(0, MARGIN_TOP+1, 0, MARGIN_BOTTOM+1);
			//params2.setMargins(MARGIN_LEFT+1, 0, 0, 0);
			params2.setMargins(0, MARGIN_TOP+1, 0, MARGIN_BOTTOM+1);
			
			Log.d("", "Md>>SCREEN_WIDTH -- "+SCREEN_WIDTH);
			Log.d("", "Md>>IMAGE_HIGHT --  "+IMAGE_HIGHT);
			Log.d("", "Md>>THUMBNAIL_WIDTH -- "+THUMBNAIL_WIDTH);
			Log.d("", "Md>>THUMBNAIL_HEIGHT -- "+THUMBNAIL_HEIGHT);
		}


	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return clipInfoArr.size();
	}

	@Override
	public ClipInfo getItem(int position) {
		// TODO Auto-generated method stub
		return clipInfoArr.get(position);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView==null){
			ViewHolder holder 		= new ViewHolder();
			inflator				= context.getLayoutInflater();
			convertView 			= inflator.inflate(R.layout.learn_dance_list, null);
			holder.text_cat		= (TextView)  convertView.findViewById(R.id.text_song);
			holder.text_cat_name	= (TextView)  convertView.findViewById(R.id.text_song_name);
			holder.text_cat.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/KabelMediumBT.ttf"));
			holder.text_cat_name.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/KabelMediumBT.ttf"));
			holder.img_song_thumbnail	= (ImageView) convertView.findViewById(R.id.img_song_thumbnail);
			//holder.img_song_thumbnail.setVisibility(View.GONE);


			holder.img_back 		  = (ImageView) convertView.findViewById(R.id.img_back);
			holder.layout			  = (LinearLayout) convertView.findViewById(R.id.linear_img_text);
			holder.layout			  = (LinearLayout) convertView.findViewById(R.id.linear_img_text);
			holder.layoutPlayBttn		= (RelativeLayout) convertView.findViewById(R.id.layout_playBttn);
			holder.img_playBttn		  = (ImageView) convertView.findViewById(R.id.img_play_bttn);
			holder.img_back.setLayoutParams(params);
			holder.layout.setLayoutParams(params);
			//holder.img_playBttn.setLayoutParams(params2);
			holder.layoutPlayBttn.setLayoutParams(params2);

			holder.img_song_thumbnail.setLayoutParams(params1);

			convertView.setTag(holder);

		}

		ViewHolder hold = (ViewHolder) convertView.getTag();
		try{
			hold.text_cat.setText("Lesson");
			hold.text_cat.setVisibility(View.GONE);
			hold.text_cat_name.setText(getItem(position).getClip_tittle());
			//hold.img_song_thumbnail.setVisibility(View.GONE);

			/*if(position<=count){
				//hold.img_song_thumbnail.setBackgroundResource(R.drawable.playbutton);
				hold.img_song_thumbnail.setVisibility(View.INVISIBLE);
				hold.img_playBttn.setVisibility(View.VISIBLE);
			}else{
				hold.img_song_thumbnail.setBackgroundResource(R.drawable.lock);
				hold.img_song_thumbnail.setVisibility(View.VISIBLE);
				hold.img_playBttn.setVisibility(View.GONE);
			}*/

			if(lock_type.equalsIgnoreCase("0")){
				//hold.img_song_thumbnail.setBackgroundResource(R.drawable.playbutton);
				hold.img_song_thumbnail.setVisibility(View.INVISIBLE);
				hold.img_playBttn.setVisibility(View.VISIBLE);


			}else{
				if(position<=count){
					//hold.img_song_thumbnail.setBackgroundResource(R.drawable.playbutton);
					hold.img_song_thumbnail.setVisibility(View.INVISIBLE);
					hold.img_playBttn.setVisibility(View.VISIBLE);
				}else{
					hold.img_song_thumbnail.setBackgroundResource(R.drawable.lock);
					hold.img_song_thumbnail.setVisibility(View.VISIBLE);
					hold.img_playBttn.setVisibility(View.GONE);
				}
			}


			//imageLoader.DisplayImage(getItem(position).getThumbnail(), hold.img_song_thumbnail);
		}catch(Exception ex){
			ex.printStackTrace();
		}


		return convertView;
	}





	static class ViewHolder{
		public TextView 	text_cat,text_cat_name;
		public ImageView	img_song_thumbnail,img_back,img_playBttn;
		LinearLayout		layout;
		RelativeLayout		layoutPlayBttn;
	}



}
