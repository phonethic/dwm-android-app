package com.phonethics.adapters;

import java.util.ArrayList;

import com.phonethics.adapters.TweetsAdapter.ViewHolder;
import com.phonethics.customclass.SongInfo;
import com.phonethics.dancewithmadhuri.R;
import com.phonethics.imageloader.ImageLoader;

import android.accounts.OnAccountsUpdateListener;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ImageView.ScaleType;
import android.widget.Toast;

public class LearnDanceAdapter extends ArrayAdapter<SongInfo> {

	ArrayList<SongInfo> 			songInfoArr;
	private static LayoutInflater 	inflator = null;
	ImageLoader						imageLoader;
	Activity						context;

	int									IMAGE_HIGHT,SCREEN_WIDTH,THUMBNAIL_WIDTH,THUMBNAIL_HEIGHT,SONG_CREDITS_IMG_WIDTH;
	int									MARGIN_LEFT,MARGIN_TOP,MARGIN_BOTTOM,SONG_CREDITS_IMG_HEIGHT;
	LinearLayout.LayoutParams 			params1,songCreditLayout;
	RelativeLayout.LayoutParams			params;
	RelativeLayout.LayoutParams			params2,song_credits_params;
	boolean								isOldLayout = false;
	String								catName;
	int									index;
	Typeface							tf;
	Animation							fade;
	LinearLayout						songCreditLayout1;
	ArrayList<Boolean> 					textCreditVisibleArr;

	public LearnDanceAdapter(Activity context,ArrayList<SongInfo> songInfoArr,String catName){
		super(context,R.layout.learn_dance_list, songInfoArr);

		imageLoader	= new ImageLoader(context);
		imageLoader.setStubId(R.drawable.thumbnail);
		this.context =context;  
		this.songInfoArr = songInfoArr;
		this.catName = catName;
		fade = AnimationUtils.loadAnimation(context, R.anim.fade_out);
		tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/KabelMediumBT.ttf");
		textCreditVisibleArr = new ArrayList<Boolean>();
		for(int i=0;i<songInfoArr.size();i++){
			textCreditVisibleArr.add(false);
		}

		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		SCREEN_WIDTH = metrics.widthPixels;
		if(isOldLayout){
			IMAGE_HIGHT 		= (int) ((SCREEN_WIDTH/4.44));
			MARGIN_LEFT 		= (int) ((SCREEN_WIDTH*6.25)/100);
			//MARGIN_LEFT 		= (int) ((SCREEN_WIDTH*5.625)/100);
			THUMBNAIL_WIDTH 	= (int) ((SCREEN_WIDTH*31.25)/100);
			THUMBNAIL_HEIGHT 	= IMAGE_HIGHT;
			params1 = new LinearLayout.LayoutParams(THUMBNAIL_WIDTH,LayoutParams.MATCH_PARENT);
			params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,IMAGE_HIGHT);
			params2 = new RelativeLayout.LayoutParams(THUMBNAIL_WIDTH,THUMBNAIL_HEIGHT);
			params1.setMargins(MARGIN_LEFT, 0, 0, 0);
			params2.setMargins(MARGIN_LEFT, 0, 0, 0);
		}else{
			//MARGIN_LEFT 		= (int) ((SCREEN_WIDTH*2.68)/100);
			//MARGIN_TOP			= (int) ((IMAGE_HIGHT*2.75)/100);
			//MARGIN_BOTTOM		= (int) ((IMAGE_HIGHT*2.75)/100);
			//MARGIN_LEFT 		= (int) ((SCREEN_WIDTH*5.625)/100);
			//THUMBNAIL_WIDTH 	= (int) ((SCREEN_WIDTH*31.314)/100);
			//THUMBNAIL_HEIGHT 	= (int) ((IMAGE_HIGHT*94.81)/100);
			//THUMBNAIL_HEIGHT 	= IMAGE_HIGHT;
			IMAGE_HIGHT 		= (int) ((SCREEN_WIDTH/3.317));
			THUMBNAIL_WIDTH	 	= (int)((SCREEN_WIDTH*33.125)/100);
			THUMBNAIL_HEIGHT 	= IMAGE_HIGHT;
			MARGIN_TOP			= (int) ((IMAGE_HIGHT*2.75)/100);
			MARGIN_BOTTOM		= (int) ((IMAGE_HIGHT*2.75)/100);
			
			SONG_CREDITS_IMG_WIDTH 	= (int)((SCREEN_WIDTH*9.02/100));
			SONG_CREDITS_IMG_HEIGHT = (int)(((SONG_CREDITS_IMG_WIDTH)/0.33));

			params1 = new LinearLayout.LayoutParams(THUMBNAIL_WIDTH,LayoutParams.MATCH_PARENT);
			params 	= new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,IMAGE_HIGHT);
			params2 = new RelativeLayout.LayoutParams(THUMBNAIL_WIDTH,THUMBNAIL_HEIGHT);
			songCreditLayout = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT);

			songCreditLayout1 = new LinearLayout(context);
			song_credits_params = new RelativeLayout.LayoutParams(SONG_CREDITS_IMG_WIDTH,THUMBNAIL_HEIGHT);
			//song_credits_params.setMargins(0, MARGIN_TOP+1, 2, 0);
			song_credits_params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

			//params1.setMargins(MARGIN_LEFT+1, 0, 0, 0);
			params1.setMargins(0, MARGIN_TOP+1, 0, MARGIN_BOTTOM+1);
			//params2.setMargins(MARGIN_LEFT+1, 0, 0, 0);.
			params2.setMargins(0, MARGIN_TOP+1, 0, MARGIN_BOTTOM+1);
			
			songCreditLayout.setMargins(0,  MARGIN_TOP+1,0, MARGIN_BOTTOM+1);

			Log.d("", "Md>>SCREEN_WIDTH -- "+SCREEN_WIDTH);
			Log.d("", "Md>>IMAGE_HIGHT --  "+IMAGE_HIGHT);
			Log.d("", "Md>>THUMBNAIL_WIDTH -- "+THUMBNAIL_WIDTH);
			Log.d("", "Md>>THUMBNAIL_HEIGHT -- "+THUMBNAIL_HEIGHT);
		}


	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return songInfoArr.size();
	}

	@Override
	public SongInfo getItem(int position) {
		// TODO Auto-generated method stub
		return songInfoArr.get(position);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final int index = position;
		if(convertView==null){
			final ViewHolder holder 		= new ViewHolder();
			inflator				= context.getLayoutInflater();
			convertView 				= inflator.inflate(R.layout.learn_dance_list_with_song_credits, null);
			holder.text_song			= (TextView)  convertView.findViewById(R.id.text_song);
			holder.text_movie			= (TextView)  convertView.findViewById(R.id.text_movie);
			holder.text_movieName		= (TextView)  convertView.findViewById(R.id.text_movie_detials);
			holder.text_choreographer	= (TextView)  convertView.findViewById(R.id.text_choreographer);
			holder.text_choreographerName	= (TextView)  convertView.findViewById(R.id.text_choreographer_name);

			holder.text_song_name	= (TextView)  convertView.findViewById(R.id.text_song_name);
			holder.text_song.setTypeface(tf);
			holder.text_song_name.setTypeface(tf);
			holder.text_movieName.setTypeface(tf);
			holder.text_movie.setTypeface(tf);
			holder.text_choreographer.setTypeface(tf);
			holder.text_choreographerName.setTypeface(tf);



			holder.img_song_thumbnail		= (ImageView) convertView.findViewById(R.id.img_song_thumbnail);
			holder.img_songCredit			= (ImageView) convertView.findViewById(R.id.img_song_credits); 


			holder.img_back 		  		= (ImageView) convertView.findViewById(R.id.img_back);
			holder.layout			  		= (LinearLayout) convertView.findViewById(R.id.linear_img_text);
			holder.layout			  	  	= (LinearLayout) convertView.findViewById(R.id.linear_img_text);
			holder.layout_song			  	= (LinearLayout) convertView.findViewById(R.id.layout_song);
			holder.layout_song_credits 		= (LinearLayout) convertView.findViewById(R.id.layout_song_credits); 
			holder.layoutPlayBttn			= (RelativeLayout) convertView.findViewById(R.id.layout_playBttn);
			holder.img_playBttn		  		= (ImageView) convertView.findViewById(R.id.img_play_bttn);
			holder.img_back.setLayoutParams(params);
			holder.layout.setLayoutParams(params);
			//holder.layout_song_credits.setLayoutParams(params);
			//holder.layout_song.setLayoutParams(params);
			//holder.img_playBttn.setLayoutParams(params2);
			holder.layoutPlayBttn.setLayoutParams(params2);

			holder.img_song_thumbnail.setLayoutParams(params1);
			holder.img_song_thumbnail.setScaleType(ScaleType.CENTER_CROP);

			//holder.layout_song_credits.setLayoutParams(songCreditLayout);
			holder.img_songCredit.setLayoutParams(song_credits_params);
			holder.img_songCredit.setScaleType(ScaleType.FIT_XY);
			holder.img_songCredit.setBackgroundResource(R.drawable.song_credits_off);
//			holder.img_songCredit.setOnClickListener(new OnClickListener() {
//
//				@Override
//				public void onClick(View v) {
//					// TODO Auto-generated method stub
//					//Toast.makeText(context, "clicked "+index, 0).show();
//					if(holder.layout_song.isShown() && !textCreditVisibleArr.get(index)){
//						textCreditVisibleArr.set(index, true);
//						holder.img_songCredit.setBackgroundResource(R.drawable.song_credits_on);
//						//holder.img_songCredit.setBackgroundColor(Color.WHITE);
//						
//						holder.layout_song.setVisibility(View.GONE);
//				 		holder.layout_song_credits.setVisibility(View.VISIBLE);
//						
//						
//						Animation anim = new TranslateAnimation(((SCREEN_WIDTH/2)-(SONG_CREDITS_IMG_WIDTH-25)), 0, 0, 0);
//						
//						anim.setDuration(1000);
//						holder.layout_song_credits.startAnimation(anim);
//						anim.setAnimationListener(new AnimationListener() {
//							
//							@Override
//							public void onAnimationStart(Animation animation) {
//								// TODO Auto-generated method stub
//								holder.img_songCredit.bringToFront();
//							}
//							
//							@Override
//							public void onAnimationRepeat(Animation animation) {
//								// TODO Auto-generated method stub
//								
//							}
//							
//							@Override
//							public void onAnimationEnd(Animation animation) {
//								// TODO Auto-generated method stub
//								
//							}
//						});
//						//anim.setFillAfter(true);
//
//					}else{
//						
//
//						textCreditVisibleArr.set(index, false);
//						Animation anim = new TranslateAnimation(0, ((SCREEN_WIDTH/2)-(SONG_CREDITS_IMG_WIDTH-25)), 0, 0);
//						anim.setDuration(1000);
//						holder.layout_song_credits.startAnimation(anim);
//						//anim.setFillAfter(true);
//						
//					
//						anim.setAnimationListener(new AnimationListener() {
//							
//							@Override
//							public void onAnimationStart(Animation animation) {
//								// TODO Auto-generated method stub
//								//holder.img_songCredit.setBackgroundResource(R.drawable.song_credits_on);
//								holder.img_songCredit.bringToFront();
//							}
//							
//							@Override
//							public void onAnimationRepeat(Animation animation) {
//								// TODO Auto-generated method stub
//								
//							}
//							
//							@Override
//							public void onAnimationEnd(Animation animation) {
//								// TODO Auto-generated method stub
//								fade = AnimationUtils.loadAnimation(context, R.anim.fade_out);
//								holder.layout_song.setVisibility(View.VISIBLE);
//								
//								holder.img_songCredit.startAnimation(fade);
//								//fade_out.setFillAfter(true);
//								fade.setAnimationListener(new AnimationListener() {
//									
//									@Override
//									public void onAnimationStart(Animation animation) {
//										// TODO Auto-generated method stub
//										holder.layout_song_credits.setVisibility(View.GONE);
//									}
//									
//									@Override
//									public void onAnimationRepeat(Animation animation) {
//										// TODO Auto-generated method stub
//										
//									}
//									
//									@Override
//									public void onAnimationEnd(Animation animation) {
//										// TODO Auto-generated method stub
//										holder.img_songCredit.setBackgroundResource(R.drawable.song_credits_off);
//										fade = AnimationUtils.loadAnimation(context, R.anim.fade_in);
//										fade.setFillAfter(true);
//										holder.img_songCredit.startAnimation(fade);
//									}
//								});
//							}
//						});
//					}
//				}
//			});

			convertView.setTag(holder);

		}

		final ViewHolder hold = (ViewHolder) convertView.getTag();
		try{
			hold.text_choreographer.setText("Choreographer");
			hold.text_movie.setText("Movie");
			
			hold.text_movieName.setText(getItem(position).getMovie());
			hold.text_choreographerName.setText(getItem(position).getChoreographer());
			hold.text_song.setText(catName);
			hold.text_song.setVisibility(View.GONE);
			hold.text_song_name.setText(getItem(position).getTitle());
			imageLoader.DisplayImage(getItem(position).getThumbnail().replaceAll(" ", "%20"), hold.img_song_thumbnail);
			
			if(textCreditVisibleArr.get(position)){
				hold.layout_song_credits.setVisibility(View.VISIBLE);
				hold.layout_song.setVisibility(View.GONE);
				hold.img_songCredit.setBackgroundResource(R.drawable.song_credits_on);
			}else{
				hold.layout_song_credits.setVisibility(View.GONE);
				hold.layout_song.setVisibility(View.VISIBLE);
				hold.img_songCredit.setBackgroundResource(R.drawable.song_credits_off);
			}
			
			final OnClickListener layoutListener = new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub


					
						
						//Toast.makeText(context, "clicked "+index).show();
						if(hold.layout_song.isShown() && !textCreditVisibleArr.get(index)){
							textCreditVisibleArr.set(index, true);
							hold.img_songCredit.setBackgroundResource(R.drawable.song_credits_on);
							//hold.img_songCredit.setBackgroundColor(Color.WHITE);
							
							hold.layout_song.setVisibility(View.GONE);
					 		hold.layout_song_credits.setVisibility(View.VISIBLE);
							
							
							Animation anim = new TranslateAnimation(((SCREEN_WIDTH/2)-(SONG_CREDITS_IMG_WIDTH-25)), 0, 0, 0);
							
							anim.setDuration(1000);
							hold.layout_song_credits.startAnimation(anim);
							anim.setAnimationListener(new AnimationListener() {
								
								@Override
								public void onAnimationStart(Animation animation) {
									// TODO Auto-generated method stub
									hold.img_songCredit.bringToFront();
								}
								
								@Override
								public void onAnimationRepeat(Animation animation) {
									// TODO Auto-generated method stub
									
								}
								
								@Override
								public void onAnimationEnd(Animation animation) {
									// TODO Auto-generated method stub
									
								}
							});
							//anim.setFillAfter(true);

						}else{
							

							textCreditVisibleArr.set(index, false);
							Animation anim = new TranslateAnimation(0, ((SCREEN_WIDTH/2)-(SONG_CREDITS_IMG_WIDTH-25)), 0, 0);
							anim.setDuration(1000);
							hold.layout_song_credits.startAnimation(anim);
							//anim.setFillAfter(true);
							
						
							anim.setAnimationListener(new AnimationListener() {
								
								@Override
								public void onAnimationStart(Animation animation) {
									// TODO Auto-generated method stub
									//hold.img_songCredit.setBackgroundResource(R.drawable.song_credits_on);
									hold.img_songCredit.bringToFront();
								}
								
								@Override
								public void onAnimationRepeat(Animation animation) {
									// TODO Auto-generated method stub
									
								}
								
								@Override
								public void onAnimationEnd(Animation animation) {
									// TODO Auto-generated method stub
									fade = AnimationUtils.loadAnimation(context, R.anim.fade_out);
									hold.layout_song.setVisibility(View.VISIBLE);
									
									hold.img_songCredit.startAnimation(fade);
									//fade_out.setFillAfter(true);
									fade.setAnimationListener(new AnimationListener() {
										
										@Override
										public void onAnimationStart(Animation animation) {
											// TODO Auto-generated method stub
											hold.layout_song_credits.setVisibility(View.GONE);
										}
										
										@Override
										public void onAnimationRepeat(Animation animation) {
											// TODO Auto-generated method stub
											
										}
										
										@Override
										public void onAnimationEnd(Animation animation) {
											// TODO Auto-generated method stub
											hold.img_songCredit.setBackgroundResource(R.drawable.song_credits_off);
											fade = AnimationUtils.loadAnimation(context, R.anim.fade_in);
											fade.setFillAfter(true);
											hold.img_songCredit.startAnimation(fade);
										}
									});
								}
							});
						}
					
				
				}
			};
			
			hold.img_songCredit.setOnClickListener(layoutListener);
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}


		return convertView;
	}





	static class ViewHolder{
		public  TextView 	text_song,text_song_name,text_movie,text_movieName,text_choreographer,text_choreographerName;
		public ImageView	img_song_thumbnail,img_back,img_playBttn,img_songCredit;
		LinearLayout		layout,layout_song_credits,layout_song;
		RelativeLayout		layoutPlayBttn;
	}

	public void swithLayout(View view1, View view2){

	}

	
	
	
	



}
