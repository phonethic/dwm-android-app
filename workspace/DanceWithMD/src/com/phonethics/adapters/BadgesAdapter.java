package com.phonethics.adapters;

import java.util.ArrayList;


import com.phonethics.dancewithmadhuri.R;
import com.phonethics.imageloader.ImageLoader;





import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;



public class BadgesAdapter extends ArrayAdapter<String> {


	ArrayList<String> 				badgeName_arr,badgesImage_arr,badgeCount_arr;
	Activity						context;
	private static LayoutInflater 	inflator = null;
	ImageLoader						imageLoader;


	public BadgesAdapter(Activity context,ArrayList<String> badgeName_arr,ArrayList<String> badgesImage_arr,ArrayList<String> badgeCount_arr){
		super(context,R.layout.badges_list_layout, badgeName_arr);
		// TODO Auto-generated constructor stub
		this.context	= context;
		this.badgeName_arr = badgeName_arr;
		this.badgesImage_arr= badgesImage_arr; 
		this.badgeCount_arr = badgeCount_arr;
		imageLoader	= new ImageLoader(context);



	}


	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return badgeName_arr.size();
	}

	@Override
	public String getItem(int position) {
		// TODO Auto-generated method stub
		return badgeName_arr.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView==null){

			ViewHolder holder 		= new ViewHolder();
			inflator				= context.getLayoutInflater();

			convertView 			= inflator.inflate(R.layout.badges_list_layout, null);

			holder.text_badge_name		= (TextView) convertView.findViewById(R.id.text_badge_name);
			holder.text_badge_count		= (TextView) convertView.findViewById(R.id.text_count);

			holder.text_badge_name.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/KabelMediumBT.ttf"));
			holder.text_badge_count.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/KabelMediumBT.ttf"));
			holder.text_badge_name.setTextColor(Color.WHITE);
			holder.text_badge_count.setTextColor(Color.WHITE);

			holder.imageView	= (ImageView) convertView.findViewById(R.id.img_badge);

			convertView.setTag(holder);

		}

		ViewHolder hold = (ViewHolder) convertView.getTag();


		try{
			hold.text_badge_name.setText(getItem(position));
			hold.text_badge_count.setText(badgeCount_arr.get(position));
			imageLoader.DisplayImage(badgesImage_arr.get(position), hold.imageView);
		}catch(Exception ex){
			ex.printStackTrace();
		}

		return convertView;
	}

	static class ViewHolder{
		public TextView 	text_badge_name,text_badge_count;
		public ImageView	imageView;
	}




}
