package com.phonethics.adapters;

import java.io.File;
import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.phonethics.dancewithmadhuri.R;
import com.phonethics.dancewithmadhuri.YoutubeVideoPlay;
import com.phonethics.imageloader.ImageLoader;
import com.phonethics.network.NetworkCheck;

public class StarSpeakListViewAdapter extends ArrayAdapter<String> {

	ArrayList<String> title, thumbnail, description, videoLink;
	ArrayList<Integer> videoTypeArrayList;
	Context context;
	LayoutInflater inflater;
	/*DisplayImageOptions options;
	ImageLoaderConfiguration config;*/
	File cacheDir;
	ImageLoader loader;
	Typeface tf;

	public StarSpeakListViewAdapter(Context context, int textViewResourceId, ArrayList<String> title, ArrayList<String> thumbnail, ArrayList<String> description, ArrayList<String> videoLink, ArrayList<Integer> videoTypeArrayList) {
		super(context, textViewResourceId);

		this.context=context;
		this.title=title;
		this.thumbnail=thumbnail;
		this.description=description;
		this.videoLink=videoLink;
		this.videoTypeArrayList=videoTypeArrayList;
		loader= new ImageLoader(context);
		tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/KabelMediumBT.ttf");
		inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		//------------Set configuration for Image loader-------------------
		NetworkCheck network = new NetworkCheck(context);

		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
		{
			cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"/.parsexml2");
		}
		else
		{
			cacheDir=context.getCacheDir();
		}

		if(!cacheDir.exists())
		{
			cacheDir.mkdirs();
		}else if(network.isNetworkAvailable())
		{

		}

		/*config= new ImageLoaderConfiguration.Builder(context)

		.denyCacheImageMultipleSizesInMemory()
		.threadPoolSize(4)

		.discCache(new UnlimitedDiscCache(cacheDir))
		.build();
		loader.init(config);
		options = new DisplayImageOptions.Builder()
		.cacheOnDisc(true)

		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		.build();

		loader.init(config);*/
		//------------Set configuration for Image loader-------------------
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return thumbnail.size();
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		if(convertView==null){
			ViewHolder holder=new ViewHolder();
			convertView=inflater.inflate(R.layout.starspeaklistviewlayout, null);
			holder.ivThumbnailImage=(ImageView) convertView.findViewById(R.id.ivThumbnailImage);
			holder.tvTitle=(TextView) convertView.findViewById(R.id.tvTitle);
			holder.tvTitle.setTypeface(tf);

			holder.tvDescription=(TextView) convertView.findViewById(R.id.tvDescription);
			holder.tvDescription.setTypeface(tf);
			convertView.setTag(holder);
		}

		final ViewHolder hold=(ViewHolder) convertView.getTag();

		loader.DisplayImage(thumbnail.get(position), hold.ivThumbnailImage);

		hold.tvDescription.setText(description.get(position));
		hold.tvTitle.setText(title.get(position));

		/*convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				//embedd=1; host=0
				String url=videoLink.get(position);
				int type=videoTypeArrayList.get(position);

				if(type==1){ //embedd
					Intent intent=new Intent(context,YoutubeVideoPlay.class);
					intent.putExtra("youtubeUrl", url);
					intent.putExtra("starName", title.get(position));
					
					context.startActivity(intent);
				}

			}
		});*/

		return convertView;
	}


	static class ViewHolder{
		ImageView ivThumbnailImage;
		TextView tvTitle;
		TextView tvDescription;
	}

}
