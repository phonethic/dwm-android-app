package com.phonethics.sharepref;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SharePref {
	
	Context 			context;
	Editor 				editor;
	String 				userId="",userEmail="",userGender="",userLogin_count="",
						userRegistered="",userLast_login="",userPicture="",
						userFb_link="",userPhone="",userPoints="";
	boolean				videoFullWatched;
	SharedPreferences	user_info_share_pref;
	SharedPreferences	presentTab;
	SharedPreferences	videoWatched;
	
	
	public SharePref(Context context){
		this.context 		= context;
		user_info_share_pref 	=	this.context.getSharedPreferences("User", 0);
		presentTab				=	this.context.getSharedPreferences("presentTag", 0);
		videoWatched			=	this.context.getSharedPreferences("videoWatched", 0);
	}
	
	public boolean isUserLoggedIn(){
		return user_info_share_pref.getBoolean("user_logged_in", false);
	}
	
	public void setUserLogin(boolean isLogin){
		editor = user_info_share_pref.edit();
		editor.putBoolean("user_logged_in", isLogin);
		editor.commit();
	}
	
	public void setUserName(String name){
		editor = user_info_share_pref.edit();
		editor.putString("user_name", name);
		editor.commit();
	}
	
	public String getUserName(){
		return  user_info_share_pref.getString("user_name", "no user");
	}
	
	
	public void setPresentTab(String tab){
		editor = presentTab.edit();
		editor.putString("presentTab", tab);
		editor.commit();
	}
	
	public String getPresentTab(){
		return presentTab.getString("presentTab", "Home");
	}
	
	public boolean isLoginFromFb(){
		return user_info_share_pref.getBoolean("isLoginFromFb", false);
	}
	
	public void setLoginFromFb(boolean isLoginFromFb){
		editor = user_info_share_pref.edit();
		editor.putBoolean("isLoginFromFb", isLoginFromFb);
		editor.commit();
	}
	
	public String getUserToken(){
		return user_info_share_pref.getString("userToken", "0");
	}
	
	public void setHelpImageVisible(boolean isHelp){
		editor = user_info_share_pref.edit();
		editor.putBoolean("isHelp", isHelp);
		editor.commit();
	}
	
	public boolean getHelpImageVisiblity(){
		return user_info_share_pref.getBoolean("isHelp", true);
	}
	public void setUserToken(String token){
		editor = user_info_share_pref.edit();
		editor.putString("userToken", token);
		editor.commit();
	}

	public String getUserId() {
		return user_info_share_pref.getString("userId", "0");
	}

	public void setUserId(String userId) {
		editor = user_info_share_pref.edit();
		editor.putString("userId", userId);
		editor.commit();
	}

	public String getUserEmail() {
		return user_info_share_pref.getString("userEmail", "0");
	}

	public void setUserEmail(String userEmail) {
		editor = user_info_share_pref.edit();
		editor.putString("userEmail", userEmail);
		editor.commit();
	}

	public String getUserGender() {
		return user_info_share_pref.getString("userGender", "Male");
	}

	public void setUserGender(String userGender) {
		editor = user_info_share_pref.edit();
		editor.putString("userGender", userGender);
		editor.commit();
	}

	public String getUserLogin_count() {
		return user_info_share_pref.getString("userLoginCount", "0");
	}

	public void setUserLogin_count(String userLogin_count) {
		editor = user_info_share_pref.edit();
		editor.putString("userLoginCount", userLogin_count);
		editor.commit();
	}

	public String getUserRegistered() {
		return user_info_share_pref.getString("userRegistered", "0");
	}

	public void setUserRegistered(String userRegistered) {
		editor = user_info_share_pref.edit();
		editor.putString("userRegistered", userRegistered);
		editor.commit();
	}

	public String getUserLast_login() {
		return user_info_share_pref.getString("userLastLogin", "0");
	}

	public void setUserLast_login(String userLast_login) {
		editor = user_info_share_pref.edit();
		editor.putString("userLastLogin", userLast_login);
		editor.commit();
	}

	public String getUserPicture() {
		return user_info_share_pref.getString("userPicture", "0");
	}

	public void setUserPicture(String userPicture) {
		editor = user_info_share_pref.edit();
		editor.putString("userPicture", userPicture);
		editor.commit();
	}

	public String getUserFb_link() {
		return user_info_share_pref.getString("userFbLink", "0");
	}

	public void setUserFb_link(String userFb_link) {
		editor = user_info_share_pref.edit();
		editor.putString("userFb_link", userFb_link);
		editor.commit();
	}

	public String getUserPhone() {
		return user_info_share_pref.getString("userPhone", "0");
	}

	public void setUserPhone(String userPhone) {
		editor = user_info_share_pref.edit();
		editor.putString("userPhone", userPhone);
		editor.commit();
	}

	public String getUserPoints() {
		return user_info_share_pref.getString("userPoints", "0");
	}

	public void setUserPoints(String userPoints) {
		editor = user_info_share_pref.edit();
		editor.putString("userPoints", userPoints);
		editor.commit();
	}

	public boolean isVideoFullWatched() {
		return videoWatched.getBoolean("videoWatched", false);
	}

	public void setVideoFullWatched(boolean videoFullWatched) {
		editor = videoWatched.edit();
		editor.putBoolean("videoWatched", videoFullWatched);
		editor.commit();
	}
	
	
	public void setJhalakCheck(boolean jhalakCheck) {
		editor = user_info_share_pref.edit();
		editor.putBoolean("jhalakCheck", jhalakCheck);
		editor.commit();
	}

	public boolean getJhalakCheck(){
		return user_info_share_pref.getBoolean("jhalakCheck", true);
	}
	
	public void setApiResponse(boolean apiResponse) {
		editor = user_info_share_pref.edit();
		editor.putBoolean("apiResponse", apiResponse);
		editor.commit();
	}

	public boolean getApiResponse(){
		return user_info_share_pref.getBoolean("apiResponse", false);
	}
	
	public void setAppUpdate(boolean appUpdate) {
		editor = user_info_share_pref.edit();
		editor.putBoolean("appUpdate", appUpdate);
		editor.commit();
	}

	public boolean getAppUpdate(){
		return user_info_share_pref.getBoolean("appUpdate", true);
	}
	
	public void setNewUserLogin(boolean newUser) {
		editor = user_info_share_pref.edit();
		editor.putBoolean("newUser", newUser);
		editor.commit();
	}

	public boolean getNewUserLogin(){
		return user_info_share_pref.getBoolean("newUser", false);
	}
	
	
	
	
	

}
