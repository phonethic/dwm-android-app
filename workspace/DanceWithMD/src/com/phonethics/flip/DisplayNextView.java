package com.phonethics.flip;

import android.content.Context;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.ViewSwitcher;

public final class DisplayNextView implements Animation.AnimationListener {
	private boolean mCurrentView;
	ImageView image1;
	ImageView image2;
	ViewSwitcher view;
	Context context;
	float x,y;

	public DisplayNextView(boolean currentView, ViewSwitcher view, Context context, float x, float y) {
		mCurrentView = currentView;
		/*this.image1 = image1;
		this.image2 = image2;*/
		this.view = view;
		this.context = context;
		this.x = x;
		this.y = y;
	}

	public void onAnimationStart(Animation animation) {
		view.showNext();
	}

	public void onAnimationEnd(Animation animation) {
		view.post(new SwapViews(mCurrentView, view,context,x,y));
	}

	public void onAnimationRepeat(Animation animation) {
	}
}
