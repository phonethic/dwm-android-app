package com.phonethics.flip;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.ViewSwitcher;

public final class SwapViews implements Runnable {
	private boolean mIsFirstView;
	ImageView image1;
	ImageView image2;
	ViewSwitcher view;
	Context context;
	float centerX;
	float centerY;

	public SwapViews(boolean isFirstView, ViewSwitcher view, Context context, float centerX, float centerY ) {
		mIsFirstView = isFirstView;
		/*this.image1 = image1;
		this.image2 = image2;*/
		this.view = view;
		this.context = context;
		this.centerX = centerX;
		this.centerY = centerY;
	}

	public void run() {

	/*	DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);	

		final float centerX = image1.getWidth() / 2.0f;
		final float centerY = image1.getHeight() / 2.0f;*/
		Flip3dAnimation rotation;

		if (mIsFirstView) {
			/*image1.setVisibility(View.GONE);
			image2.setVisibility(View.VISIBLE);
			image2.requestFocus();*/
			view.showNext();
			rotation = new Flip3dAnimation(-90, 0, centerX, centerY);
		} else {
			/*		image2.setVisibility(View.GONE);
			image1.setVisibility(View.VISIBLE);
			image1.requestFocus();
			 */
			rotation = new Flip3dAnimation(90, 0, centerX, centerY);
		}

		rotation.setDuration(500);
		rotation.setFillAfter(true);
		rotation.setInterpolator(new DecelerateInterpolator());

		if (mIsFirstView) {
			view.startAnimation(rotation);
		}/* else {
			image1.startAnimation(rotation);
		}*/
	}
}