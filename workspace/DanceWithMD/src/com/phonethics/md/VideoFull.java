package com.phonethics.md;

import com.google.android.youtube.player.internal.p;
import com.phonethics.dancewithmadhuri.R;
import com.phonethics.network.NetworkCheck;

import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.VideoView;

public class VideoFull extends Activity {

	VideoView 	vd;
	String		url;
	int			pos;
	Context		context;

	ImageView	pBar;
	Animation	rotate;
	NetworkCheck netCheck;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_video_view);

		context = this;
		netCheck = new NetworkCheck(context);
		vd = (VideoView) findViewById(R.id.vide_full_screen);
		rotate = AnimationUtils.loadAnimation(context, R.anim.rotate);
		pBar		= (ImageView) findViewById(R.id.progressBar2);
		pBar.startAnimation(rotate);
		Bundle bundle = getIntent().getExtras();
		if(bundle!=null){
			url = bundle.getString("url_play");
			pos = bundle.getInt("current_pos");
		}
		if(netCheck.isNetworkAvailable()){
			new StreamVideo().execute();
		}else{
			Toast.makeText(context, "No Internet Connection", 0).show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.video_view, menu);
		return true;
	}

	private class StreamVideo extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressbar
			/* pDialog = new ProgressDialog(getActivity());
            // Set progressbar title
            pDialog.setTitle("Android Video Streaming Tutorial");
            // Set progressbar message
            pDialog.setMessage("Buffering...");
            pDialog.setIndeterminate(false);
            // Show progressbar
            pDialog.show();*/
			pBar.setVisibility(View.VISIBLE);
			pBar.startAnimation(rotate);

		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected void onPostExecute(Void args) {

			try {
				// Start the MediaController

				MediaController mediacontroller = new MediaController(context);
				mediacontroller.setAnchorView(vd);
				// Get the URL from String VideoURL
				Uri video = Uri.parse(url);
				vd.setMediaController(mediacontroller);
				vd.setVideoURI(video);

				vd.requestFocus();
				vd.setOnPreparedListener(new OnPreparedListener() {
					// Close the progress bar and play the video
					public void onPrepared(MediaPlayer mp) {
						//pDialog.dismiss();
						if(pos!=0){
							vd.seekTo(pos);
						}
						vd.start();
						pBar.clearAnimation();
						pBar.setVisibility(View.GONE);

						//scrollView.setVisibility(View.VISIBLE);
					}
				});
				//scrollView.startAnimation(fade_in_anim);

			} catch (Exception e) {
				//pDialog.dismiss();
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}

		}

	}

}
