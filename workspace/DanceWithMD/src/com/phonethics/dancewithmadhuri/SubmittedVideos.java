package com.phonethics.dancewithmadhuri;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyResponse;
import com.android.volley.VolleyResponse.ErrorListener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.flurry.android.FlurryAgent;
import com.localytics.android.LocalyticsSession;
import com.phonethics.adapters.UsersVideoAdapter;

import com.phonethics.customclass.UsersVideo;
import com.phonethics.network.NetworkCheck;

public class SubmittedVideos extends Activity {


	String 					URL_DWM 				= "";
	String					URL_SUBMITTED_VIDEOS 	= "";
	String					URL_LAST 				= "latest";
	String					SONG_ID					= "1";
	String					tittle					= "";
	ArrayList<UsersVideo>	userVideoArr;

	ListView				listUsersVideo;



	ImageView				pBar;
	Animation				rotate;
	TextView				text_tittle;

	NetworkCheck			netCheck;
	Context					context;

	Activity				contextAct;
	Map<String, String> 	flurryEeventParams;

	private static boolean useLocalytics 		= false;
	private static boolean useFlurry 			= false;
	private LocalyticsSession localyticsSession;
	private static  String LOCALYTICS_APP_KEY	="";




	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_downloaded_videos);

		context				= this;
		contextAct			= this;


		useFlurry				= getResources().getBoolean(R.bool.useFlurry);
		useLocalytics			= getResources().getBoolean(R.bool.useLocalytics);

		if(useLocalytics){
			LOCALYTICS_APP_KEY 		= getResources().getString(R.string.LOCALYTICS_APP_KEY);
			this.localyticsSession 	= new LocalyticsSession(this.getApplicationContext(),LOCALYTICS_APP_KEY);
			this.localyticsSession.open();
			//this.localyticsSession.upload();
			//localyticsSession.tagScreen("Splash");
			this.localyticsSession.upload();
		}

		listUsersVideo 		= (ListView) findViewById(R.id.list_submitted_videos);
		text_tittle			= (TextView) findViewById(R.id.text_tittle);
		Button bttn 		= (Button) findViewById(R.id.bttn_menu);
		bttn.setVisibility(View.GONE);
		flurryEeventParams	= new HashMap<String, String>();
		rotate 				= AnimationUtils.loadAnimation(context, R.anim.rotate);
		pBar				= (ImageView) findViewById(R.id.progressBar2);
		pBar.startAnimation(rotate);
		userVideoArr		= new ArrayList<UsersVideo>();

		URL_DWM				= getResources().getString(R.string.url_md_live_server);
		URL_SUBMITTED_VIDEOS = URL_DWM + getResources().getString(R.string.url_submitted_videos);


		netCheck		= new NetworkCheck(context);

		Bundle bundle  	= getIntent().getExtras();
		if(bundle!=null){
			SONG_ID = bundle.getString("songId");
			tittle	= bundle.getString("songName");
		}

		//flurryEeventParams.put("SongID", SONG_ID);
		flurryEeventParams.put("SongName", tittle);
		if(useFlurry){
			FlurryAgent.logEvent(getResources().getString(R.string.fEvent_view_submission),flurryEeventParams);
		}

		if(useLocalytics){
			localyticsSession.tagEvent(getResources().getString(R.string.localEvent_view_submission), flurryEeventParams);
		}
		URL_SUBMITTED_VIDEOS = URL_DWM + getResources().getString(R.string.url_submitted_videos) + SONG_ID + "/" + URL_LAST;
		text_tittle.setText(tittle);
		text_tittle.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/KabelMediumBT.ttf"));

		if(netCheck.isNetworkAvailable()){
			getUsersVideos(URL_SUBMITTED_VIDEOS);
		}else{
			showToast("No Internet Connection!");
		}


		listUsersVideo.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, WatchVideoSubmitted.class);
				intent.putExtra("VIDEO_URL", userVideoArr.get(pos).getMp4Link());
				intent.putExtra("USER_ID", userVideoArr.get(pos).getUserId());
				intent.putExtra("VIDEO_ID", userVideoArr.get(pos).getVideoId());
				intent.putExtra("USER_NAME", userVideoArr.get(pos).getUserName());
				intent.putExtra("SongName", tittle);
				
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.downloaded_videos, menu);
		return false;
	}


	public void getUsersVideos(String url){
		pBar.setVisibility(View.VISIBLE);
		pBar.startAnimation(rotate);

		Log.d("", "MdUsersVideoUrl == "+url);
		RequestQueue 	queue	= Volley.newRequestQueue(contextAct);

		VolleyResponse.Listener<JSONObject> listener = new VolleyResponse.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub
				Log.d("Responce", "MdSongsResponce success "+ response.toString());
				try{
					pBar.clearAnimation();
					pBar.setVisibility(View.GONE);
					String 	success = response.getString("success");
					boolean	isSuccess = response.getBoolean("success");
					if(success.equalsIgnoreCase("true")){
						parseJsonData(response.getJSONArray("data"));
					}else{
						showToast("No videos found.\nPlease upload your video in this dance style to be the first one.");
					}

				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
		};

		VolleyResponse.ErrorListener errorListener = new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				Log.d("Error", "MdSongsResponce error "+ error.toString());
				pBar.clearAnimation();
				pBar.setVisibility(View.GONE);
				showToast("Network Error");

			}
		};

		JsonObjectRequest req = new JsonObjectRequest(Method.GET, url, null, listener, errorListener);
		queue.add(req);

	}



	public void parseJsonData(JSONArray jArr){

		for(int i=0; i<jArr.length();i++){

			try{
				UsersVideo usersVideo 	= new UsersVideo();
				JSONObject jObj 		= jArr.getJSONObject(i);

				usersVideo.setMp4Link(jObj.getString("Mp4_Link"));
				usersVideo.setPicture(jObj.getString("Picture"));
				usersVideo.setSongId(jObj.getString("Song_ID"));
				usersVideo.setUserId(jObj.getString("User_ID"));
				usersVideo.setUserName(jObj.getString("User_Name"));
				usersVideo.setVideoId(jObj.getString("Video_ID"));
				usersVideo.setVotes(jObj.getString("Votes"));

				userVideoArr.add(usersVideo);

			}catch(Exception ex){
				ex.printStackTrace();
			}

			listUsersVideo.setAdapter(new UsersVideoAdapter(contextAct, userVideoArr));
		}
	}


	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		finish();
	}

	public void showToast(String content){
		try{
			Toast.makeText(context, content, 0).show();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}



	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if(useFlurry){
			FlurryAgent.onStartSession(context, getResources().getString(R.string.flurry_id));
			
		}

	}


	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if(useFlurry){
			FlurryAgent.onEndSession(context);
		}


	}




@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(useLocalytics){
			this.localyticsSession.open();
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub

		super.onPause();
		if(useLocalytics){
			this.localyticsSession.close();
			this.localyticsSession.upload();
		}
	}

}
