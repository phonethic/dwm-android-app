package com.phonethics.dancewithmadhuri;

import org.json.JSONObject;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyResponse;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.LoggingBehavior;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.Settings;

import com.phonethics.fragments.RootFragment;
import com.phonethics.sharepref.SharePref;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class LogoutDialog extends Activity {

	private Session.StatusCallback statusCallback = new SessionStatusCallback();
	SharePref	sharePref;
	Activity		context;
	boolean		from_fb=false;
	String		url_logout = "";
	AlertDialog 	alert=null;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		context = this;

		sharePref = new SharePref(context);

		from_fb	  = sharePref.isLoginFromFb();

		Settings.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);

		Session session = Session.getActiveSession();

		if (session == null) {
			if (savedInstanceState != null) {
				session = Session.restoreSession(context, null, statusCallback, savedInstanceState);
			}
			if (session == null) {
				session = new Session(context);
			}
			Session.setActiveSession(session);
			if (session.getState().equals(SessionState.CREATED_TOKEN_LOADED)) {
				session.openForRead(new Session.OpenRequest(this).setCallback(statusCallback));
			}
		}


		url_logout	= getResources().getString(R.string.url_md_live_server)+getResources().getString(R.string.url_user_logout);

		logoutAlert();
	}



	public void logoutAlert(){
		//getSlidingMenu().showContent(true);
		AlertDialog.Builder  alertDialog = new AlertDialog.Builder(context);
		//alertDialog.setIcon(R.drawable.ic_launcher);
		alertDialog.setTitle("Dance With Madhuri");
		alertDialog.setMessage("Are you sure, You want to Log out ?");
		alertDialog.setCancelable(true);
		alertDialog.setPositiveButton("Log out", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				alert.dismiss();
				if(from_fb){
					//backToLogin = true;
					logoutFromFb();
					//loginBack();
				}else{
					logoutRequest();
				}
				//onBackPressed();
			}
		});
		alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				alert.dismiss();
			}

		});

		alert = alertDialog.create();
		alert.show();
	}


	private class SessionStatusCallback implements Session.StatusCallback {
		@Override
		public void call(Session session, SessionState state, Exception exception) {
			//updateView();
			loginBack();
		}
	}


	public void logoutRequest(){
		String 	email	= sharePref.getUserEmail();
		String	token	= sharePref.getUserToken();
		JSONObject jObj = new JSONObject();
		try{

			jObj.put("uname", email);
			jObj.put("token", token);
		}catch(Exception ex){
			ex.printStackTrace();
		}


		RequestQueue queue = Volley.newRequestQueue(context);
		JsonObjectRequest req = new JsonObjectRequest(Method.POST, url_logout, jObj, new VolleyResponse.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub
				//pBar.setVisibility(View.INVISIBLE);

				Log.d("Responce", "MDLogout responce "+response.toString());

				try{
					String success = response.getString("success");
					if(success.equalsIgnoreCase("true")){
						Toast.makeText(context, "You have successfully logged out.", 0).show();

					}else if(response.getString("message").equalsIgnoreCase("Already logout")){
						Toast.makeText(context, "You are already logged out.", 0).show();

					}else{
						Toast.makeText(context, response.getString("message"), 0).show();
					}	
					loginBack();
				}catch(Exception ex){

				}
			}
		}, new VolleyResponse.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				//pBar.setVisibility(View.INVISIBLE);
				Log.d("Error", "MDLogout error "+error.toString());
			}
		});
		//pBar.setVisibility(View.VISIBLE);
		queue.add(req);
	}
	
	public void logoutFromFb(){
		try{
			Session.openActiveSession(this, false, new Session.StatusCallback() {


				// callback when session changes state
				@Override
				public void call(Session session, SessionState state, Exception exception) {

					if(session.isOpened()){	
						session.closeAndClearTokenInformation();
						//exception.printStackTrace();
						//Intent intent = new Intent(getActivity(), LoginScreen.class);
						//sharePref.setUserLogin(false);
						//startActivity(intent);
						//getActivity().finish();
						//backToLogin  = true;
					}
					Log.i("Clearing", "Token");
					
				}
			});
		}catch(Exception ex){
			Log.i("Log out error", "Log out error");
			ex.printStackTrace();
		}
	}


	@Override
	public void onStart() {
		super.onStart();
		if(statusCallback!=null){
			Session.getActiveSession().addCallback(statusCallback);
		}
	}



	@Override
	public void onStop() {
		super.onStop();
		Session.getActiveSession().removeCallback(statusCallback);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Session.getActiveSession().onActivityResult(context, requestCode, resultCode, data);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		Session session = Session.getActiveSession();
		Session.saveSession(session, outState);
	}

	public void loginBack(){
		sharePref.setLoginFromFb(false);
		sharePref.setUserLogin(false);
		sharePref.setUserName("");

		
		
		Intent intent = new Intent(context, LoginScreen.class);
		startActivity(intent);
		finish();
		
		
		/*sharePref.setLoginFromFb(false);
		sharePref.setUserLogin(false);
		sharePref.setUserName("");
		
		RootFragment fr = new RootFragment();
		fr.loginBack();
		finish();*/
		
	}
}
