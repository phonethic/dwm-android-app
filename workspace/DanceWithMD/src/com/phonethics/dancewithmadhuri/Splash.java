	package com.phonethics.dancewithmadhuri;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyResponse;
import com.android.volley.VolleyResponse.ErrorListener;
import com.android.volley.VolleyResponse.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.flurry.android.FlurryAgent;
import com.localytics.android.LocalyticsSession;

import com.phonethics.network.NetworkCheck;
import com.phonethics.sharepref.SharePref;

public class Splash extends Activity {


	Activity context;
	VideoView vd;
	RelativeLayout layout;
	NetworkCheck	netCheck;
	String			url_jhalak_check="",url_valid_api="",server_url="";
	SharePref		sharePref;

	private LocalyticsSession localyticsSession;
	private static  String LOCALYTICS_APP_KEY=""; 

	private static boolean useLocalytics 	= false;
	private static boolean useFlurry 		= false;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		context 				= this;
		useFlurry				= getResources().getBoolean(R.bool.useFlurry);
		useLocalytics			= getResources().getBoolean(R.bool.useLocalytics);
		LOCALYTICS_APP_KEY 		= getResources().getString(R.string.LOCALYTICS_APP_KEY);
		this.localyticsSession 	= new LocalyticsSession(this.getApplicationContext(),LOCALYTICS_APP_KEY);
		if(useLocalytics){

			this.localyticsSession.open();
			//this.localyticsSession.upload();
			localyticsSession.tagScreen("Splash");
			this.localyticsSession.upload();
		}

		sharePref 			= new SharePref(context);
		netCheck			= new NetworkCheck(context);
		vd 					= (VideoView) findViewById(R.id.vide_full_screen);
		layout 				= (RelativeLayout) findViewById(R.id.layout_video);
		server_url			= getResources().getString(R.string.url_md_live_server);
		url_jhalak_check	= server_url+getResources().getString(R.string.url_jhalak_check);
		url_valid_api		= server_url+getResources().getString(R.string.url_valid_api);
		sharePref.setApiResponse(false);
		/*if(netCheck.isNetworkAvailable()){
			checkApi(url_valid_api);
		}*/
		layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				vd.stopPlayback();

				Intent mainactivity = new Intent(context,  LoginScreen.class);
				startActivity(mainactivity);
				finish();
				overridePendingTransition(R.anim.activity_push_up_in, 0);
			}
		});

		showVideo();


	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.splash, menu);
		return true;
	}


	public void showVideo(){
		Uri uri = Uri.parse("android.resource://"+ getPackageName() +"/" +R.raw.test1);
		vd.setVideoURI(uri);
		vd.start();
		vd.setOnPreparedListener(new OnPreparedListener() {

			@Override
			public void onPrepared(MediaPlayer mp) {
				// TODO Auto-generated method stub
				Log.d("", "MDVideoPrepared");
				if(netCheck.isNetworkAvailable()){
					checkApi(url_valid_api);
				}

			}
		});
		vd.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				Intent mainactivity = new Intent(context,  LoginScreen.class);
				startActivity(mainactivity);
				finish();
				overridePendingTransition(R.anim.activity_push_up_in, 0);
			}
		});

		vd.setOnErrorListener(new OnErrorListener() {

			@Override
			public boolean onError(MediaPlayer mp, int what, int extra) {
				// TODO Auto-generated method stub
				Log.d("", "MDVideoError");
				Intent mainactivity = new Intent(context,  LoginScreen.class);
				startActivity(mainactivity);
				finish();
				overridePendingTransition(R.anim.activity_push_up_in, 0);
				return true;
			}
		});
	}



	public void checkJhalak(String url){
		boolean jhalakValid = false;
		Log.d("", "MdCheckJhalak Url >> "+url.toString());
		try{
			RequestQueue queue = Volley.newRequestQueue(context);
			VolleyResponse.Listener<JSONObject> listener = new Listener<JSONObject>() {

				@Override
				public void onResponse(JSONObject response) {
					// TODO Auto-generated method stub
					try{

						Log.d("", "MdCheckJhalak Response >> "+response.toString());
						if(response.getString("success").equalsIgnoreCase("true")){
							sharePref.setJhalakCheck(true);
							sharePref.setApiResponse(true);
						}else{
							sharePref.setJhalakCheck(false);
							sharePref.setApiResponse(false);
						}
					}

					catch(Exception ex){
						ex.printStackTrace();
					}
				}
			};

			VolleyResponse.ErrorListener errorListener = new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {
					// TODO Auto-generated method stub
					try{
						Log.d("", "MdCheckJhalak error >> "+error.toString());
						sharePref.setApiResponse(false);
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}

				}
			};

			JsonObjectRequest request = new JsonObjectRequest(url, null, listener, errorListener);
			queue.add(request);
		}catch(OutOfMemoryError oom){
			oom.printStackTrace();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}


	}

	public void checkApi(String url){
		Log.d("", "MdCheckApi Url >> "+url.toString());
		try{
			RequestQueue queue = Volley.newRequestQueue(context);


			VolleyResponse.Listener<JSONObject> listener = new Listener<JSONObject>() {

				@Override
				public void onResponse(JSONObject response) {
					// TODO Auto-generated method stub
					try{
						Log.d("", "MdCheckApi >> "+response.toString());
						if(response.getString("success").equalsIgnoreCase("true")){
							sharePref.setApiResponse(true);
							sharePref.setAppUpdate(true);
							checkJhalak(url_jhalak_check);

						}else{
							sharePref.setApiResponse(true);
							sharePref.setAppUpdate(false);
						}

					}catch(Exception ex){
						ex.printStackTrace();
					}
				}
			};

			VolleyResponse.ErrorListener errorListener = new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {
					// TODO Auto-generated method stub
					try{
						Log.d("", "MdCheckApiError >> "+error.toString());
						sharePref.setApiResponse(false);
					}catch(Exception ex){
						ex.printStackTrace();
					}
				}
			};

			JsonObjectRequest request = new JsonObjectRequest(url, null, listener, errorListener);
			queue.add(request);
		}catch(OutOfMemoryError oom){
			oom.printStackTrace();
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}



	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if(useFlurry){
			FlurryAgent.onStartSession(this, getResources().getString(R.string.flurry_id));
			FlurryAgent.logEvent(getResources().getString(R.string.fEvent_app_started));
		}

	}


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if(useFlurry){
			FlurryAgent.onEndSession(this);
		}


	}




	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(useLocalytics){
			this.localyticsSession.open();
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub

		super.onPause();
		if(useLocalytics){
			this.localyticsSession.close();
			this.localyticsSession.upload();
		}
	}






}
