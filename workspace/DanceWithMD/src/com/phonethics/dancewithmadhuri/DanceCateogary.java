package com.phonethics.dancewithmadhuri;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyResponse;
import com.android.volley.VolleyResponse.ErrorListener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.flurry.android.FlurryAgent;
import com.localytics.android.LocalyticsSession;
import com.phonethics.adapters.LearnDanceAdapter;
import com.phonethics.customclass.SongInfo;

import com.phonethics.network.NetworkCheck;
import com.phonethics.sharepref.SharePref;

public class DanceCateogary extends Activity {

	String						url_dwm;
	String						category_id = "1";
	String						url_songs;
	String						style_name = "";
	ArrayList<SongInfo> 		songInfoArr;
	SongInfo					songInfo;
	RequestQueue 				queue;

	ImageView	pBar;
	Animation	rotate;
	ListView					listView;
	Activity					context;
	TextView					textView_tittle;
	SharePref					sharePref;
	NetworkCheck				netCheck;
	Map<String, String> 		flurryEeventParams;
	boolean						onStartCall = true;

	private static boolean useLocalytics 	= false;
	private static boolean useFlurry 		= false;
	private LocalyticsSession localyticsSession;
	private static  String LOCALYTICS_APP_KEY	=""; 


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dance_cateogary);

		context					= this;
		useFlurry				= getResources().getBoolean(R.bool.useFlurry);
		useLocalytics			= getResources().getBoolean(R.bool.useLocalytics);
		this.localyticsSession 	= new LocalyticsSession(this.getApplicationContext(),LOCALYTICS_APP_KEY);
		if(useLocalytics){
			LOCALYTICS_APP_KEY 		= getResources().getString(R.string.LOCALYTICS_APP_KEY);

			this.localyticsSession.open();
			localyticsSession.tagScreen("DanceStyle_Lessons");
			this.localyticsSession.upload();
		}
		flurryEeventParams	= new HashMap<String, String>();
		netCheck		= new NetworkCheck(context);
		url_dwm 		= getResources().getString(R.string.url_md_live_server);
		url_songs		= url_dwm + getResources().getString(R.string.url_song_info);
		songInfoArr		= new ArrayList<SongInfo>();
		sharePref		= new SharePref(context);
		queue			= Volley.newRequestQueue(context);
		rotate 			= AnimationUtils.loadAnimation(context, R.anim.rotate);
		pBar			= (ImageView) findViewById(R.id.progressBar2);
		listView		= (ListView) findViewById(R.id.list_category);
		listView.setCacheColorHint(0);
		textView_tittle	= (TextView) findViewById(R.id.text_tittle);

		Bundle	bundle = getIntent().getExtras();
		if(bundle!=null){
			category_id = bundle.getString("category_id");
			style_name =  bundle.getString("style_name");


		}



		//flurryEeventParams.put("StyleID", category_id);
		flurryEeventParams.put("DanceStyle", style_name);
		if(useLocalytics){
			localyticsSession.tagEvent(getResources().getString(R.string.localEvent_dance_style), flurryEeventParams);
		}
		
		textView_tittle.setText(style_name);
		textView_tittle.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/KabelMediumBT.ttf"));

		/*if(netCheck.isNetworkAvailable()){
			getSongs(url_songs+category_id);
		}else{
			showToast("No Internet Connection");
		}*/

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				if((netCheck.isNetworkAvailable())&&(songInfoArr!=null)){
					Intent intent = new Intent(context, SongClips.class);
					intent.putExtra("user_id",sharePref.getUserId());
					intent.putExtra("song_id", songInfoArr.get(pos).getSong_id());
					//String songName = songInfoArr.get(pos).getCategory_name()+" - "+songInfoArr.get(pos).getTitle();
					intent.putExtra("song_name", songInfoArr.get(pos).getTitle());
					intent.putExtra("dance_style", songInfoArr.get(pos).getCategory_name());
					intent.putExtra("lock_type", songInfoArr.get(pos).getLock_type());
					intent.putExtra("fromJhalak", false);
					startActivity(intent);
					//startActivity(new Intent(context, SongClips.class));
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}else{
					showToast("No Internet Connection");
				}


			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.dance_cateogary, menu);
		return false;
	}


	public void getSongs(String url){
		pBar.setVisibility(View.VISIBLE);
		pBar.startAnimation(rotate);
		Log.d("", "MdUrl "+url);


		VolleyResponse.Listener<JSONObject> listener = new VolleyResponse.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub
				Log.d("Responce", "MdSongsResponce success "+ response.toString());
				try{
					String success = response.getString("success");
					if(success.equalsIgnoreCase("true")){
						JSONArray jArr = response.getJSONArray("data");
						ParseJsonData(jArr);
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}

		};


		VolleyResponse.ErrorListener errorListener = new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				Log.d("Error", "MdSongsResponce error "+ error.toString());
				pBar.clearAnimation();
				pBar.setVisibility(View.GONE);
				showToast("Network Error!");

			}
		};

		JsonObjectRequest req = new JsonObjectRequest(Method.GET, url, null, listener, errorListener);
		queue.add(req);

	}



	public void ParseJsonData(JSONArray jsonArr){
		pBar.setVisibility(View.VISIBLE);
		pBar.startAnimation(rotate);
		try{
			for(int i=0;i<jsonArr.length();i++){
				songInfo = new SongInfo();

				JSONObject tempObj = jsonArr.getJSONObject(i);
				String id 		=	tempObj.getString("Song_ID");
				String title	=	tempObj.getString("Title");
				String movie	=	tempObj.getString("Movie");
				String choreographer =	tempObj.getString("Choreographer");
				String thumb		=	tempObj.getString("Thumbnail");
				String locktype		=	tempObj.getString("Lock_Type");
				String category		=	tempObj.getString("Category");
				String category_name=	tempObj.getString("Category_Name");
				String is_video 	=	tempObj.getString("Is_Video");
				String video_link 	=	tempObj.getString("Video_Link");

				songInfo.setSong_id(id);
				songInfo.setTitle(title);
				songInfo.setMovie(movie);
				songInfo.setChoreographer(choreographer);
				songInfo.setThumbnail(thumb);
				songInfo.setLock_type(locktype);
				songInfo.setCategory(category);
				songInfo.setCategory_name(category_name);
				songInfo.setIs_video(is_video);
				songInfo.setVideo_link(video_link);

				songInfoArr.add(songInfo);

			}

			listView.setAdapter(new LearnDanceAdapter(context, songInfoArr,"Song"));
			pBar.clearAnimation();
			pBar.setVisibility(View.GONE);

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}



	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		finish();
	}

	public void showToast(String content){
		Toast.makeText(context, content, 0).show();
	}


	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if(useFlurry){
			FlurryAgent.onStartSession(context, getResources().getString(R.string.flurry_id));
			//FlurryAgent.logEvent(getResources().getString(R.string.localEvent_tab_learn_dance));
			FlurryAgent.logEvent(getResources().getString(R.string.fEvent_dance_style),flurryEeventParams);
		}
		if(onStartCall){
			onStartCall = false;
			if(netCheck.isNetworkAvailable()){
				getSongs(url_songs+category_id);
			}else{
				showToast("No Internet Connection");
			}
		}

	}


	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if(useFlurry){
			FlurryAgent.onEndSession(context);
		}


	}


	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(useLocalytics){
			this.localyticsSession.open();
			localyticsSession.tagScreen("DanceStyle_Lessons");
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub

		super.onPause();
		if(useLocalytics){
			this.localyticsSession.close();
			this.localyticsSession.upload();
		}
	}

}
