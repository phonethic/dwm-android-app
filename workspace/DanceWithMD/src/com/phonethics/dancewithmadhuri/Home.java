package com.phonethics.dancewithmadhuri;


import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.phonethics.dancewithmadhuri.R;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Home extends BaseActivity{

	
	Button	bttn;
	
	public Home(){
		super(R.string.app_name);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home_land);
		
		bttn = (Button) findViewById(R.id.bttn_menu);
		bttn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SlidingMenu sm = getSlidingMenu();
				sm.showMenu(true);
			}
		});
	}
	
	
}
