package com.phonethics.dancewithmadhuri;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager.BadTokenException;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyResponse;
import com.android.volley.VolleyResponse.ErrorListener;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.flurry.android.FlurryAgent;
import com.localytics.android.LocalyticsSession;
import com.phonethics.imageloader.ImageLoader;
import com.phonethics.network.NetworkCheck;
import com.phonethics.sharepref.SharePref;

public class WatchVideo extends Activity {

	VideoView 		vd;
	String			url,song_id;
	int				pos = 0;
	Context			context;
	ImageView	pBar;
	Animation	rotate;
	String			url_update_count = "http://dancewithmadhuri.com/point.php?uid=",song_name="";
	int				count;
	SharePref		sharePref;
	String			user_id,sharePic,clipId,songId,shareText,dance_style,clip_title;
	NetworkCheck	netCheck;

	private static final List<String> PERMISSIONS = Arrays.asList("publish_actions");
	private static final String PENDING_PUBLISH_KEY = "pendingPublishReauthorization";
	private boolean pendingPublishReauthorization = false;
	static  String	TOAST_FB_REPLY="";
	static	String	FB_DESCRIPTION="";
	Activity		contextAct;
	private static boolean useLocalytics 		= false;
	private static boolean useFlurry 			= false;
	private LocalyticsSession localyticsSession;
	private static  String LOCALYTICS_APP_KEY	=""; 

	Map<String, String> 		flurryEeventParams;




	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_video_view);
		if(savedInstanceState!=null){
			pos = savedInstanceState.getInt("pos");
			context = this;
		}else{
			context = this;
		}


		contextAct = this;
		netCheck = new NetworkCheck(context);
		useFlurry				= getResources().getBoolean(R.bool.useFlurry);
		useLocalytics			= getResources().getBoolean(R.bool.useLocalytics);

		if(useLocalytics){
			LOCALYTICS_APP_KEY 		= getResources().getString(R.string.LOCALYTICS_APP_KEY);
			this.localyticsSession 	= new LocalyticsSession(this.getApplicationContext(),LOCALYTICS_APP_KEY);
			this.localyticsSession.open();
			//this.localyticsSession.upload();
			//localyticsSession.tagScreen("Splash");
			this.localyticsSession.upload();
		}
		sharePref = new SharePref(context);
		vd = (VideoView) findViewById(R.id.vide_full_screen);
		rotate 		= AnimationUtils.loadAnimation(context, R.anim.rotate);
		pBar		= (ImageView) findViewById(R.id.progressBar2);
		flurryEeventParams		= new HashMap<String, String>();
		pBar.startAnimation(rotate);
		TOAST_FB_REPLY = getResources().getString(R.string.toast_fb_reply);
		FB_DESCRIPTION = getResources().getString(R.string.fb_description);
		Bundle bundle = getIntent().getExtras();
		if(bundle!=null){
			url 		= bundle.getString("url_play");
			song_id 	= bundle.getString("song_id");
			count		= bundle.getInt("position");
			url 		= url.replaceAll(" ", "%20");
			sharePic	= bundle.getString("sharePic");
			clipId		= bundle.getString("clip_id");
			shareText	= bundle.getString("shareText");
			song_name	= bundle.getString("song_name");
			dance_style = bundle.getString("dance_style");
			clip_title  = bundle.getString("clip_title");

			Log.d("Url encode", "MdVideoUrl == "+ url);

		}
		user_id = sharePref.getUserId();
		count = count+1;
		url_update_count = url_update_count	+ user_id +	"&sid="	+ song_id +	"&cid="	+ count;


		flurryEeventParams.put("DanceStyle", dance_style);
		flurryEeventParams.put("SongName", dance_style+" - "+song_name);
		flurryEeventParams.put("clip_title", dance_style+" - "+song_name +" - "+clip_title);

		FlurryAgent.logEvent("View_Clips", flurryEeventParams);
		if(useLocalytics){
			localyticsSession.tagEvent("View_Clips",flurryEeventParams);
		}
		if(useFlurry){
			FlurryAgent.logEvent("View_Clips", flurryEeventParams);
		}

		if(netCheck.isNetworkAvailable()){
			new StreamVideo().execute();
		}else{
			Toast.makeText(context, "No Internet Connection", 0).show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.video_view, menu);
		return false;
	}

	private class StreamVideo extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressbar
			/* pDialog = new ProgressDialog(getActivity());
            // Set progressbar title
            pDialog.setTitle("Android Video Streaming Tutorial");
            // Set progressbar message
            pDialog.setMessage("Buffering...");
            pDialog.setIndeterminate(false);
            // Show progressbar
            pDialog.show();*/
			pBar.startAnimation(rotate);
			pBar.setVisibility(View.VISIBLE);

		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected void onPostExecute(Void args) {

			try {
				// Start the MediaController

				MediaController mediacontroller = new MediaController(context);
				mediacontroller.setAnchorView(vd);
				// Get the URL from String VideoURL
				Uri video = Uri.parse(url);
				vd.setMediaController(mediacontroller);
				vd.setVideoURI(video);

				vd.requestFocus();

				vd.setOnPreparedListener(new OnPreparedListener() {
					// Close the progress bar and play the video
					public void onPrepared(MediaPlayer mp) {
						//pDialog.dismiss();
						if(pos!=0){
							vd.seekTo(pos);
						}
						mp.setOnBufferingUpdateListener(new OnBufferingUpdateListener() {

							@Override
							public void onBufferingUpdate(MediaPlayer mp, int percent) {
								// TODO Auto-generated method stub

							}
						});
						vd.start();		
						pBar.clearAnimation();
						pBar.setVisibility(View.GONE);


					}
				});
				//

				vd.setOnCompletionListener(new OnCompletionListener() {

					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						//Toast.makeText(context, "Video Complete", 0).show();
						showAlert();
						//updateCounter(url_update_count);

					}
				});
				vd.setOnErrorListener(new OnErrorListener() {

					@Override
					public boolean onError(MediaPlayer mp, int what, int extra) {
						// TODO Auto-generated method stub
						Toast.makeText(context, "Some Error Occured", 0).show();
						return true;
					}
				});






			} catch (Exception e) {
				//pDialog.dismiss();
				Log.e("Error",""+ e.toString());
				e.printStackTrace();
			}

		}

	}

	public void updateCounter(String url){
		pBar.setVisibility(View.VISIBLE);
		pBar.startAnimation(rotate);
		Log.d("Url", "MdUpdate url "+url);
		//Toast.makeText(context, "Please Wait..", Toast.LENGTH_LONG).show();

		RequestQueue 				queue;
		queue					= Volley.newRequestQueue(context);
		VolleyResponse.Listener<String> listener = new VolleyResponse.Listener<String>() {

			@Override
			public void onResponse(String response) {
				// TODO Auto-generated method stub
				Log.d("", "MdUpdateResponce success "+response);
				if(response.equalsIgnoreCase("1")){
					pBar.clearAnimation();
					pBar.setVisibility(View.GONE);
					//Toast.makeText(context, "success", Toast.LENGTH_LONG).show();

					Handler handler = new Handler();
					Runnable runnable = new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							sharePref.setVideoFullWatched(true);
							onBackPressed();
							finish();
						}
					};
					handler.postDelayed(runnable, 500);

				}else{

					onBackPressed();
					finish();
				}
			}
		};
		VolleyResponse.ErrorListener errorListener = new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				Log.d("", "MdUpdateResponce error "+error.toString());
				pBar.clearAnimation();
				pBar.setVisibility(View.GONE);
			}
		};

		StringRequest req = new StringRequest(Method.GET, url, listener, errorListener);
		queue.add(req);
	}



	public void showAlert(){


		AlertDialog.Builder  alertDialog = new AlertDialog.Builder(context);
		View view =  getLayoutInflater().inflate(R.layout.dialog_view, null);
		ImageView imgview = (ImageView) view.findViewById(R.id.img_badge);
		ImageLoader	loader = new ImageLoader(context);
		TextView  text	= (TextView) view.findViewById(R.id.text_badge);
		text.setText(shareText);
		text.setTextColor(Color.WHITE);
		loader.DisplayImage("http://dancewithmadhuri.com/images/"+sharePic, imgview);
		alertDialog.setView(view);
		alertDialog.setTitle("Dance With Madhuri");
		alertDialog.setCancelable(true);
		alertDialog.setPositiveButton("Share", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				//updateCounter(url_update_count);
				/*Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
				sharingIntent.setType("text/plain");
				sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Dance With Madhuri");
				//sharingIntent.putExtra(android.content.Intent.EXTRA_STREAM, "http://dancewithmadhuri.com/images/"+sharePic);
				sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareText);
				startActivity(Intent.createChooser(sharingIntent, "Share via"));*/


				publishStory(shareText, "http://dancewithmadhuri.com/images/"+sharePic);

			}
		});
		alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				updateCounter(url_update_count);
			}
		});
		AlertDialog alert = alertDialog.create();
		alert.show();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if(useLocalytics){
			this.localyticsSession.close();
			this.localyticsSession.upload();
		}
		try{
			pos = vd.getCurrentPosition();
			Log.d("Puase", "MdActivityPause >> "+pos);
			vd.pause();
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();

		try{
			Log.d("Puase", "MdActivityRestart >> "+pos);
			vd.seekTo(pos);
			vd.resume();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	private void publishStory(String caption,String pictureUrl) {

		try{
			Session session = Session.getActiveSession();

			if(session.isClosed()){
				//showToast("closed");
				login_facebook();
			}else{

			}
			if(session.isOpened()){
				//showToast("open");
			}
			if (session != null && (session.isOpened())) {
				showToast("Please Wait..");
				// Check for publish permissions
				pBar.startAnimation(rotate);
				pBar.setVisibility(View.VISIBLE);
				List<String> permissions = session.getPermissions();
				if (!isSubsetOf(PERMISSIONS, permissions)) {
					pendingPublishReauthorization = true;
					try{
						Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(this, PERMISSIONS);
						session.requestNewPublishPermissions(newPermissionsRequest);
					}catch(Exception ex){
						ex.printStackTrace();
					}
					return;
				}

				Bundle postParams = new Bundle();
				postParams.putString("name", "Dance with Madhuri");
				postParams.putString("caption", caption);
				postParams.putString("description", FB_DESCRIPTION);
				//postParams.putString("link", "https://developers.facebook.com/android");
				postParams.putString("picture", pictureUrl);

				Request.Callback callback= new Request.Callback() {
					public void onCompleted(Response response) {
						try {
							JSONObject graphResponse = null;
							try{
								graphResponse = response.getGraphObject().getInnerJSONObject();
							}catch(NullPointerException nexp){
								nexp.printStackTrace();
							}catch(Exception ex){
								ex.printStackTrace();
							}

							if(graphResponse==null){
								login_facebook();
							}else{
								String postId = null;
								try{
									postId = graphResponse.getString("id");
								}catch(Exception ex){
									ex.printStackTrace();
								}
								FacebookRequestError error = response.getError();
								if (error != null) {
									Toast.makeText(context.getApplicationContext(),error.getErrorMessage(),Toast.LENGTH_SHORT).show();
									pBar.clearAnimation();
									pBar.setVisibility(View.GONE);
								} else {
									Toast.makeText(context.getApplicationContext(),TOAST_FB_REPLY,Toast.LENGTH_LONG).show();
									pBar.clearAnimation();
									pBar.setVisibility(View.GONE);
									updateCounter(url_update_count);
								}
							}
						} catch (Exception e) {
							Log.i("errorFB","JSON error "+ e.getMessage());
						}
					}
				};

				Request request = new Request(session, "me/feed", postParams,HttpMethod.POST, callback);

				RequestAsyncTask task = new RequestAsyncTask(request);
				task.execute();
			}else{
				//showToast("Session null");
				login_facebook();
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}


	private boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
		for (String string : subset) {
			if (!superset.contains(string)) {
				return false;
			}
		}
		return true;
	}


	public void showToast(String content){
		try{
			Toast.makeText(context, content, 0).show();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		//uiHelper.onActivityResult(requestCode, resultCode, data);
		try
		{
			Log.d("", "MdFbLogin requestCode >> "+requestCode);
			Log.d("", "MdFbLogin resultCode >> "+resultCode);
			if(resultCode==0){
				pBar.clearAnimation();
				pBar.setVisibility(View.GONE);
			}
			Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
		}catch(IllegalStateException ilgx)
		{
			ilgx.printStackTrace();
		}
		catch (NullPointerException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	public void login_facebook()
	{

		try
		{
			pBar.setVisibility(View.VISIBLE);
			pBar.startAnimation(rotate);
			Session.openActiveSession(contextAct, true, new Session.StatusCallback() {

				@Override
				public void call(final Session session, SessionState state, Exception exception) {
					// TODO Auto-generated method stub
					if(session.isOpened())
					{

						//Toast.makeText(context, "Please Wait", 1).show();
						List<String> permissions = session.getPermissions();
						if (!isSubsetOf(PERMISSIONS, permissions)) {
							pendingPublishReauthorization = true;
							Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(contextAct, PERMISSIONS);
							session.requestNewPublishPermissions(newPermissionsRequest);
						}

						Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {

							@Override
							public void onCompleted(GraphUser user, Response response) {
								// TODO Auto-generated method stub
								if(user!=null)
								{

									String fbUserid			= user.getId();
									String fbAccessToken	=  session.getAccessToken();
									Log.i("User Name ", "Name : "+user.getName());
									Log.i("User Id ", "Id : "+user.getId());
									Log.i("User Access Token ", "Access Token : "+session.getAccessToken());
									/*if(sharePref.isLoginFromFb()){
										if(user.getId().equalsIgnoreCase(sharePref.getUserId())){
											publishStory(shareText, "http://dancewithmadhuri.com/images/"+sharePic);		
										}else{
											showToast("You are Logged in as different user");
										}
									}else{

									}*/
									publishStory(shareText, "http://dancewithmadhuri.com/images/"+sharePic);



								}

								else{

									Log.i("ELSE", "ELSE");
								}
							}


						});
						/*session.getAccessToken();
					Toast.makeText(context, "Session AccessToken : "+session.getAccessToken(), Toast.LENGTH_LONG).show();*/
					}else{

					}

					if(session.isClosed()){
						pBar.clearAnimation();
						pBar.setVisibility(View.GONE);
						//Toast.makeText(context, "Closed", 0).show();
					}

				}


			});}catch(NullPointerException npx)
			{
				npx.printStackTrace();
			}
		catch(BadTokenException bdx)
		{
			bdx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}


	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (vd.isPlaying()){ 
			outState.putInt("pos", vd.getCurrentPosition());
			vd.pause();

		}else{
			outState.putInt("pos", 0);
		}
		context = this;


	}




	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(savedInstanceState);
		if(savedInstanceState != null){
			context = this;
			pos = savedInstanceState.getInt("pos");
		}

	}



	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(useLocalytics){
			this.localyticsSession.open();
			localyticsSession.tagScreen("Watch_Video");

		}
	}




	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if(useFlurry){
			FlurryAgent.onStartSession(context, getResources().getString(R.string.flurry_id));

		}
	}


	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if(useFlurry){
			FlurryAgent.onEndSession(context);
		}


	}

}
