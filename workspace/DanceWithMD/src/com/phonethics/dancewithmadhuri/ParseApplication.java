package com.phonethics.dancewithmadhuri;



import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseUser;
import com.parse.PushService;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.util.Log;

public class ParseApplication extends Application {

	Context context;
	@Override
	public void onCreate() {
		super.onCreate();

		// Add your initialization code here
		/*Parse.initialize(this, "xVf594RDAOScmU75UHoRXP07fpXqXhsl34rrB5ek", "cFjC1Hq1aGuzrzdzo2FZaVCc5NIdw4SpSbJyzMev");
*/
		context = this;
	/*	ParseObject testObject = new ParseObject("Tab4");
		testObject.put("hi", "bye");
		testObject.saveInBackground();
*/
		
		/*
		 * parse
		 */
		try
		{
			ActivityManager activityManager = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);

			String packageName = activityManager.getRunningTasks(1).get(0).topActivity.getPackageName();

			String class_running = activityManager.getRunningTasks(1).get(0).topActivity.getClassName();

			//Parse.initialize(this, "mPCFgY9KPzYeJtjYSzGYLlXXYPb7R8APoCqrhq3V", "h9MAqCZgRzI9mCcuUtzMwNr30gIJ3YeAqDtNX18o"); // testing app id
			Parse.initialize(this, "tWsjACyCVqMFJiJmL1PNfHloOk3sGEKMb10Tvkuj", "T7rQdUuoPf5fh7VbMg2J7eR7WFFAxoOMkhXmlf2o"); // main app id
			
			
			PushService.subscribe(context,"", LoginScreen.class);
			PushService.setDefaultPushCallback(this, NotificationCallBack.class);


		}catch(Exception ex)
		{ 
			ex.printStackTrace();
		}



		ParseUser.enableAutomaticUser();
		ParseACL defaultACL = new ParseACL();

		// If you would like all objects to be private by default, remove this line.
		defaultACL.setPublicReadAccess(true);

		ParseACL.setDefaultACL(defaultACL, true);
	}



	/*public  void setClassForClick(String className){
		Log.d("!!!!!!!!!!!!!!!!", "INSIDE PARSE APP CLASS");
		try{


			if(className.equalsIgnoreCase("tools")){
				PushService.setDefaultPushCallback(context, ToolsTab.class);
			}else if(className.equalsIgnoreCase("gallery")){
				PushService.setDefaultPushCallback(context, NissanCarGallery.class);
			}else if(className.equalsIgnoreCase("remeinders") || className.equalsIgnoreCase("remainders") ){
				PushService.setDefaultPushCallback(context, Remeinders.class);
			}
		}catch(NullPointerException exp){
			exp.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/


}

