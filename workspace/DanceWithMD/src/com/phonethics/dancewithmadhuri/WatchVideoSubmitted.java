package com.phonethics.dancewithmadhuri;


import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyResponse;
import com.android.volley.Request.Method;
import com.android.volley.VolleyResponse.ErrorListener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.flurry.android.FlurryAgent;
import com.localytics.android.LocalyticsSession;
import com.phonethics.imageloader.ImageLoader;
import com.phonethics.network.NetworkCheck;
import com.phonethics.sharepref.SharePref;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

public class WatchVideoSubmitted extends Activity {

	String 			VIDEO_URL,USER_ID,VIDEO_ID,LOGIN_USER_ID,USER_NAME,SongName;
	VideoView		vd;



	ImageView		pBar;
	Animation		rotate;
	Activity		context;
	NetworkCheck	netCheck;
	AlertDialog 	alert=null;
	SharePref		pref;
	String 		 	URL_VOTEUP;
	int				pos = 0;
	
	Map<String, String> 	flurryEeventParams;

	private static boolean useLocalytics 		= false;
	private static boolean useFlurry 			= false;
	private LocalyticsSession localyticsSession;
	private static  String LOCALYTICS_APP_KEY	="";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_watch_video_submitted);
		if(savedInstanceState!=null){
			pos = savedInstanceState.getInt("pos");
			context = WatchVideoSubmitted.this;
		}else{
			context 	= this;
		}
		useFlurry				= getResources().getBoolean(R.bool.useFlurry);
		useLocalytics			= getResources().getBoolean(R.bool.useLocalytics);

		if(useLocalytics){
			LOCALYTICS_APP_KEY 		= getResources().getString(R.string.LOCALYTICS_APP_KEY);
			this.localyticsSession 	= new LocalyticsSession(this.getApplicationContext(),LOCALYTICS_APP_KEY);
			this.localyticsSession.open();
			//this.localyticsSession.upload();
			//localyticsSession.tagScreen("Splash");
			this.localyticsSession.upload();
		}

		
		vd 			= (VideoView) findViewById(R.id.vide_full_screen);
		flurryEeventParams	= new HashMap<String, String>();
		netCheck	= new NetworkCheck(context);
		rotate 		= AnimationUtils.loadAnimation(context, R.anim.rotate);
		pBar		= (ImageView) findViewById(R.id.progressBar2);
		pBar.startAnimation(rotate);
		pBar.setVisibility(View.VISIBLE);
		pBar.startAnimation(rotate);
		pref = new SharePref(context);
		LOGIN_USER_ID = pref.getUserId();
		URL_VOTEUP = getResources().getString(R.string.url_md_live_server) + getResources().getString(R.string.url_vote_up);
		Bundle bundle = getIntent().getExtras();
		if(bundle!=null){
			VIDEO_URL = bundle.getString("VIDEO_URL");
			USER_ID = bundle.getString("USER_ID");
			VIDEO_ID = bundle.getString("VIDEO_ID");
			USER_NAME = bundle.getString("USER_NAME");
			SongName = bundle.getString("SongName");
			VIDEO_URL = VIDEO_URL.replaceAll(" ", "%20");
			Log.d("Url encode", "MdVideoUrl == USER_ID "+ USER_ID);
			Log.d("Url encode", "MdVideoUrl == VIDEO_ID "+ VIDEO_ID);
			Log.d("Url encode", "MdVideoUrl == VIDEO_URL "+ VIDEO_URL);
			Log.d("Url encode", "MdVideoUrl == LOGIN_USER_ID "+ LOGIN_USER_ID);
		}
		//flurryEeventParams.put("videoUserId", USER_ID);
		flurryEeventParams.put("videoId", VIDEO_ID);
		flurryEeventParams.put("UserNameToVote", USER_NAME);
		flurryEeventParams.put("SongName", SongName);
		/*if(useFlurry){
			FlurryAgent.logEvent("Vote", flurryEeventParams);
		}
		if(useLocalytics){
			localyticsSession.tagEvent("Vote", flurryEeventParams);
		}*/
		if(netCheck.isNetworkAvailable()){
			if(vd.isPlaying()){
				
			}
			new StreamVideo().execute();
		}else{
			Toast.makeText(context, "No Internet Connection", 0).show();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.watch_video_submitted, menu);
		return true;
	}


	private class StreamVideo extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressbar
			/* pDialog = new ProgressDialog(getActivity());
            // Set progressbar title
            pDialog.setTitle("Android Video Streaming Tutorial");
            // Set progressbar message
            pDialog.setMessage("Buffering...");
            pDialog.setIndeterminate(false);
            // Show progressbar
            pDialog.show();*/
			pBar.setVisibility(View.VISIBLE);
			pBar.startAnimation(rotate);

		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected void onPostExecute(Void args) {

			try {
				// Start the MediaController

				MediaController mediacontroller = new MediaController(context);
				mediacontroller.setAnchorView(vd);
				// Get the URL from String VideoURL
				Uri video = Uri.parse(VIDEO_URL);
				vd.setMediaController(mediacontroller);
				vd.setVideoURI(video);

				vd.requestFocus();

				vd.setOnPreparedListener(new OnPreparedListener() {
					// Close the progress bar and play the video
					public void onPrepared(MediaPlayer mp) {
						//pDialog.dismiss();
						
						if(pos!=0){
							vd.seekTo(pos);
						}
						mp.setOnBufferingUpdateListener(new OnBufferingUpdateListener() {

							@Override
							public void onBufferingUpdate(MediaPlayer mp, int percent) {
								// TODO Auto-generated method stub
								
							}
						});
						vd.start();
						pBar.clearAnimation();
						pBar.setVisibility(View.GONE);


					}
				});
				//

				vd.setOnCompletionListener(new OnCompletionListener() {

					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						//Toast.makeText(context, "Video Complete", 0).show();
						showAlert();
						//onBackPressed();
						//updateCounter(url_update_count);

					}
				});
				vd.setOnErrorListener(new OnErrorListener() {

					@Override
					public boolean onError(MediaPlayer mp, int what, int extra) {
						// TODO Auto-generated method stub
						Toast.makeText(context, "Some Error Occured", 0).show();
						return true;
					}
				});






			} catch (Exception e) {
				//pDialog.dismiss();
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}

		}

	}


	public void showAlert(){
		
		AlertDialog.Builder  alertDialog = new AlertDialog.Builder(context);
		//alertDialog.setIcon(R.drawable.ic_launcher);
		alertDialog.setTitle("Dance With Madhuri");
		alertDialog.setMessage("Vote Up this video.?");
		alertDialog.setCancelable(true);
		alertDialog.setPositiveButton("Vote Up", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				alert.dismiss();
				voteUp();
				//onBackPressed();
			}
		});
		alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				onBackPressed();
			}

		});

		alert = alertDialog.create();
		alert.show();
	}
	
	
	
	public void voteUp(){
		showToast("Please Wait.");
		pBar.setVisibility(View.VISIBLE);
		pBar.startAnimation(rotate);
		RequestQueue 	queue	= Volley.newRequestQueue(context);
		JSONObject jObj = new JSONObject();
		try{
			
			jObj.put("user_id", LOGIN_USER_ID);
			jObj.put("video_id", VIDEO_ID);
			jObj.put("video_user", USER_ID);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		VolleyResponse.Listener<JSONObject> listener = new VolleyResponse.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub
				pBar.setVisibility(View.GONE);
				pBar.clearAnimation();
				try{
					if(response.getString("success").equalsIgnoreCase("true")){
						String data = response.getString("data");
						showToast(data);
						if(useFlurry){
							FlurryAgent.logEvent("Vote", flurryEeventParams);
						}
						if(useLocalytics){
							localyticsSession.tagEvent("Vote", flurryEeventParams);
						}
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
			
				onBackPressed();
				Log.d("MdVoteUpResponse", "MdResponse >> "+response.toString());
			}
		};
		
		VolleyResponse.ErrorListener errorListener = new VolleyResponse.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				pBar.setVisibility(View.GONE);
				pBar.clearAnimation();
				
				Log.d("MdVoteUperror", "MdError >> "+error.toString());
				showToast("Some Error Occured");
				
				onBackPressed();
			}
		};
		
		JsonObjectRequest request = new JsonObjectRequest(Method.POST,URL_VOTEUP, jObj, listener, errorListener);
		queue.add(request);
		
	}
	
	
	public void showToast(String content){
		try{
			Toast.makeText(context, content, 0).show();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		setContentView(R.layout.activity_watch_video_submitted);
		super.onConfigurationChanged(newConfig);
	}
	
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (vd.isPlaying()){ 
			outState.putInt("pos", vd.getCurrentPosition());
			vd.pause();
			
		}else{
			outState.putInt("pos", 0);
		}
		context = WatchVideoSubmitted.this;


	}
	
	
	

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(savedInstanceState);
		if(savedInstanceState != null){
			context = WatchVideoSubmitted.this;
			pos = savedInstanceState.getInt("pos");
		}
		
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		finish();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if(useLocalytics){
			this.localyticsSession.close();
			this.localyticsSession.upload();
		}
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(useLocalytics){
			this.localyticsSession.open();
			//localyticsSession.tagScreen("Watch_Video");

		}
	}
	
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if(useFlurry){
			FlurryAgent.onStartSession(context, getResources().getString(R.string.flurry_id));

		}
	}


	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if(useFlurry){
			FlurryAgent.onEndSession(context);
		}


	}

}


