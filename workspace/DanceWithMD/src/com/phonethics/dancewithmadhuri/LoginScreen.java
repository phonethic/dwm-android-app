package com.phonethics.dancewithmadhuri;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.InputType;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.BadTokenException;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;


import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyResponse;
import com.android.volley.VolleyResponse.ErrorListener;
import com.android.volley.VolleyResponse.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.flurry.android.FlurryAgent;
import com.localytics.android.LocalyticsSession;

import com.phonethics.flip.DisplayNextView;
import com.phonethics.flip.Flip3dAnimation;
import com.phonethics.fragments.RootFragment;
import com.phonethics.imageloader.ImageLoader;
import com.phonethics.network.NetworkCheck;
import com.phonethics.sharepref.SharePref;

public class LoginScreen extends Activity implements OnClickListener {


	Button			bttn_login,bttn_passwrd,bttn_login_account,bttn_fb,bttn_signup;
	Button			bttn_register,bttn_cancel;
	Typeface		tf;
	Context			context;
	ViewSwitcher	viewSwitch;

	ImageView		pBar;
	Animation		rotate;
	RelativeLayout	layout_login,relative_sign_up,layout_login_fb;
	TextView		text_or,text_have_account;
	EditText		edit_email,edit_password,edit_sign_name,edit_sign_email,edit_sign_pass,edit_sign_veri_pass;
	Activity		actcontext;
	String 			fbUserid="";
	String 			fbAccessToken="";
	private static final List<String> PERMISSIONS = Arrays.asList("publish_actions");
	private boolean pendingPublishReauthorization = false;

	SharePref		sharePref;
	Animation		fade_in_anim,fade_out_anim;

	RadioGroup		radioSexGroup;

	String			server_url="", sign_up_api_url="",url_signUp,log_in_url="",url_forgot_pass="",url_userInfo="";
	String			url_jhalak_check="",url_valid_api;

	boolean			isLoggedIn;
	NetworkCheck	netCheck;
	Map<String, String> 	flurryEeventParams;
	boolean			versionExpire = false;
	AlertDialog alert=null;


	private static boolean useLocalytics 		= false;
	private static boolean useFlurry 			= false;
	private LocalyticsSession localyticsSession;
	private static  String LOCALYTICS_APP_KEY	=""; 


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		context				= this;
		actcontext 			= this;

		useFlurry				= getResources().getBoolean(R.bool.useFlurry);
		useLocalytics			= getResources().getBoolean(R.bool.useLocalytics);

		if(useLocalytics){
			LOCALYTICS_APP_KEY 		= getResources().getString(R.string.LOCALYTICS_APP_KEY);
			this.localyticsSession 	= new LocalyticsSession(this.getApplicationContext(),LOCALYTICS_APP_KEY);
			this.localyticsSession.open();
			//this.localyticsSession.upload();
			localyticsSession.tagScreen("LoginScreen");
			this.localyticsSession.upload();
		}
		sharePref 			= new SharePref(context);
		netCheck			= new NetworkCheck(context);
		rotate 				= AnimationUtils.loadAnimation(context, R.anim.rotate);
		pBar				= (ImageView) findViewById(R.id.progressBar2);
		server_url			= getResources().getString(R.string.url_md_live_server);
		sign_up_api_url		= getResources().getString(R.string.url_registeration);
		url_signUp			= server_url+sign_up_api_url;
		log_in_url			= server_url+getResources().getString(R.string.url_login);
		url_forgot_pass		= server_url+getResources().getString(R.string.url_forgot_password);
		url_userInfo		= server_url+getResources().getString(R.string.url_user_details);
		url_jhalak_check	= server_url+getResources().getString(R.string.url_jhalak_check);
		url_valid_api		= server_url+getResources().getString(R.string.url_valid_api);
		viewSwitch			= (ViewSwitcher) findViewById(R.id.view_switcher);
		flurryEeventParams	= new HashMap<String, String>();

		if(sharePref.isUserLoggedIn()){				/// is user already logged in ...?
			Log.d("", "MdLogin user login true");
			isLoggedIn = true;
			sharePref.setNewUserLogin(false);
			if(sharePref.getApiResponse()){			/// have we got the api response from previous splash activity ...?
				Log.d("", "MdLogin api response true");
				if(sharePref.getAppUpdate()){		// is app upto date ...?
					Log.d("", "MdLogin app update true");
					sharePref.setPresentTab("Learn Dance");
					sharePref.setHelpImageVisible(false);
					localyticsSession.setCustomerName(sharePref.getUserName());
					Intent intent = new Intent(context, RootFragment.class);
					startActivity(intent);
					finish();
				}else{
					versionExpire = true;
				}
			}else{												/// user log in but api check have not been done.
				if(netCheck.isNetworkAvailable()){
					checkApi(url_valid_api);	
				}else{
					sharePref.setPresentTab("Learn Dance");
					sharePref.setHelpImageVisible(false);
					Intent intent = new Intent(context, RootFragment.class);
					startActivity(intent);
					finish();
				}


			}


		}else if(netCheck.isNetworkAvailable()){		// user not logged in check for network
			isLoggedIn = false;
			if(sharePref.getApiResponse()){

			}else{
				checkApi(url_valid_api);
			}

		}

		//if(sharePref.isUserLoggedIn()){if()

		tf					= Typeface.createFromAsset(context.getAssets(), "fonts/KabelMediumBT.ttf");



		layout_login		= (RelativeLayout) findViewById(R.id.relative_view_2);
		relative_sign_up	= (RelativeLayout) findViewById(R.id.relative_sign_up);
		layout_login_fb		= (RelativeLayout) findViewById(R.id.relative_login_facebook);
		relative_sign_up.setVisibility(View.INVISIBLE);

		bttn_login 			= (Button) findViewById(R.id.bttn_login);
		bttn_passwrd 		= (Button) findViewById(R.id.bttn_forgotPassword);
		bttn_login_account 	= (Button) findViewById(R.id.bttn_Login);
		bttn_fb				= (Button) findViewById(R.id.bttn_loginWithFb);
		bttn_signup			= (Button) findViewById(R.id.bttn_signup);
		bttn_register		= (Button) findViewById(R.id.bttn_register);
		bttn_cancel			= (Button) findViewById(R.id.bttn_cancel);


		edit_email			= (EditText) findViewById(R.id.edit_email);
		edit_password		= (EditText) findViewById(R.id.edit_password);

		edit_sign_name		= (EditText) findViewById(R.id.edit_signup_name);
		edit_sign_email		= (EditText) findViewById(R.id.edit_signup_email);
		edit_sign_pass		= (EditText) findViewById(R.id.edit_signup_password);
		edit_sign_veri_pass	= (EditText) findViewById(R.id.edit_signup_verify_password);


		text_or				= (TextView) findViewById(R.id.text_or);
		text_have_account	= (TextView) findViewById(R.id.text_haveAccount);

		radioSexGroup	  	= (RadioGroup) findViewById(R.id.radioSex);


		pBar.setVisibility(View.INVISIBLE);


		fade_in_anim		= AnimationUtils.loadAnimation(context, R.anim.slide_anim_from_right);
		fade_out_anim		= AnimationUtils.loadAnimation(context, R.anim.slide_out_right);

		bttn_login.setTypeface(tf);
		bttn_passwrd.setTypeface(tf);
		bttn_login_account.setTypeface(tf);
		bttn_fb.setTypeface(tf);
		bttn_signup.setTypeface(tf);
		bttn_register.setTypeface(tf);
		bttn_cancel.setTypeface(tf);

		edit_email.setTypeface(tf);
		edit_password.setTypeface(tf);
		edit_sign_name.setTypeface(tf);
		edit_sign_email.setTypeface(tf);
		edit_sign_pass.setTypeface(tf);
		edit_sign_veri_pass.setTypeface(tf);


		text_or.setTypeface(tf);
		text_have_account.setTypeface(tf);

		text_or.setTextColor(Color.WHITE);
		text_have_account.setTextColor(Color.WHITE);






		try {
			PackageInfo info = getPackageManager().getPackageInfo("com.phonethics.dancewithmadhuri",PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Log.d("MY KEY HASH:", "MY KEY HASH:" +Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (NameNotFoundException e) {

		} catch (NoSuchAlgorithmException e) {

		}

		/*if(sharePref.isUserLoggedIn()){
			Intent intent = new Intent(context, RootFragment.class);
			startActivity(intent);
			finish();
		}*/

		bttn_login.setOnClickListener(this);
		bttn_fb.setOnClickListener(this);
		bttn_signup.setOnClickListener(this);
		bttn_cancel.setOnClickListener(this);
		bttn_register.setOnClickListener(this);
		bttn_login_account.setOnClickListener(this);
		bttn_passwrd.setOnClickListener(this);

		//		bttn_login.setOnClickListener(new OnClickListener() {
		//
		//			@Override
		//			public void onClick(View v) {
		//				// TODO Auto-generated method stub
		//				if(pBar.isShown()){
		//					showToast("Please Wait.");
		//				}else{
		//					viewSwitch.showNext();
		//					applyRotation(0, 90);
		//				}
		//			}
		//		});
		//
		//		bttn_fb.setOnClickListener(new OnClickListener() {
		//
		//			@Override
		//			public void onClick(View v) {
		//				// TODO Auto-generated method stub
		//				if(netCheck.isNetworkAvailable()){
		//					if(pBar.isShown()){
		//						showToast("Please Wait.Log in taking place.");
		//					}else{
		//						try{
		//							sharePref.setPresentTab("Home");
		//							sharePref.setLoginFromFb(true);
		//							pBar.setVisibility(View.VISIBLE);
		//							login_facebook();
		//						}catch(Exception ex){
		//							ex.printStackTrace();
		//						}
		//					}
		//				}else{
		//					showToast("No Internet Connection");
		//				}
		//
		//			}
		//		});
		//
		//
		//
		//		bttn_signup.setOnClickListener(new OnClickListener() {
		//
		//			@Override
		//			public void onClick(View v) {
		//				// TODO Auto-generated method stub
		//				clearEditData();
		//				layout_login_fb.setVisibility(View.INVISIBLE);
		//				//relative_sign_up.setVisibility(View.VISIBLE);
		//				relative_sign_up.startAnimation(fade_in_anim);
		//				fade_in_anim.setFillAfter(true);
		//				fade_in_anim.setAnimationListener(new AnimationListener() {
		//
		//					@Override
		//					public void onAnimationStart(Animation animation) {
		//						// TODO Auto-generated method stub
		//
		//					}
		//
		//					@Override
		//					public void onAnimationRepeat(Animation animation) {
		//						// TODO Auto-generated method stub
		//
		//					}
		//
		//					@Override
		//					public void onAnimationEnd(Animation animation) {
		//						// TODO Auto-generated method stub
		//						relative_sign_up.setVisibility(View.VISIBLE);
		//					}
		//				});
		//			}
		//		});
		//
		//		bttn_cancel.setOnClickListener(new OnClickListener() {
		//
		//			@Override
		//			public void onClick(View v) {
		//				// TODO Auto-generated method stub
		//				//layout_login_fb.setVisibility(View.VISIBLE);
		//				//relative_sign_up.setVisibility(View.INVISIBLE);
		//
		//				relative_sign_up.startAnimation(fade_out_anim);
		//				fade_out_anim.setFillAfter(true);
		//				fade_out_anim.setAnimationListener(new AnimationListener() {
		//
		//					@Override
		//					public void onAnimationStart(Animation animation) {
		//						// TODO Auto-generated method stub
		//
		//					}
		//
		//					@Override
		//					public void onAnimationRepeat(Animation animation) {
		//						// TODO Auto-generated method stub
		//
		//					}
		//
		//					@Override
		//					public void onAnimationEnd(Animation animation) {
		//						// TODO Auto-generated method stub
		//						layout_login_fb.setVisibility(View.VISIBLE);
		//						layout_login_fb.bringToFront();
		//						relative_sign_up.setVisibility(View.INVISIBLE);
		//					}
		//				});
		//			}
		//		});
		//
		//		bttn_register.setOnClickListener(new OnClickListener() {
		//
		//			@Override
		//			public void onClick(View v) {
		//				// TODO Auto-generated method stub
		//				/*if(relative_sign_up.isShown()){*/
		//
		//				int selectedId = radioSexGroup.getCheckedRadioButtonId();
		//
		//				// find the radiobutton by returned id
		//				String 	email_id = edit_sign_email.getText().toString();
		//				String	password = edit_sign_pass.getText().toString();
		//				String	name	 = edit_sign_name.getText().toString();
		//				String	confirm_pass = edit_sign_veri_pass.getText().toString();
		//
		//				if(		(email_id.equalsIgnoreCase("")) || (email_id.length()==0)	){
		//					showToast("Enter Email Id");
		//				}else if(		(password.equalsIgnoreCase("")) || (password.length()==0)	){
		//					showToast("Enter Password");
		//				}else if(		(name.equalsIgnoreCase("")) || (name.length()==0)	){
		//					showToast("Enter Name");
		//				}else if(		(confirm_pass.equalsIgnoreCase("")) || (confirm_pass.length()==0)	){
		//					showToast("Verify Password can not be blank.");
		//				}else if(	!isValidEmail(email_id)	){
		//					showToast("Please Enter Valid Email Id");
		//				}else{
		//					if(netCheck.isNetworkAvailable()){
		//						RadioButton radioSexButton = (RadioButton) findViewById(selectedId);
		//						registerUserCall(email_id,name,password,confirm_pass,radioSexButton.getText().toString(),url_signUp);
		//					}else{
		//						showToast("No Internet Connection");
		//					}
		//				}
		//
		//				/*RadioButton radioSexButton = (RadioButton) findViewById(selectedId);
		//				registerUserCall(email_id,name,password,confirm_pass,radioSexButton.getText().toString(),url_signUp);*/
		//				//}
		//
		//			}
		//		});
		//
		//
		//		bttn_login_account.setOnClickListener(new OnClickListener() {
		//
		//			@Override
		//			public void onClick(View v) {
		//				// TODO Auto-generated method stub
		//				if(pBar.isShown()){
		//					showToast("Please Wait.Log in taking place.");
		//				}else{
		//					if(netCheck.isNetworkAvailable()){
		//						String email_id = edit_email.getText().toString();
		//						String psswrd 	= edit_password.getText().toString();
		//						if(		(email_id.equalsIgnoreCase("")) || (email_id.length()==0)	){
		//							showToast("Enter Email Id");
		//						}else if(	(psswrd.equalsIgnoreCase("")) || (psswrd.length()==0)	){
		//							showToast("Enter Password");
		//						}else if( !isValidEmail(email_id) ){
		//							showToast("Please Enter Valid Email Id");
		//						}else{
		//							pBar.setVisibility(View.VISIBLE);
		//							sharePref.setLoginFromFb(false);
		//							login_user_account(	edit_email.getText().toString(),md5(	edit_password.getText().toString()	)	);
		//						}
		//					}else{
		//						showToast("No Internet Connection");
		//					}
		//				}
		//
		//
		//				/*login_user_account(edit_email.getText().toString(),edit_password.getText().toString());
		//				login_user_account("nitin@phonethics.in",md5("12345678"));*/
		//
		//			}
		//		});
		//
		//
		//		bttn_passwrd.setOnClickListener(new OnClickListener() {
		//
		//			@Override
		//			public void onClick(View v) {
		//				// TODO Auto-generated method stub
		//				if(pBar.isShown()){
		//
		//				}else{
		//					if(netCheck.isNetworkAvailable()){
		//						String emailId = edit_email.getText().toString();
		//						forgotPasswordRequest(emailId);	
		//					}else{
		//						showToast("No Internet Connection");
		//					}
		//				}
		//			}
		//		});

		if(versionExpire){
			showAlertOfAppExpire();
		}else{

		}


	}

	public void clearEditData(){
		edit_sign_email.setText("");
		edit_sign_name.setText("");
		edit_sign_pass.setText("");
		edit_sign_veri_pass.setText("");
	} 



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return false;
	}


	private void applyRotation(float start, float end) {
		// Find the center of image
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);

		final float centerX = metrics.widthPixels / 2.0f;
		final float centerY = metrics.heightPixels / 2.0f;

		// Create a new 3D rotation with the supplied parameter
		// The animation listener is used to trigger the next animation
		final Flip3dAnimation rotation = new Flip3dAnimation(start, end, centerX, centerY);
		rotation.setDuration(500);
		rotation.setFillAfter(true);
		rotation.setInterpolator(new AccelerateInterpolator());
		rotation.setAnimationListener(new DisplayNextView(layout_login.isShown(), viewSwitch,context,centerX,centerY));

		if (layout_login.isShown()){
			/*view.showNext();*/
			viewSwitch.startAnimation(rotation);
		} /*else {
			image2.startAnimation(rotation);
		}*/

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(layout_login.isShown()){
			if(relative_sign_up.isShown()){
				bttn_cancel.performClick();
			}else{
				viewSwitch.showPrevious();
			}
			//applyRotation(0, 90);
		}else{
			super.onBackPressed();
		}

	}



	public void login_facebook()
	{

		try
		{
			Session.openActiveSession(actcontext, true, new Session.StatusCallback() {

				@Override
				public void call(final Session session, SessionState state, Exception exception) {
					// TODO Auto-generated method stub
					if(session.isOpened())
					{
						Toast.makeText(context, "Please Wait", 1).show();
						List<String> permissions = session.getPermissions();
						if (!isSubsetOf(PERMISSIONS, permissions)) {
							pendingPublishReauthorization = true;
							Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(actcontext, PERMISSIONS);
							session.requestNewPublishPermissions(newPermissionsRequest);
						}

						Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {

							@Override
							public void onCompleted(GraphUser user, Response response) {
								// TODO Auto-generated method stub
								if(user!=null)
								{	flurryEeventParams.put("Login_from", "facebook");
								if(useFlurry){	
									FlurryAgent.logEvent(getResources().getString(R.string.fEvent_login),flurryEeventParams);
								}
								if(useLocalytics){
									localyticsSession.tagEvent(getResources().getString(R.string.localEvent_login), flurryEeventParams);
								}
								fbUserid=user.getId();
								fbAccessToken=session.getAccessToken();
								Log.i("User Name ", "Name : "+user.getName());
								Log.i("User Id ", "Id : "+user.getId());
								Log.i("User Access Token ", "Access Token : "+session.getAccessToken());

								sharePref.setUserLogin(true);
								sharePref.setNewUserLogin(true);
								sharePref.setPresentTab("Home");
								sharePref.setHelpImageVisible(true);
								sharePref.setUserName(user.getName());
								sharePref.setUserId(user.getId());
								if(useLocalytics){
									localyticsSession.setCustomerName(user.getName());
								}
								pBar.clearAnimation();
								pBar.setVisibility(View.GONE);
								Intent intent = new Intent(context, RootFragment.class);
								//intent.putExtra("name", user.getName());
								startActivity(intent);
								finish();

								}

								else{
									pBar.clearAnimation();
									pBar.setVisibility(View.GONE);
									showToast("Network Error");
									Log.i("ELSE", "ELSE");
								}
							}


						});
						/*session.getAccessToken();
					Toast.makeText(context, "Session AccessToken : "+session.getAccessToken(), Toast.LENGTH_LONG).show();*/
					}

					if(session.isClosed()){

						//Toast.makeText(context, "Closed", 0).show();
					}

				}


			});}catch(NullPointerException npx)
			{
				npx.printStackTrace();
			}
		catch(BadTokenException bdx)
		{
			bdx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	public boolean checkIfSessionOpened(){
		isLoggedIn = false;
		try
		{
			Session.openActiveSession(actcontext, true, new Session.StatusCallback() {

				@Override
				public void call(final Session session, SessionState state, Exception exception) {
					// TODO Auto-generated method stub
					if(session.isOpened()){
						isLoggedIn = true;
					}

					if(session.isClosed()){
						//Toast.makeText(context, "Closed", 0).show();
						isLoggedIn = false;
					}

				}


			});
		}catch(NullPointerException npx){
			npx.printStackTrace();
		}
		catch(BadTokenException bdx){
			bdx.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return isLoggedIn;
	}

	private boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
		for (String string : subset) {
			if (!superset.contains(string)) {
				return false;
			}
		}
		return true;
	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		try
		{
			Log.d("", "MdFbLogin requestCode >> "+requestCode);
			Log.d("", "MdFbLogin resultCode >> "+resultCode);
			if(resultCode==0){
				pBar.clearAnimation();
				pBar.setVisibility(View.GONE);
			}
			Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
		}catch(IllegalStateException ilgx)
		{
			ilgx.printStackTrace();
		}
		catch (NullPointerException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}


	public void registerUserCall(final String email,final String name,final String password,final String verifyPassword,String gender,String url){
		RequestQueue queue = Volley.newRequestQueue(context);

		JSONObject json = new JSONObject();
		try{
			/*json.put("uname", "bathija.nitin@gmail.com");
			json.put("name", "nitin");
			json.put("gender", "Male");
			json.put("pass", "12345678");
			json.put("confirm_pass", "12345678");*/

			json.put("uname", email);
			json.put("name", name);
			json.put("gender", gender);
			json.put("pass", password);
			json.put("confirm_pass", verifyPassword);
		}catch(Exception ex){
			ex.printStackTrace();
		}

		//JsonArrayRequest req = new JsonArrayRequest(Method.POST, url, json, createMyReqSuccessListener1(), createMyReqErrorListener());


		JsonObjectRequest req = new JsonObjectRequest(Method.POST, url, json, new VolleyResponse.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub
				pBar.clearAnimation();
				pBar.setVisibility(View.GONE);
				Log.d("response", "MDError "+response.toString());
				try {
					String data = response.getString("message");
					//showToast(data);
					if(data.equalsIgnoreCase("success")){
						showToast("User Registered Successfully");
					}else if(data.equalsIgnoreCase("Already registered")){
						showToast("User Already Registered");
					}else if(data.equalsIgnoreCase("Password not matching")){
						showToast("Passwords are not matching.");
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		},new VolleyResponse.ErrorListener(){

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				pBar.clearAnimation();
				pBar.setVisibility(View.GONE);
				Toast.makeText(context, "Network Error!", 0).show();
				Log.d("response", "MDError "+error.toString());
			}

		});

		pBar.startAnimation(rotate);
		pBar.setVisibility(View.VISIBLE);

		queue.add(req);


	}


	public void showToast(String content){
		try{
			Toast.makeText(context, content, 0).show();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}



	public void login_user_account(final String email, String password){
		RequestQueue queue = Volley.newRequestQueue(context);
		Log.d("", "user email "+email);
		Log.d("", "user password "+password);
		JSONObject json = new JSONObject();
		try{
			//email = "nitin@pgonethics.in";
			json.put("uname", email);
			json.put("pass", password);
			Log.d("", "MDPass" + password);

			/*json.put("uname", email);
			json.put("pass", password);*/


		}catch(Exception ex){
			ex.printStackTrace();
		}

		//JsonArrayRequest req = new JsonArrayRequest(Method.POST, url, json, createMyReqSuccessListener1(), createMyReqErrorListener());

		JsonObjectRequest req = new JsonObjectRequest(Method.POST,log_in_url,json, new VolleyResponse.Listener<JSONObject>(){

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub
				pBar.clearAnimation();
				pBar.setVisibility(View.GONE);
				Log.d("Login", "MDLogin "+response.toString());
				//showToast("Success");

				String token = "";
				String data = "";
				String success = "false";
				try {
					success = response.getString("success");
					if(success.equalsIgnoreCase("true")){
						token = response.getJSONObject("data").getString("token");

					}else{
						String message = response.getString("message");
						showToast(message);
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					token="";
					try{
						data= response.getString("message");
					}catch(Exception ex){
						data="";
						ex.printStackTrace();
					}
					e.printStackTrace();
				}
				if(!token.equalsIgnoreCase("")){
					/*sharePref.setLoginFromFb(false);
					sharePref.setPresentTab("Home");*/
					sharePref.setUserToken(token);
					//sharePref.setUserLogin(true);
					getUserInfoRequest(email,token);
					//showToast("Token "+ token);
					/*Intent intent = new Intent(context, RootFragment.class);
					startActivity(intent);
					finish();		*/
				}else if(!data.equalsIgnoreCase("")){
					showToast(data);
				}
			}

		}, new VolleyResponse.ErrorListener(){

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				pBar.clearAnimation();
				pBar.setVisibility(View.GONE);
				Log.d("Login", "MDLogin Error "+error.toString());
				showToast("Network Error");
			}

		});
		pBar.startAnimation(rotate);
		pBar.setVisibility(View.VISIBLE);
		queue.add(req);

	}




	public void forgotPasswordRequest(String email){
		RequestQueue queue = Volley.newRequestQueue(context);
		Log.d("", "user email "+email);
		JSONObject json = new JSONObject();
		try{
			json.put("uname", email);
			//json.put("pass", "12345678");

			/*json.put("uname", email);
			json.put("pass", password);*/


		}catch(Exception ex){
			ex.printStackTrace();
		}

		JsonObjectRequest req = new JsonObjectRequest(Method.POST,url_forgot_pass,json,new VolleyResponse.Listener<JSONObject>() {


			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub
				try{
					pBar.clearAnimation();
					pBar.setVisibility(View.GONE);
					Log.d("", "MDForgotPssword Response "+response.toString());
					String success = response.getString("success");
					String data = "";
					if(success.equalsIgnoreCase("true")){
						data = response.getString("data");
						if(data.equalsIgnoreCase("success")){
							showToast("Password reset email sent. Please check your email for further instructions.");
						}
					}else{
						data = response.getString("message");

						if(data.equalsIgnoreCase("Not registered")){
							showToast("User not registered");
						}else if(data.equalsIgnoreCase("failed")){
							showToast("Failed to reset password. Please try again later.");
						}

					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
		},new VolleyResponse.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub

				pBar.clearAnimation();
				Log.d("", "MDForgotPssword Error "+error.toString());
				showToast("Network Error!");
			}
		});
		pBar.startAnimation(rotate);
		pBar.setVisibility(View.VISIBLE);
		queue.add(req);

	}

	public final static boolean isValidEmail(CharSequence target) {
		if (target == null) {
			return false;
		} else {
			return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
		}
	}

	private String md5(String in) {

		MessageDigest digest;

		try {

			digest = MessageDigest.getInstance("MD5");

			digest.reset();

			digest.update(in.getBytes());

			byte[] a = digest.digest();

			int len = a.length;

			StringBuilder sb = new StringBuilder(len << 1);

			for (int i = 0; i < len; i++) {

				sb.append(Character.forDigit((a[i] & 0xf0) >> 4, 16));

				sb.append(Character.forDigit(a[i] & 0x0f, 16));

			}

			return sb.toString();

		} catch (NoSuchAlgorithmException e) { e.printStackTrace(); }

		return null;

	}



	public void getUserInfoRequest(String emailId,String token){
		String url = url_userInfo + emailId + "/" + token;
		Log.d("URL", "MDUserUrl "+url);

		RequestQueue queue = Volley.newRequestQueue(context);
		JsonObjectRequest req = new JsonObjectRequest(Method.GET, url, null, new VolleyResponse.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub
				try{
					String id="",user_name="",email="",gender="",login_count="",registered="",last_login="",picture="",
							fb_link="",phone="",points="";
					Log.d("user info", "MDUSerInfo "+response.toString());
					String success = response.getString("success");
					if(success.equalsIgnoreCase("true")){
						flurryEeventParams.put("Login_from", "DWM_SERVER");
						if(useFlurry){	
							FlurryAgent.logEvent(getResources().getString(R.string.fEvent_login),flurryEeventParams);

						}
						if(useLocalytics){
							localyticsSession.tagEvent(getResources().getString(R.string.localEvent_login), flurryEeventParams);
						}

						JSONArray jArr = response.getJSONArray("data");
						for(int i=0; i <jArr.length();i++){
							JSONObject jObj =  jArr.getJSONObject(i);
							id			=	jObj.getString("ID");
							user_name	=	jObj.getString("User_Name");
							email		=	jObj.getString("Email");
							gender		=	jObj.getString("Gender");
							login_count	=	jObj.getString("Login_Count");
							registered	=	jObj.getString("Registered_Date");
							last_login	=	jObj.getString("Last_Login");
							picture		=	jObj.getString("Picture");
							fb_link		=	jObj.getString("FB_Link");
							phone		=	jObj.getString("Phone");
							points		=	jObj.getString("points");
						}

						sharePref.setUserId(id);
						sharePref.setUserName(user_name);
						sharePref.setUserEmail(email);
						sharePref.setUserGender(gender);
						sharePref.setUserLogin_count(login_count);
						sharePref.setUserRegistered(registered);
						sharePref.setUserLast_login(last_login);
						sharePref.setUserPicture(picture);
						sharePref.setUserFb_link(fb_link);
						sharePref.setUserPhone(phone);
						sharePref.setUserPoints(points);

						sharePref.setLoginFromFb(false);
						sharePref.setPresentTab("Home");
						sharePref.setHelpImageVisible(true);
						//sharePref.setUserToken(token);
						sharePref.setUserLogin(true);
						sharePref.setNewUserLogin(true);

						pBar.clearAnimation();
						pBar.setVisibility(View.GONE);
						if(useLocalytics){
							localyticsSession.setCustomerName(user_name);
						}
						Intent intent = new Intent(context, RootFragment.class);
						startActivity(intent);
						finish();

					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
		}, new VolleyResponse.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				Log.d("error", "MDUserInfo error "+error.toString());
				pBar.clearAnimation();
				pBar.setVisibility(View.GONE);
				showToast("Network Error");
			}
		}); 

		pBar.startAnimation(rotate);
		pBar.setVisibility(View.VISIBLE);
		queue.add(req);
	}


	public void checkJhalak(String url){
		boolean jhalakValid = false;
		try{
			Log.d("", "MdCheckJhalak Url >> "+url.toString());
			if(isLoggedIn){
				pBar.setVisibility(View.VISIBLE);
				pBar.startAnimation(rotate);
			}
			RequestQueue queue = Volley.newRequestQueue(context);
			VolleyResponse.Listener<JSONObject> listener = new Listener<JSONObject>() {

				@Override
				public void onResponse(JSONObject response) {
					// TODO Auto-generated method stub
					try{
						pBar.setVisibility(View.GONE);
						pBar.clearAnimation();
						Log.d("", "MdCheckJhalak Response >> "+response.toString());
						if(response.getString("success").equalsIgnoreCase("true")){

							if(sharePref.isUserLoggedIn()){
								sharePref.setPresentTab("Learn Dance");
								sharePref.setHelpImageVisible(false);
								Intent intent = new Intent(context, RootFragment.class);
								startActivity(intent);
								finish();
								overridePendingTransition(0, R.anim.activity_push_up_in);
							}else{
								viewSwitch.setVisibility(View.VISIBLE);
							}
						}

					}catch(Exception ex){
						ex.printStackTrace();
					}
				}
			};

			VolleyResponse.ErrorListener errorListener = new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {
					// TODO Auto-generated method stub
					Log.d("", "MdCheckJhalak error >> "+error.toString());
					pBar.setVisibility(View.GONE);
					pBar.clearAnimation();
				}
			};

			JsonObjectRequest request = new JsonObjectRequest(url, null, listener, errorListener);
			queue.add(request);
		}catch(OutOfMemoryError oom){
			oom.printStackTrace();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	public void checkApi(String url){
		Log.d("", "MdCheckApi Url >> "+url.toString());
		try{
			RequestQueue queue = Volley.newRequestQueue(context);
			if(isLoggedIn){
				pBar.setVisibility(View.VISIBLE);
				pBar.startAnimation(rotate);
			}
			//viewSwitch.setVisibility(View.INVISIBLE);
			VolleyResponse.Listener<JSONObject> listener = new Listener<JSONObject>() {

				@Override
				public void onResponse(JSONObject response) {
					// TODO Auto-generated method stub
					try{
						Log.d("", "MdCheckApi >> "+response.toString());
						if(response.getString("success").equalsIgnoreCase("true")){
							checkJhalak(url_jhalak_check);
						}else{
							showAlertOfAppExpire();
						}

					}catch(Exception ex){
						ex.printStackTrace();
					}
				}
			};

			VolleyResponse.ErrorListener errorListener = new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {
					// TODO Auto-generated method stub
					Log.d("", "MdCheckApiError >> "+error.toString());
					pBar.setVisibility(View.GONE);
					pBar.clearAnimation();
				}
			};

			JsonObjectRequest request = new JsonObjectRequest(url, null, listener, errorListener);
			queue.add(request);
		}catch(OutOfMemoryError oom){
			oom.printStackTrace();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if(useFlurry){
			FlurryAgent.onStartSession(this, getResources().getString(R.string.flurry_id));

		}

	}


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if(useFlurry){
			FlurryAgent.onEndSession(this);
		}


	}



	public void showAlertOfAppExpire(){

		AlertDialog.Builder  alertDialog = new AlertDialog.Builder(context);

		alertDialog.setTitle("Dance With Madhuri");
		alertDialog.setMessage(getResources().getString(R.string.app_expire));
		alertDialog.setCancelable(false);

		alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				//
				alert.dismiss();
				finish();
			}
		});

		alert = alertDialog.create();
		alert.show();
	}




	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {


		case R.id.bttn_login:
			if(pBar.isShown()){
				showToast("Please Wait.");
			}else{
				viewSwitch.showNext();
				applyRotation(0, 90);
			}
			break;

		case R.id.bttn_forgotPassword:
			if(pBar.isShown()){

			}else{
				if(netCheck.isNetworkAvailable()){
					String emailId = edit_email.getText().toString();
					forgotPasswordRequest(emailId);
					if(useLocalytics){
						localyticsSession.tagEvent("Forgot_Password");

						this.localyticsSession.upload();
					}
					if(useFlurry){
						FlurryAgent.logEvent("Forgot_Password");
					}
				}else{
					showToast("No Internet Connection");
				}
			}
			break;		
		case R.id.bttn_Login:
			if(pBar.isShown()){
				showToast("Please Wait.Log in taking place.");
			}else{
				if(netCheck.isNetworkAvailable()){
					String email_id = edit_email.getText().toString();
					String psswrd 	= edit_password.getText().toString();
					if(		(email_id.equalsIgnoreCase("")) || (email_id.length()==0)	){
						showToast("Enter Email Id");
					}else if(	(psswrd.equalsIgnoreCase("")) || (psswrd.length()==0)	){
						showToast("Enter Password");
					}else if( !isValidEmail(email_id) ){
						showToast("Please Enter Valid Email Id");
					}else{
						pBar.startAnimation(rotate);
						pBar.setVisibility(View.VISIBLE);
						sharePref.setLoginFromFb(false);
						login_user_account(	edit_email.getText().toString(),md5(	edit_password.getText().toString()	)	);
					}
				}else{
					showToast("No Internet Connection");
				}
			}
			break;		
		case R.id.bttn_loginWithFb:
			if(netCheck.isNetworkAvailable()){
				if(pBar.isShown()){
					showToast("Please Wait.Log in taking place.");
				}else{
					try{
						sharePref.setPresentTab("Home");
						sharePref.setHelpImageVisible(true);
						sharePref.setLoginFromFb(true);
						pBar.setVisibility(View.VISIBLE);
						pBar.startAnimation(rotate);
						login_facebook();
					}catch(Exception ex){
						ex.printStackTrace();
					}
				}
			}else{
				showToast("No Internet Connection");
			}
			break;
		case R.id.bttn_signup:
			clearEditData();
			if(useLocalytics){
				localyticsSession.tagScreen("Sign_Up");
				this.localyticsSession.upload();
			}
			/*if(useFlurry){
				FlurryAgent.logEvent("Sign_up");
			}*/
			layout_login_fb.setVisibility(View.INVISIBLE);
			//relative_sign_up.setVisibility(View.VISIBLE);
			relative_sign_up.startAnimation(fade_in_anim);
			fade_in_anim.setFillAfter(true);
			fade_in_anim.setAnimationListener(new AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub
					relative_sign_up.setVisibility(View.VISIBLE);
				}
			});
			break;
		case R.id.bttn_register:
			int selectedId = radioSexGroup.getCheckedRadioButtonId();

			// find the radiobutton by returned id
			String 	email_id = edit_sign_email.getText().toString();
			String	password = edit_sign_pass.getText().toString();
			String	name	 = edit_sign_name.getText().toString();
			String	confirm_pass = edit_sign_veri_pass.getText().toString();

			if(		(email_id.equalsIgnoreCase("")) || (email_id.length()==0)	){
				showToast("Enter Email Id");
			}else if(		(password.equalsIgnoreCase("")) || (password.length()==0)	){
				showToast("Enter Password");
			}else if(		(name.equalsIgnoreCase("")) || (name.length()==0)	){
				showToast("Enter Name");
			}else if(		(confirm_pass.equalsIgnoreCase("")) || (confirm_pass.length()==0)	){
				showToast("Verify Password can not be blank.");
			}else if(	!isValidEmail(email_id)	){
				showToast("Please Enter Valid Email Id");
			}else{
				if(netCheck.isNetworkAvailable()){
					RadioButton radioSexButton = (RadioButton) findViewById(selectedId);
					registerUserCall(email_id,name,password,confirm_pass,radioSexButton.getText().toString(),url_signUp);
					FlurryAgent.logEvent(getResources().getString(R.string.fEvent_login),flurryEeventParams);
					if(useLocalytics){
						localyticsSession.tagEvent("Sign_Up");
						this.localyticsSession.upload();
					}
					if(useFlurry){
						FlurryAgent.logEvent("Sign_up");
					}
				}else{
					showToast("No Internet Connection");
				}
			}
			break;
		case R.id.bttn_cancel:
			relative_sign_up.startAnimation(fade_out_anim);
			fade_out_anim.setFillAfter(true);
			fade_out_anim.setAnimationListener(new AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub
					layout_login_fb.setVisibility(View.VISIBLE);
					layout_login_fb.bringToFront();
					relative_sign_up.setVisibility(View.INVISIBLE);
				}
			});
			break;

		default:
			break;
		}
	}


	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(useLocalytics){
			this.localyticsSession.open();
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub

		super.onPause();
		if(useLocalytics){
			this.localyticsSession.close();
			this.localyticsSession.upload();
		}
	}

}
