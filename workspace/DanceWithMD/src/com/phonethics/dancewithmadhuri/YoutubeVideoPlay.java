package com.phonethics.dancewithmadhuri;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.OnInitializedListener;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerView;
import com.localytics.android.LocalyticsSession;

public class YoutubeVideoPlay extends YouTubeBaseActivity implements OnInitializedListener {

	String youtubeUrl=null;
	String DEVELOPER_KEY="AIzaSyAjqV4ReCrs02zdXBr_kluWfHZGlsM1S28";
	String videoId;

	YouTubePlayerView youtubePlayer;
	TextView		text;
	Button			bttn_menu;

	private static boolean useLocalytics 		= false;
	private static boolean useFlurry 			= false;
	private LocalyticsSession localyticsSession;
	private static  String LOCALYTICS_APP_KEY	="";

	Map<String, String> 	flurryEeventParams;
	Context					context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.youtubevideoplay);

		context = this;
		Typeface tf = Typeface.createFromAsset(getResources().getAssets(), "fonts/KabelMediumBT.ttf");
		Button bttn = (Button) findViewById(R.id.bttn_menu);
		bttn.setVisibility(View.GONE);



		TextView text_tittle = (TextView) findViewById(R.id.text_tittle);
		text_tittle.setTypeface(tf);

		bttn.setTypeface(tf);

		useFlurry				= getResources().getBoolean(R.bool.useFlurry);
		useLocalytics			= getResources().getBoolean(R.bool.useLocalytics);

		if(useLocalytics){
			LOCALYTICS_APP_KEY 		= getResources().getString(R.string.LOCALYTICS_APP_KEY);
			this.localyticsSession 	= new LocalyticsSession(this.getApplicationContext(),LOCALYTICS_APP_KEY);
			this.localyticsSession.open();
			//this.localyticsSession.upload();
			localyticsSession.tagScreen("View_Star_Speaks");
			this.localyticsSession.upload();
		}

		flurryEeventParams	= new HashMap<String, String>();
		try{
			youtubeUrl = getIntent().getExtras().getString("youtubeUrl");
			Log.i("url", youtubeUrl);


			String starName = getIntent().getExtras().getString("starName");
			text_tittle.setText(starName);
			
			boolean isFromJhalak = false;
			isFromJhalak = getIntent().getExtras().getBoolean("FROM_JHALAK");
			if(isFromJhalak){
			
			}else{
				flurryEeventParams.put("StarName", starName);
				if(useFlurry){
					FlurryAgent.logEvent("View_Star_Speaks", flurryEeventParams);
				}
				if(useLocalytics){
					localyticsSession.tagEvent("View_Star_Speaks", flurryEeventParams);
				}
			}

			videoId=youtubeUrl.substring(29, youtubeUrl.length()); //Obtains the id from video url
			Log.i("id",""+videoId);
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		

		youtubePlayer=(YouTubePlayerView) findViewById(R.id.youtube_view);


		youtubePlayer.initialize(DEVELOPER_KEY, this);

	}

	@Override
	public void onInitializationFailure(Provider arg0,
			YouTubeInitializationResult arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onInitializationSuccess(Provider provider, YouTubePlayer player,
			boolean wasRestored) {

		player.loadVideo(videoId);

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}


	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if(useLocalytics){
			this.localyticsSession.close();
			this.localyticsSession.upload();
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(useLocalytics){
			this.localyticsSession.open();
			localyticsSession.tagScreen("View_Star_Speaks");

		}
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if(useFlurry){
			FlurryAgent.onStartSession(context, getResources().getString(R.string.flurry_id));

		}
	}


	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if(useFlurry){
			FlurryAgent.onEndSession(context);
		}


	}



}
