package com.phonethics.dancewithmadhuri;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.phonethics.dancewithmadhuri.R;
import com.phonethics.fragments.Disclaimer;
import com.phonethics.fragments.Facebook;
import com.phonethics.fragments.Feedback;
import com.phonethics.fragments.HomeFragment;
import com.phonethics.fragments.Jhalak;
import com.phonethics.fragments.LearnDance;
import com.phonethics.fragments.Logout;
import com.phonethics.fragments.MyProfileFragment;
import com.phonethics.fragments.RootFragment;
import com.phonethics.fragments.StarSpeak;
import com.phonethics.fragments.Submissions;
import com.phonethics.fragments.Twitter;
import com.phonethics.fragments.TwitterNew;
import com.phonethics.sharepref.SharePref;

public class SampleListFragment extends ListFragment {

	SampleAdapter 		adapter;
	ArrayList<String> 	tabs;
	ArrayList<Integer> 	img_icon_off;
	ArrayList<Integer> 	img_icon_on;
	SharePref			sharePref;
	ArrayList<Boolean>  presentStateArr;
	Context 			context;
	String[]			menu_tabs;
	int					presentTab = 0;
	int					tempTab = 0;
	Typeface			tf;

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ListView list = (ListView)inflater.inflate(R.layout.list, null);
		
		tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/KabelMediumBT.ttf");
		View view = inflater.inflate(R.layout.menu_header, null);
		TextView textView = (TextView) view.findViewById(R.id.text_menu);
		textView.setTypeface(tf);
		list.addHeaderView(view);




		return list;
	}



	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setRetainInstance(true);
		if(savedInstanceState!=null){
			context =  getActivity();
		}else{
			context = getActivity();
		}
		tabs 			= new ArrayList<String>();
		img_icon_on 	= new ArrayList<Integer>();
		img_icon_off 	= new ArrayList<Integer>();
		presentStateArr	= new ArrayList<Boolean>();

		sharePref		= new SharePref(context);

		/*tabs.add("Home");
		tabs.add("My Profile");
		tabs.add("Learn Dance");
		tabs.add("Submissions");
		tabs.add("Star Speak");
		tabs.add("#DanceWithMD");
		tabs.add("Facebook");
		tabs.add("Feedback");
		tabs.add("Disclaimer");
		tabs.add("Jhalak");
		tabs.add("Logout");*/

		if(sharePref.getJhalakCheck()){
			menu_tabs = context.getResources().getStringArray(R.array.menu_tabs_jhalak_yes);

			img_icon_off.add(R.drawable.icon_home_off);
			img_icon_on.add(R.drawable.icon_home_on);

			img_icon_off.add(R.drawable.icon_profile_off);
			img_icon_on.add(R.drawable.icon_profile_on);

			img_icon_off.add(R.drawable.icon_learndance_off);
			img_icon_on.add(R.drawable.icon_learndance_on);

			img_icon_off.add(R.drawable.icon_submission_off);
			img_icon_on.add(R.drawable.icon_submission_on);

			img_icon_off.add(R.drawable.icon_gurusspeak_off);
			img_icon_on.add(R.drawable.icon_gurusspeak_on);


			img_icon_off.add(R.drawable.facebook_off);
			img_icon_on.add(R.drawable.facebook_on);

			img_icon_off.add(R.drawable.icon_tweeter_off);
			img_icon_on.add(R.drawable.icon_tweeter_on);



			img_icon_off.add(R.drawable.icon_feedback_off);
			img_icon_on.add(R.drawable.icon_feedback_on);

			img_icon_off.add(R.drawable.icon_disclaimer_off);
			img_icon_on.add(R.drawable.icon_disclaimer_on);

			img_icon_off.add(R.drawable.jdj_off);
			img_icon_on.add(R.drawable.jdj_on);

			img_icon_off.add(R.drawable.icon_logout_off);
			img_icon_on.add(R.drawable.icon_logout_on);
		}else{
			menu_tabs = context.getResources().getStringArray(R.array.menu_tabs_jhalak_no);

			img_icon_off.add(R.drawable.icon_home_off);
			img_icon_on.add(R.drawable.icon_home_on);

			img_icon_off.add(R.drawable.icon_profile_off);
			img_icon_on.add(R.drawable.icon_profile_on);

			img_icon_off.add(R.drawable.icon_learndance_off);
			img_icon_on.add(R.drawable.icon_learndance_on);

			img_icon_off.add(R.drawable.icon_submission_off);
			img_icon_on.add(R.drawable.icon_submission_on);

			img_icon_off.add(R.drawable.icon_gurusspeak_off);
			img_icon_on.add(R.drawable.icon_gurusspeak_on);

			img_icon_off.add(R.drawable.facebook_off);
			img_icon_on.add(R.drawable.facebook_on);

			img_icon_off.add(R.drawable.icon_tweeter_off);
			img_icon_on.add(R.drawable.icon_tweeter_on);



			img_icon_off.add(R.drawable.icon_feedback_off);
			img_icon_on.add(R.drawable.icon_feedback_on);

			img_icon_off.add(R.drawable.icon_disclaimer_off);
			img_icon_on.add(R.drawable.icon_disclaimer_on);



			img_icon_off.add(R.drawable.icon_logout_off);
			img_icon_on.add(R.drawable.icon_logout_on);
		}



		adapter = new SampleAdapter(context);

		for (int i = 0; i < menu_tabs.length; i++) {
			adapter.add(new SampleItem(menu_tabs[i], img_icon_on.get(i)));
			if(i==0){
				presentStateArr.add(true);
			}else{
				presentStateArr.add(false);
			}



		}
		/*adapter.add(new SampleItem("Home", android.R.drawable.ic_menu_search));
		adapter.add(new SampleItem("My Profile", android.R.drawable.ic_menu_search));
		adapter.add(new SampleItem("Feedback", android.R.drawable.ic_menu_search));
		adapter.add(new SampleItem("DanceWithMadhuri", android.R.drawable.ic_menu_search));
		adapter.add(new SampleItem("Facebook", android.R.drawable.ic_menu_search));
		adapter.add(new SampleItem("Twitter", android.R.drawable.ic_menu_search));
		adapter.add(new SampleItem("Log out", android.R.drawable.ic_menu_search));*/

		setListAdapter(adapter);
	}

	private class SampleItem {
		public String tag;
		public int iconRes;
		public SampleItem(String tag, int iconRes) {
			this.tag = tag; 
			this.iconRes = iconRes;
		}
	}

	public class SampleAdapter extends ArrayAdapter<SampleItem> {
		boolean instanceOfNewClass = true;
		int a = 0;
		LinearLayout layout;
		Fragment newContent = null;
		public SampleAdapter(Context context) {
			super(context, 0);
		}


		public View getView(int position, View convertView, ViewGroup parent) {
			final int a = position;
			if (convertView == null) {
				convertView = LayoutInflater.from(getContext()).inflate(R.layout.row, null);
				layout = (LinearLayout) convertView.findViewById(R.id.layout);
				/*if(sharePref.getPresentTab().equalsIgnoreCase(tabs.get(a))){
					layout.setBackgroundColor(Color.BLACK);
				}else{
					layout.setBackgroundColor(Color.parseColor("#333332"));
				}*/
				layout.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						//Toast.makeText(getActivity(), ""+tabs.get(a).toString(), 0).show();
						/*if(a==1){
							newContent = new HomeFragment();
						}

						if (newContent != null)
							switchFragment(newContent);*/

						switch(a){

						case 0:
							tempTab = 0;
							newContent = new HomeFragment();

							break;
						case 1:
							tempTab = 1;
							Log.d("", "MdInstatnce true");
							newContent = new MyProfileFragment();	
							instanceOfNewClass = true;


							//sharePref.setPresentTab("MyProfile");
							break;
						case 2:
							tempTab = 2;
							newContent = new LearnDance();
							//sharePref.setPresentTab("LearnDance");
							break;
						case 3:
							tempTab = 3;
							newContent = new Submissions();
							//sharePref.setPresentTab("Submissions");
							break;
						case 4:
							tempTab = 4;
							newContent = new StarSpeak();
							//sharePref.setPresentTab("StarSpeak");
							//Toast.makeText(getActivity(), "start speak", 0).show();
							break;
						case 6:
							tempTab = 6;
							newContent = new TwitterNew();
							//sharePref.setPresentTab("Twitter");
							break;

						case 5:
							tempTab = 5;
							newContent = new Facebook();
							break;
						case 7:
							tempTab = 7;
							newContent = new Feedback();
							//sharePref.setPresentTab("Feedback");
							break;
						case 8:
							tempTab = 8;
							newContent = new Disclaimer();
							//sharePref.setPresentTab("Disclaimer");
							break;
						case 9:
							
							if(sharePref.getJhalakCheck()){
								tempTab = 9;
								newContent = new Jhalak();
							}else{
								tempTab = 10;
								newContent = new Logout();
								break;
							}
							//sharePref.setPresentTab("Jhalak");
							break;
						case 10:
							tempTab = 10;
							newContent = new Logout();
							break;


						}
						sharePref.setPresentTab(menu_tabs[a]);
						if (newContent != null){
							
								presentTab = a;
								switchFragment(newContent);
							

						}



						//notifyDataSetChanged();
					}
				});
			}
			ImageView icon = (ImageView) convertView.findViewById(R.id.row_icon);
			/*if(sharePref.getPresentTab().equalsIgnoreCase(tabs.get(position))){
				icon.setImageResource(img_icon_off.get(position));

			}else{
				icon.setImageResource(getItem(position).iconRes);

			}*/
			//icon.setImageResource(getItem(position).iconRes);
			TextView title = (TextView) convertView.findViewById(R.id.row_title);
			title.setText(getItem(position).tag);
			title.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/KabelMediumBT.ttf"));
			title.setTextColor(Color.parseColor("#9e9e9e"));
			//if(sharePref.getPresentTab().equalsIgnoreCase(tabs.get(position))){
			if(sharePref.getPresentTab().equalsIgnoreCase(menu_tabs[position])){
				icon.setImageResource(img_icon_off.get(position));
				title.setTextColor(Color.WHITE);
			}else{
				icon.setImageResource(getItem(position).iconRes);
				title.setTextColor(Color.parseColor("#9e9e9e"));
			}

			//if(sharePref.getPresentTab().equalsIgnoreCase(tabs.get(a))){
			if(sharePref.getPresentTab().equalsIgnoreCase(menu_tabs[position])){
				layout.setBackgroundColor(Color.BLACK);
			}else{
				layout.setBackgroundColor(Color.parseColor("#333332"));
			}


			return convertView;
		}

		private void switchFragment(Fragment fragment) {
			//Toast.makeText(getActivity(), "switch fragment", 0).show();
			if (context == null){
				Toast.makeText(getActivity(), "null", 0).show();
				return;
			}


			if (context instanceof RootFragment) {
				//Toast.makeText(getActivity(), "instance", 0).show();
				if(fragment instanceof Logout ){
					RootFragment fca = (RootFragment) getActivity();
					fca.logoutAlert();
				}else{
					RootFragment fca = (RootFragment) getActivity();
					fca.switchContent(fragment);
				}
			}else {
				if(fragment instanceof Logout ){
					RootFragment fca = (RootFragment) getActivity();
					fca.logoutAlert();
				}else{
					RootFragment fca = (RootFragment) getActivity();
					fca.switchContent(fragment);
				}
				//Toast.makeText(getActivity(), "not instance", 0).show();
			}
		}

		private void showContent(){
			RootFragment fca = (RootFragment) getActivity();
			fca.showContent();
		}



		@Override
		public void notifyDataSetChanged() {
			// TODO Auto-generated method stub
			super.notifyDataSetChanged();
		}


		private void setPresentState(int pos){
			for (int i = 0; i < presentStateArr.size(); i++) {
				if(i==pos){
					presentStateArr.set(i, true);
				}else{
					presentStateArr.set(i, false);
				}
			}
		}

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		if(outState!=null){
			context = getActivity().getApplicationContext();
		}
	}



}

