package com.phonethics.dancewithmadhuri;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyResponse.ErrorListener;
import com.android.volley.VolleyResponse.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.flurry.android.FlurryAgent;
import com.localytics.android.LocalyticsSession;
import com.phonethics.adapters.LeaderBoardAdapter;
import com.phonethics.customclass.LeaderBoardDetails;

import com.phonethics.network.NetworkCheck;

public class LeaderBoard extends Activity {

	Activity 		context;
	TextView		text_dancers,text_rank,text_userName,text_points,text_uDancers,text_uRank,text_uPoints;
	TextView		text_rankList,text_NameList,text_pointsList;
	ListView		list_leaderboard;
	String			dancers,points,rank,userName;
	Typeface		tf;
	String			URL_DWM,URl_LEADERBOARD;
	NetworkCheck 	netCheck;
	RequestQueue 	queue;
	ArrayList<LeaderBoardDetails> leaderBrdArr;
	ImageView	pBar;
	Animation	rotate;
	ImageView		imgTrans1,imgTrans2,imgTrans3,imgTrans4;
	static 			float transValue = 2f;

	private static boolean useLocalytics 		= false;
	private static boolean useFlurry 			= false;
	private LocalyticsSession localyticsSession;
	private static  String LOCALYTICS_APP_KEY	="";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_leader_board);

		context = this;

		useFlurry					= getResources().getBoolean(R.bool.useFlurry);
		useLocalytics				= getResources().getBoolean(R.bool.useLocalytics);

		if(useLocalytics){
			LOCALYTICS_APP_KEY 		= getResources().getString(R.string.LOCALYTICS_APP_KEY);
			this.localyticsSession 	= new LocalyticsSession(this.getApplicationContext(),LOCALYTICS_APP_KEY);
			this.localyticsSession.open();
			//this.localyticsSession.upload();
			localyticsSession.tagScreen(getResources().getString(R.string.localEvent_tab_leaderboard));
			this.localyticsSession.upload();
		}

		tf				= Typeface.createFromAsset(getAssets(), "fonts/KabelMediumBT.ttf");
		netCheck		= new NetworkCheck(context);
		URL_DWM			= getResources().getString(R.string.url_md_live_server);
		URl_LEADERBOARD	= URL_DWM  + getResources().getString(R.string.url_leader_board);
		rotate 			= AnimationUtils.loadAnimation(context, R.anim.rotate);
		pBar			= (ImageView) findViewById(R.id.progressBar2);

		Button bttn = (Button) findViewById(R.id.bttn_menu);
		TextView textView_tittle = (TextView) findViewById(R.id.text_tittle);
		textView_tittle.setTypeface(tf);
		textView_tittle.setText("Leader Board");
		bttn.setVisibility(View.GONE);


		text_dancers 	= (TextView) findViewById(R.id.text_dancers);
		text_rank 		= (TextView) findViewById(R.id.text_rank);
		text_userName 	= (TextView) findViewById(R.id.text_user_name);
		text_points 	= (TextView) findViewById(R.id.text_points);
		text_uDancers 	= (TextView) findViewById(R.id.text_dancers_users);
		text_uRank 		= (TextView) findViewById(R.id.text_rank_users);
		text_uPoints 	= (TextView) findViewById(R.id.text_points_users);
		text_rankList	= (TextView) findViewById(R.id.text_rank_list);
		text_NameList	= (TextView) findViewById(R.id.text_name_list);
		text_pointsList	= (TextView) findViewById(R.id.text_points_list);
		list_leaderboard = (ListView) findViewById(R.id.list_leaderBoard);

		imgTrans1	= (ImageView) findViewById(R.id.img_trans_leader_dancers);
		imgTrans2	= (ImageView) findViewById(R.id.img_trans_leader_points);
		imgTrans3	= (ImageView) findViewById(R.id.img_trans_leader_rank);
		imgTrans4	= (ImageView) findViewById(R.id.img_trans_leader_list_back);

		leaderBrdArr	= new ArrayList<LeaderBoardDetails>();

		text_dancers.setText("Dancers");
		text_rank.setText("Rank");
		text_points.setText("Points");
		text_rankList.setText("Rank");
		text_NameList.setText("Name");
		text_pointsList.setText("Points");

		text_dancers.setTypeface(tf);
		text_rank.setTypeface(tf);
		text_userName.setTypeface(tf);
		text_points.setTypeface(tf);
		text_uDancers.setTypeface(tf);
		text_uRank.setTypeface(tf);
		text_uPoints.setTypeface(tf);
		text_rankList.setTypeface(tf);
		text_NameList.setTypeface(tf);
		text_pointsList.setTypeface(tf);

		text_userName.setVisibility(View.GONE);


		Bundle bundle = getIntent().getExtras();
		if(bundle !=  null){
			dancers 	= bundle.getString("dancers");
			points 		= bundle.getString("points");
			userName 	= bundle.getString("userName");
			rank 		= bundle.getString("rank");
		}

		text_uDancers.setText(dancers);
		text_uRank.setText(rank);
		text_uPoints.setText(points);
		text_userName.setText(userName);

		setAlphaForView(imgTrans1, transValue);
		setAlphaForView(imgTrans2, transValue);
		setAlphaForView(imgTrans3, transValue);
		setAlphaForView(imgTrans4, 5f);
		list_leaderboard.setCacheColorHint(0);
		imgTrans4.getBackground().setAlpha(100);

		if(netCheck.isNetworkAvailable()){
			getLeaderBoardDetails();
		}else{
			Toast.makeText(context, "No Internet Connection", 0).show();
			pBar.setVisibility(View.GONE);
			pBar.clearAnimation();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.leader_board, menu);
		return true;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		finish();
	}


	public void getLeaderBoardDetails(){
		Log.d("URL", "MdLedrBrdUrl == "+URl_LEADERBOARD);
		queue				= Volley.newRequestQueue(context);
		pBar.setVisibility(View.VISIBLE);
		pBar.startAnimation(rotate);
		Listener<JSONObject> listener = new Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub
				try{
					Log.d("", "MdLeaderBoardResponse == "+response.toString());
					if(response.getString("success").equalsIgnoreCase("true")|| response.getBoolean("success")){
						JSONArray jArr = response.getJSONArray("data");
						parseJson(jArr);
					}
				}catch(Exception ex){
					ex.printStackTrace();
					pBar.clearAnimation();
					pBar.setVisibility(View.INVISIBLE);
				}

			}
		};

		ErrorListener errorListener = new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				pBar.clearAnimation();
				pBar.setVisibility(View.GONE);
			}
		};

		JsonObjectRequest request = new JsonObjectRequest(Method.GET,URl_LEADERBOARD, null, listener, errorListener);
		queue.add(request);
	}


	public void parseJson(JSONArray jArr){
		try{
			pBar.clearAnimation();
			pBar.setVisibility(View.INVISIBLE);
			for(int i=0;i<jArr.length();i++){
				LeaderBoardDetails leaderBrd = new LeaderBoardDetails();
				JSONObject jObj  = jArr.getJSONObject(i);
				leaderBrd.setPoints(jObj.getString("points"));
				leaderBrd.setRank(jObj.getString("rank"));
				leaderBrd.setUser_id(jObj.getString("User_ID"));
				leaderBrd.setUser_name(jObj.getString("User_Name"));

				leaderBrdArr.add(leaderBrd);
			}

			list_leaderboard.setAdapter(new LeaderBoardAdapter(context, leaderBrdArr));
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	private void setAlphaForView(View v, float alpha) {
		AlphaAnimation animation = new AlphaAnimation(alpha, alpha);
		animation.setDuration(0); 
		animation.setFillAfter(true); 
		v.startAnimation(animation);
	}


	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if(useFlurry){
			FlurryAgent.onStartSession(this, getResources().getString(R.string.flurry_id));
			FlurryAgent.logEvent(getResources().getString(R.string.fEvent_tab_leaderboard));
		}

	}


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if(useFlurry){
			FlurryAgent.onEndSession(this);
		}


	}




	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(useLocalytics){
			this.localyticsSession.open();
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub

		super.onPause();
		if(useLocalytics){
			this.localyticsSession.close();
			this.localyticsSession.upload();
		}
	}

}
