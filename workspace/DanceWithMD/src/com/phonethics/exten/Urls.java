package com.phonethics.exten;

/**
 * Created by senneco on 21.05.13.
 */
public abstract class Urls {
    public static final String BASE_URL = "https://api.twitter.com/";
    public static String MAX_ID="";
	public static final String OBTAIN_TOKEN_URL = BASE_URL + "oauth2/token";
    //public static final String USER_TIMELINE_URL = BASE_URL + "1.1/statuses/user_timeline.json?screen_name=AndroidDev";
	//public static final String USER_TIMELINE_URL = BASE_URL + "1.1/statuses/user_timeline.json?screen_name=iTweets89";
    public static final String USER_TIMELINE_URL = BASE_URL+ "1.1/search/tweets.json?q=%23dancewithmadhuri";
    public static final String USER_TIMELINE_WITH_MAX_ID_URL = USER_TIMELINE_URL + "&max_id=";
    public static final String SEARCH_MD_TWEETS = BASE_URL+ "1.1/search/tweets.json?q=%23dancewithmadhuri";
    
    
    //https://api.twitter.com/1.1/search/tweets.json?count=10&q=%23cricket&result_type=mixed
}
