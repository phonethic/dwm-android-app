package com.phonethics.exten;

import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;



import org.json.JSONArray;
import org.json.JSONObject;

import android.os.Handler;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyResponse;
import com.android.volley.VolleyResponse.ErrorListener;
import com.android.volley.VolleyResponse.Listener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.FieldNamingStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.phonethics.fragments.TwitterNew;

/**
 * Volley adapter for JSON requests that will be parsed into Java objects by Gson.
 */
public class GsonRequest<T> extends Request<T> {
    private static final Gson GSON;
    private final String mToken;
    private final Class<T> mResponseClass;
    final Listener<T> mListener;
    String tweetsFile =	"/sdcard/dwm/tweetsTest.txt";
    
    /** Initialize  Request queue object*/
	RequestQueue queueVolley;

    static {
        GSON = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
                .setFieldNamingStrategy(new CustomFieldNamingPolicy())
                .setPrettyPrinting()
                .setDateFormat("EEE MMM d HH:mm:ss Z yyyy")
                .serializeNulls()
                .create();
    }

    public GsonRequest(String token, String url, Class<T> clazz, Listener<T> listener, ErrorListener errorListener) {
        this(token, Method.GET, url, clazz, listener, errorListener);
    }

    public GsonRequest(String token, int method, String url, Class<T> clazz, Listener<T> listener, ErrorListener errorListener) {
        super(method, url, errorListener);
        //queueVolley=newRequestQueue(getActivity());;
        mToken = token;
        mResponseClass = clazz;
        mListener = listener;
    }


	@Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", mToken);
        return headers;
    }

    @Override
    protected void deliverResponse(T response) {
        mListener.onResponse(response);
    }

    @Override
    protected VolleyResponse<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            parseJsonData(json);
            return VolleyResponse.success(GSON.fromJson(json, mResponseClass), HttpHeaderParser.parseCacheHeaders(response));
            //return null;
        } catch (Exception e) {
            return VolleyResponse.error(new ParseError(e));
        }
    }
    
  

    static class CustomFieldNamingPolicy implements FieldNamingStrategy {
        @Override
        public String translateName(Field field) {
            String name = FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES.translateName(field);
            name = name.substring(2, name.length()).toLowerCase();
            return name;
        }
    }
    
    
    
    public void parseJsonData(final String jsonString){
    	
    	//Handler hand=new Handler();
    	//Runnable run=new Runnable() {
			
			//@Override
			//public void run() {
				try{
					JSONObject json = new JSONObject(jsonString);
					JSONArray jArr = json.getJSONArray("statuses");
					for(int i=0;i<jArr.length();i++){
						
						JSONObject tempJObj = jArr.getJSONObject(i);
						String id = tempJObj.getString("id");
						
						if(TwitterNew.tweet_id.contains(id));
						else{
							String tweet = tempJObj.getString("text");
							TwitterNew.tweets_Arr.add(tweet);

							JSONObject tempUserObj = tempJObj.getJSONObject("user");
							String img_url 			= tempUserObj.getString("profile_image_url");
							//String img_url_bigger 	= img_url.replace("_normal", "_bigger");
							String img_url_bigger 	= img_url.replace("_normal", "");
							//String img_url_bigger 	= img_url.replace("_normal", "_mini");
							TwitterNew.img_url_Arr.add(img_url_bigger);

							String name = tempUserObj.getString("screen_name");
							TwitterNew.screen_name_arr.add(name);
						
							TwitterNew.tweet_id.add(id);
						}
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
		//}
    	//};
    	//hand.postDelayed(run, 2000);
    }	

}