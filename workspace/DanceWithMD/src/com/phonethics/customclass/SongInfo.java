package com.phonethics.customclass;

public class SongInfo {
	
	String	song_id;
	String	title;
	String 	movie;
	String	choreographer;
	String	thumbnail;
	String	lock_type;
	String	category;
	String	category_name;
	String	is_video;
	String	video_link;
	
	
	public String getSong_id() {
		return song_id;
	}
	public void setSong_id(String song_id) {
		this.song_id = song_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMovie() {
		return movie;
	}
	public void setMovie(String movie) {
		this.movie = movie;
	}
	public String getChoreographer() {
		return choreographer;
	}
	public void setChoreographer(String choreographer) {
		this.choreographer = choreographer;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	public String getLock_type() {
		return lock_type;
	}
	public void setLock_type(String lock_type) {
		this.lock_type = lock_type;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCategory_name() {
		return category_name;
	}
	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}
	public String getIs_video() {
		return is_video;
	}
	public void setIs_video(String is_video) {
		this.is_video = is_video;
	}
	public String getVideo_link() {
		return video_link;
	}
	public void setVideo_link(String video_link) {
		this.video_link = video_link;
	}
	
	
	

}
