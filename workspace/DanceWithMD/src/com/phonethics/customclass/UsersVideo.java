package com.phonethics.customclass;

public class UsersVideo {

	
	
	String		videoId;
	String		userId;
	String		userName;
	String		picture;
	String		songId;
	String		mp4Link;
	String		votes;
	public String getVideoId() {
		return videoId;
	}
	public void setVideoId(String videoId) {
		this.videoId = videoId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public String getSongId() {
		return songId;
	}
	public void setSongId(String songId) {
		this.songId = songId;
	}
	public String getMp4Link() {
		return mp4Link;
	}
	public void setMp4Link(String mp4Link) {
		this.mp4Link = mp4Link;
	}
	public String getVotes() {
		return votes;
	}
	public void setVotes(String votes) {
		this.votes = votes;
	}
	
	
	
}
