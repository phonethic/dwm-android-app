package com.phonethics.customclass;

public class UserDetails {
	
	String		rank;
	String		name;
	String		points;
	String		badges;
	String		special_badges;
	String		dancers;
	String		total_badges;
	String		total_special_badges;
	
	
	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}
	public String getPoints() {
		return points;
	}
	public void setPoints(String points) {
		this.points = points;
	}
	public String getBadges() {
		return badges;
	}
	public void setBadges(String badges) {
		this.badges = badges;
	}
	public String getSpecial_badges() {
		return special_badges;
	}
	public void setSpecial_badges(String special_badges) {
		this.special_badges = special_badges;
	}
	public String getDancers() {
		return dancers;
	}
	public void setDancers(String dancers) {
		this.dancers = dancers;
	}
	public String getTotal_badges() {
		return total_badges;
	}
	public void setTotal_badges(String total_badges) {
		this.total_badges = total_badges;
	}
	public String getTotal_special_badges() {
		return total_special_badges;
	}
	public void setTotal_special_badges(String total_special_badges) {
		this.total_special_badges = total_special_badges;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	

}
