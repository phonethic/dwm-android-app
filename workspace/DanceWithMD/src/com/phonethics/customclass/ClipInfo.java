package com.phonethics.customclass;

public class ClipInfo {

	String		song_id;
	String		song_name;
	String		clip_part_id;
	String		sequence;
	String		clip_tittle;
	public String getSong_id() {
		return song_id;
	}
	public void setSong_id(String song_id) {
		this.song_id = song_id;
	}
	public String getSong_name() {
		return song_name;
	}
	public void setSong_name(String song_name) {
		this.song_name = song_name;
	}
	public String getClip_part_id() {
		return clip_part_id;
	}
	public void setClip_part_id(String clip_part_id) {
		this.clip_part_id = clip_part_id;
	}
	public String getSequence() {
		return sequence;
	}
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
	public String getClip_tittle() {
		return clip_tittle;
	}
	public void setClip_tittle(String clip_tittle) {
		this.clip_tittle = clip_tittle;
	}
	
	
	
	
}
