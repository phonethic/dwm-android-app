package com.phonethics.customclass;

public class ClipVideoDetails {


	String	lesson = "";
	String	clip_id = "";
	String 	thumbnail = "";
	String 	song_name = "";
	String 	video_url = "";
	String 	url	= "";
	String 	clip_title = "";
	String	slug	= "";
	String	type	= "";
	String	embedd_code = "";
	String	style	= "";



	public String getLesson() {
		return lesson;
	}
	public void setLesson(String lesson) {
		this.lesson = lesson;
	}
	public String getClip_id() {
		return clip_id;
	}
	public void setClip_id(String clip_id) {
		this.clip_id = clip_id;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	public String getSong_name() {
		return song_name;
	}
	public void setSong_name(String song_name) {
		this.song_name = song_name;
	}
	public String getVideo_url() {
		return video_url;
	}
	public void setVideo_url(String video_url) {
		this.video_url = video_url;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getClip_title() {
		return clip_title;
	}
	public void setClip_title(String clip_title) {
		this.clip_title = clip_title;
	}
	public String getSlug() {
		return slug;
	}
	public void setSlug(String slug) {
		this.slug = slug;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getEmbedd_code() {
		return embedd_code;
	}
	public void setEmbedd_code(String embedd_code) {
		this.embedd_code = embedd_code;
	}
	public String getStyle() {
		return style;
	}
	public void setStyle(String style) {
		this.style = style;
	}




}
