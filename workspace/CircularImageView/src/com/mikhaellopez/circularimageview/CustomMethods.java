package com.mikhaellopez.circularimageview;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.app.Activity;
import android.graphics.Typeface;
import android.text.NoCopySpan.Concrete;
import android.util.DisplayMetrics;

public class CustomMethods {

	Activity							context;
	private static DisplayMetrics 		metrics;
	
	
	public CustomMethods(Activity context){
		this.context 	= context;
		
	}
	
	/*
	 * Returns the Screen width;
	 */
	public int getScreenWidth(){
		metrics 		= context.getResources().getDisplayMetrics();
		return  metrics.widthPixels;
	}
	
	
	/*
	 * Returns the Screen height;
	 */
	public int getScreenHeight(){
		metrics 		= context.getResources().getDisplayMetrics();
		return  metrics.heightPixels;
	}
	
	/*
	 * Returns the Screen Density;
	 */
	public int getScreenDensityDpi(){
		return metrics.densityDpi;
	}
	
	
	/*
	 * @stringToConvert : String which needs to Convert in md5
	 */
	public String convertToMd5(String stringToConvert){
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("MD5");
			digest.reset();
			digest.update(stringToConvert.getBytes());
			byte[] a = digest.digest();
			int len = a.length;
			StringBuilder sb = new StringBuilder(len << 1);
			for (int i = 0; i < len; i++) {
				sb.append(Character.forDigit((a[i] & 0xf0) >> 4, 16));
				sb.append(Character.forDigit(a[i] & 0x0f, 16));
			}
			return sb.toString();

		}catch(NoSuchAlgorithmException e){
			e.printStackTrace(); 
		}
		return null;
	}
	
	/*
	 * Returns Typface style;
	 */
	public Typeface getTypeFace(){
		String typFaceName = context.getResources().getString(R.string.fontStyleName);
		return Typeface.createFromAsset(context.getResources().getAssets(), "fonts/"+typFaceName);
	}
	
	
}
