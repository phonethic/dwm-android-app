package com.mikhaellopez.circularimageview;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import com.phonethics.imageloader.ImageLoader;

public class MainActivity extends Activity {

	CircularImageView	imageView;
	ImageLoader			imageLoader;
	Context				context;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	
		context = this;
		imageLoader = new ImageLoader(context);
		imageView = (CircularImageView) findViewById(R.id.imageViewCircular);
		imageLoader.DisplayImage("http://www.topnews.in/files/Madhuri-Dixit_10.jpg", imageView);
		
	}

}
